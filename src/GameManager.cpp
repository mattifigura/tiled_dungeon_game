#include "GameManager.h"
#include "InputManager.h"
#include "DeltaTime.h"
#include <iostream>

GameManager::GameManager()
{

}

GameManager::~GameManager()
{
	// Destroy the window
	if (m_pWindow)
		glfwDestroyWindow(m_pWindow);

	// End GLFW
	glfwTerminate();
}

bool GameManager::Initialise()
{
	// Initialise GLFW
	if (!glfwInit())
		return false;

	// Create the settings manager and read in settings
	// TEMPORARY: don't do this

	// Create the window
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	m_pWindow = glfwCreateWindow(1600, 900, "Generic Dungeon Game", NULL, NULL);

	// If we haven't successfully created the window quit now
	if (!m_pWindow)
	{
		std::cout << "Failed to create window" << std::endl;
		return false;
	}

	// Create input manager
	Singleton<InputManager>::Instance().AttachWindow(m_pWindow);

	// Set the current context
	glfwMakeContextCurrent(m_pWindow);

	// Set up V-Sync
	glfwSwapInterval(1); // TEMPORARY: set vsync to true

	// Initialise GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return false;

	// Enable transparency in textures
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	// Global vertex array
	//
	// NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//
	// This is f*cking lazy. This engine wasn't originally written with OpenGL
	// core 3.3+ in mind, so I never set up proper use of VAOs. Instead just
	// bind a single VAO now so it acts like non-core profiles / older GL.
	// I REALLY REALLY REALLY should set this up properly and clean up a lot of
	// this awful messy code, but that can come later...           ... :D
	//
	// NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// TEMPORARY: initialise the scene
	m_GameScene.CreateScene();

	// If we reach here we have successfully initialised
	return true;
}

void GameManager::Run()
{
	// The main game loop
	while (!glfwWindowShouldClose(m_pWindow))
	{
		// Get the delta time
		Singleton<DeltaTime>::Instance().CalculateDelta();
		
		// Run the current scene
		m_GameScene.Run();

		// Swap buffers and poll for events
		glfwSwapBuffers(m_pWindow);
		glfwPollEvents();

		// Update the input manager
		Singleton<InputManager>::Instance().Update();
	}
}
