#ifndef TERRAIN_CHUNK_MESH_H_
#define TERRAIN_CHUNK_MESH_H_

#include "Mesh.h"
#include <mutex>

class Terrain;

// TerrainChunkMesh class - the visual representation of a terrain chunk. The data is handled in a different class

class TerrainChunkMesh : public Mesh<GLVertex> {
public:
	TerrainChunkMesh();

	void BufferTerrain();
	void CreateMesh(const Terrain* terrain);

protected:

private:
	bool	m_bNeedsCreation;
};

#endif