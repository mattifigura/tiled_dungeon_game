#ifndef PARTICLE_EMITTER_H
#define PARTICLE_EMITTER_H

#include "GLBase.h"
#include <atomic>
#include <vector>

// Particle struct
#pragma pack(1)
struct Particle {
	vector3df pos;
	GLColour col;
	vector2df size;
	float spare[2];
	
	Particle() : pos(), col(), size(0.1f, 0.1f) {}
};
#pragma pack()

// ParticleData struct
struct ParticleData {
	vector3df force;
	float life;
	float end_life;
	
	// Default behaviour is to fade from white to transparent black moving upwards for 1 seconds
	ParticleData() : force(0.f, 1.f, 0.f), life(-2000.f), end_life(9000.f) {}
};

// EmitterBehaviour struct
struct EmitterBehaviour {
	vector3df force = {};
	vector3df force_randomiser_lower = {};
	vector3df force_randomiser_upper = {};
	bool force_from_start = {false};
	vector3df friction = {};
	GLColour start_colour = {};
	GLColour end_colour = {};
	float duration = {1000.f};
	float duration_randomiser = {500.f};
	vector3df gravity = {};
	vector3df gravity_anchor_position = {};
	bool gravity_anchor = {false};
	bool gravity_anchor_rotate = {false};
	bool collisions = {false};
	vector3df bounce = {};
	vector3df position_randomiser_lower = {};
	vector3df position_randomiser_upper = {};
	float minimum_start_distance = {0.f};
	vector2df start_size = {0.1f, 0.1f};
	vector2df end_size = {};
	bool only_live_once = {false};
	std::vector<GLColour> random_colours;
};

// ParticleEmitter class - Defines a particle emitter, handles particle effects

class SceneManager;
class Light;

class ParticleEmitter {
public:
	ParticleEmitter(SceneManager* sceneman, size_t size);
	~ParticleEmitter();

	void SetPosition(const vector3df& pos) { m_Position = pos; }
	const vector3df& GetPosition() const { return m_Position; }
	void Translate(const vector3df& pos);

	EmitterBehaviour& GetBehaviour() { return m_Behaviour; }

	bool IsDead() const { return m_Dead; }
	void Kill() { m_Dead = true; }

	void Burst();
	void Stop();
	void SpawnAll();

	void AddLighting();

	Particle* GetParticles(size_t& size) const;
	GLuint GetBufferObject(size_t& size) const;

	void SetAffectedByLighting(bool affected) { m_AffectedByLighting = affected; }
	bool IsAffectedByLighting() const { return m_AffectedByLighting; }

	void Update(float delta);
	bool Buffer();

protected:

private:
	SceneManager*		m_SceneMan;
	size_t				m_Size;
	Particle*			m_Particles;
	ParticleData*		m_ParticleData;
	std::vector<Light*>	m_Lights;
	EmitterBehaviour	m_Behaviour;
	vector3df			m_Position;
	std::atomic_bool	m_CanBuffer;
	std::atomic_bool	m_DataChanged;
	bool				m_Dead;
	bool				m_AffectedByLighting;
	
	GLuint				m_BufferObject;
	
	void ClearBuffers();
	void SpawnParticle(size_t id);
};

#endif // PARTICLE_EMITTER_H
