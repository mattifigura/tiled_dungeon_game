#include "GLTexture.h"
#include <libpng16/png.h>
#include <iostream>
#include <string>

GLTexture::GLTexture(const std::string& filename)
	: m_Texture(0u)
{
	// Load the texture from the given file, and if it doesn't exist, we will not be a valid texture
	if (!filename.empty())
	{
		// Load .png
		if (filename.substr(filename.size() - 4) == ".png")
			LoadFromPNG(filename);
		
		// Finally, if this is the cursor image return a texture with a large ID - this is to keep the cursor in a separate mesh
		else if (filename == "?CURSOR")
			m_Texture = 1234567u;
	}
}

GLTexture::~GLTexture()
{
	glDeleteTextures(1, &m_Texture);
}

GLuint GLTexture::ID() const
{
	return m_Texture;
}

const vector2di& GLTexture::Dimensions() const
{
	return m_Dimensions;
}

const GLColour& GLTexture::GetPixel(int x, int y) const
{
	// Ensure we're within bounds
	if (x >= 0 && x < m_Dimensions.x && y >= 0 && y < m_Dimensions.y)
		return m_Pixels.at(x * m_Dimensions.y + y);

	// Return a default pixel
	static GLColour pixel;
	return pixel;
}

void GLTexture::CreateTextureFromBuffer(const GLubyte* buffer, int width, int height, GLenum type)
{
	// Create a custom texture from the given buffer
	bool bLuminanceAlpha = type == GL_LUMINANCE_ALPHA;

	// Set up our internal buffer
	for (int x = 0; x < width; ++x) {
		for (int y = 0; y < height; ++y) {
			GLColour pixel;
			if (bLuminanceAlpha)
			{
				// 2 bytes per pixel aka greyscale
				pixel.r = buffer[x * (height * 2) + y];
				pixel.g = buffer[x * (height * 2) + y];
				pixel.b = buffer[x * (height * 2) + y];
				pixel.a = buffer[x * (height * 2) + y + 1];
			}
			else
			{
				// RGBA
				pixel.r = buffer[(x * height + y) * 4];
				pixel.g = buffer[(x * height + y) * 4 + 1];
				pixel.b = buffer[(x * height + y) * 4 + 2];
				pixel.a = buffer[(x * height + y) * 4 + 3];
			}

			m_Pixels.push_back(pixel);
		}
	}
	m_Dimensions.Set(width, height);

	// Create the texture
	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, type, GL_UNSIGNED_BYTE, buffer);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLTexture::LoadFromPNG(const std::string& filename)
{
	// This texture hasn't yet been loaded, let us load it
	std::cout << "Loading texture " << filename << std::endl;

	// Load the texture from a png file and bind it to a gl texture
#ifdef __linux__
    FILE* file = fopen(filename.c_str(), "rb");
#else
	FILE *file = new FILE;
	fopen_s(&file, filename.c_str(), "rb");
#endif
	if (!file)
	{
		std::cout << "ERROR: Cannot open texture file " << filename << std::endl;
		return;
	}

	// Check if this is a png file or not
	png_byte pngHeader[8];
	fread(pngHeader, 1, 8, file);

	if (png_sig_cmp(pngHeader, 0, 8))
	{
		std::cout << "ERROR: " << filename << " is not a valid PNG file!" << std::endl;
		fclose(file);
		return;
	}

	// Create png read struct
	png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pngPtr)
	{
		std::cout << "ERROR: PNG file loading error #1066" << std::endl;
		fclose(file);
		return;
	}

	// Create png info structs
	png_infop infoPtr = png_create_info_struct(pngPtr);
	if (!infoPtr)
	{
		std::cout << "ERROR: PNG file loading error #1166" << std::endl;
		png_destroy_read_struct(&pngPtr, NULL, NULL);
		fclose(file);
		return;
	}

	png_infop endPtr = png_create_info_struct(pngPtr);
	if (!endPtr)
	{
		std::cout << "ERROR: PNG file loading error #1266" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, NULL);
		fclose(file);
		return;
	}

	// We use setjmp to see if libpng has encountered an error
	if (setjmp(png_jmpbuf(pngPtr)))
	{
		std::cout << "ERROR: PNG file loading error #1366" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return;
	}

	// Start reading the png file
	png_init_io(pngPtr, file);
	png_set_sig_bytes(pngPtr, 8);
	png_read_info(pngPtr, infoPtr);

	int iBitDepth, iColourType;
	png_uint_32 iWidth, iHeight;

	png_get_IHDR(pngPtr, infoPtr, &iWidth, &iHeight, &iBitDepth, &iColourType, NULL, NULL, NULL);

	if (iBitDepth != 8)
	{
		std::cout << "ERROR: PNG file loading error #1466" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return;
	}

	// Get the colour format of this png file
	GLint format = GL_RGBA;
	if (iColourType == PNG_COLOR_TYPE_RGB)
	{
		format = GL_RGB;
	}
	else if (iColourType != PNG_COLOR_TYPE_RGB_ALPHA)
	{
		std::cout << "ERROR: PNG file loading error #1566" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return;
	}

	png_read_update_info(pngPtr, infoPtr);

	int iRowBytes = static_cast<int>(png_get_rowbytes(pngPtr, infoPtr));
	iRowBytes += 3 - ((iRowBytes - 1) % 4);

	if (iRowBytes == 0 || iHeight == 0 || iWidth == 0)
	{
		std::cout << "ERROR: PNG file loading error #1606" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return;
	}

	png_byte* imageData = (png_byte*)malloc(iRowBytes * iHeight * sizeof(png_byte) + 15);
	if (!imageData)
	{
		std::cout << "ERROR: PNG file loading error #1666" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return;
	}

	png_byte** rowPtrs = (png_byte**)malloc(iHeight * sizeof(png_byte*));
	if (!rowPtrs)
	{
		std::cout << "ERROR: PNG file loading error #1666" << std::endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		free(imageData);
		fclose(file);
		return;
	}

	for (unsigned int i = 0; i < iHeight; ++i) {
		rowPtrs[iHeight - 1 - i] = imageData + i * iRowBytes;
	}

	// Read the data from the png
	png_read_image(pngPtr, rowPtrs);
	png_read_end(pngPtr, endPtr);

	// Now we can generate an opengl texture
	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, format, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, imageData);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Copy the data of the image into a local buffer
	for (png_uint_32 x = 0; x < iWidth; ++x) {
		for (png_uint_32 y = 0; y < iHeight; ++y) {
			//int pos = ((x * iHeight) + y) * sizeof(GLColour);
			m_Pixels.push_back(GLColour(rowPtrs[y][x * 4], rowPtrs[y][x * 4 + 1], rowPtrs[y][x * 4 + 2], rowPtrs[y][x * 4 + 3]));
		}
	}

	// Clean up
	png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
	free(imageData);
	free(rowPtrs);
	fclose(file);
	m_Dimensions.Set(iWidth, iHeight);
}