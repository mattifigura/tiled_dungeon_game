#ifndef VECTOR_H_
#define VECTOR_H_

// custom implementation of vectors done by Matti Figura :)

#include <unordered_map>
#include <cfloat>
#include <math.h>

#define PI 3.14159f
#define TWO_PI PI * 2.0f
#define PI_OVER_2 PI * 0.5f
#define PI_OVER_4 PI_OVER_2 * 0.5f
#define ONE_OVER_PI 0.3183101f
#define DEG_TO_RAD PI / 180.0f
#define RAD_TO_DEG 180.0f / PI

// 2d vector class
template<typename T>
class Vector3;

template<typename T>
class Vector2 {
public:
	Vector2() { x = 0; y = 0; }
	Vector2(const Vector2<T> &other) { Set(other); }
	Vector2(const Vector3<T> &other) { Set(other.x, other.y); }
	template<typename O> Vector2(const Vector2<O> &other) { Set((T)other.x, (T)other.y); }
	Vector2(T X, T Y) { Set(X, Y); }

	void Set(const Vector2<T> &other) { x = other.x; y = other.y; }
	void Set(T X, T Y) { x = X; y = Y; }

	void operator=(const Vector2<T> &other) { Set(other); }
	void operator=(const Vector3<T> &other) { Set(other.x, other.y); }
	Vector2<T> operator+(const Vector2<T> &other) const { return Vector2<T>(x + other.x, y + other.y); }
	Vector2<T> operator-(const Vector2<T> &other) const { return Vector2<T>(x - other.x, y - other.y); }
	Vector2<T> operator+(const Vector3<T> &other) const { return Vector2<T>(x + other.x, y + other.y); }
	Vector2<T> operator-(const Vector3<T> &other) const { return Vector2<T>(x - other.x, y - other.y); }
	void operator+=(const Vector2<T> &other) { Set(x + other.x, y + other.y); }
	void operator-=(const Vector2<T> &other) { Set(x - other.x, y - other.y); }
	void operator+=(const Vector3<T> &other) { Set(x + other.x, y + other.y); }
	void operator-=(const Vector3<T> &other) { Set(x - other.x, y - other.y); }
	bool operator==(const Vector2<T> &other) const { return (x == other.x && y == other.y); }
	bool operator!=(const Vector2<T> &other) const { return !operator==(other); }
	Vector2<T> operator*(const Vector2<T> &other) const { return Vector2<T>(x * other.x, y * other.y); }
	Vector2<T> operator*(const T other) const { return Vector2<T>(x * other, y * other); }
	void operator*=(const T other) { x *= other; y *= other; }
	Vector2<T> operator/(const T other) const { return Vector2<T>(x / other, y / other); }
	Vector2<T> operator/(const Vector2<T>& other) const { return Vector2<T>(x / other.x, y / other.y); }
	bool operator<(const Vector2<T> &other) const { return ((x < other.x) || (x == other.x && y < other.y)); }
	bool operator<=(const Vector2<T> &other) const { return ((x <= other.x) || (x == other.x && y <= other.y)); }
	bool operator>(const Vector2<T> &other) const { return ((x > other.x) || (x == other.x && y > other.y)); }
	bool operator>=(const Vector2<T> &other) const { return ((x >= other.x) || (x == other.x && y >= other.y)); }
	Vector2<T> operator-() const { Vector2<T> v(*this); v.MinusOne(); return v; }
	T& operator[](int i)
	{
		switch (i)
		{
			default:
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
				break;
		}
	}

	const T Magnitude() const { return sqrt(x * x + y * y); }
	Vector2<T>& Normal() { T Mag = Magnitude(); Set(x / Mag, y / Mag); return *this; }
	T Distance(const Vector2<T>& other) const { return (*this - other).Magnitude(); }

	void MinusOne() { x *= -1; y *= -1; }

	T x, y;

protected:

private:

};

// 3d vector class
template<typename T>
class Vector4;

template<typename T>
class Vector3 {
public:
	Vector3() { x = 0; y = 0; z = 0; }
	Vector3(const Vector4<T> &other) { Set(other.x, other.y, other.z); }
	Vector3(const Vector3<T> &other) { Set(other); }
	Vector3(const Vector2<T> &other) { Set(other.x, other.y, 0); }
	template<typename O> Vector3(const Vector3<O> &other) { Set((T)other.x, (T)other.y, (T)other.z); }
	Vector3(T X, T Y, T Z) { Set(X, Y, Z); }
	Vector3(T o) { Set(o, o, o); }

	void Set(const Vector3<T> &other) { x = other.x; y = other.y; z = other.z; }
	void Set(T X, T Y, T Z) { x = X; y = Y; z = Z; }

	void operator=(const Vector3<T> &other) { Set(other); }
	void operator=(const Vector2<T> &other) { Set(other.x, other.y, 0); }
	Vector3<T> operator+(const Vector3<T> &other) const { return Vector3<T>(x + other.x, y + other.y, z + other.z); }
	Vector3<T> operator-(const Vector3<T> &other) const { return Vector3<T>(x - other.x, y - other.y, z - other.z); }
	Vector3<T> operator+(const Vector2<T> &other) const { return Vector3<T>(x + other.x, y + other.y, z); }
	Vector3<T> operator-(const Vector2<T> &other) const { return Vector3<T>(x - other.x, y - other.y, z); }
	void operator+=(const Vector3<T> &other) { Set(x + other.x, y + other.y, z + other.z); }
	void operator-=(const Vector3<T> &other) { Set(x - other.x, y - other.y, z - other.z); }
	void operator+=(const Vector2<T> &other) { Set(x + other.x, y + other.y, z); }
	void operator-=(const Vector2<T> &other) { Set(x - other.x, y - other.y, z); }
	bool operator==(const Vector3<T> &other) const { return (x == other.x && y == other.y && z == other.z); }
	bool operator!=(const Vector3<T> &other) const { return !operator==(other); }
	Vector3<T> operator*(const Vector3<T> &other) const { return Vector3<T>(x * other.x, y * other.y, z * other.z); }
	Vector3<T> operator*(const T other) const { return Vector3<T>(x * other, y * other, z * other); }
	void operator*=(const  Vector3<T>& other) { Set(x * other.x, y * other.y, z * other.z); }
	void operator*=(const T other) { Set(x * other, y * other, z * other); }
	Vector3<T> operator/(const T other) const { return Vector3<T>(x / other, y / other, z / other); }
	bool operator<(const Vector3<T> &other) const { return ((x < other.x) || (x == other.x && y < other.y) || (x == other.x && y == other.y && z < other.z)); }
	bool operator<=(const Vector3<T> &other) const { return ((x <= other.x) || (x == other.x && y <= other.y) || (x == other.x && y == other.y && z <= other.z)); }
	bool operator>(const Vector3<T> &other) const { return ((x > other.x) || (x == other.x && y > other.y) || (x == other.x && y == other.y && z > other.z)); }
	bool operator>=(const Vector3<T> &other) const { return ((x >= other.x) || (x == other.x && y >= other.y) || (x == other.x && y == other.y && z >= other.z)); }
	Vector3<T> operator-() const { Vector3<T> v(*this); v.MinusOne(); return v; }
	T& operator[](int i)
	{
		switch (i)
		{
			default:
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
			case 2:
				return z;
				break;
		}
	}

	void Cross(const Vector3<T> &other) { Set((y * other.z) - (z * other.y), (z * other.x) - (x * other.z), (x * other.y) - (y * other.x)); }
	Vector3<T> CrossProduct(const Vector3<T>& other) const { return Vector3<T>((y * other.z) - (z * other.y), (z * other.x) - (x * other.z), (x * other.y) - (y * other.x)); }
	const T Magnitude() const { return sqrt(x * x + y * y + z * z); }
	Vector3<T>& Normal() { T Mag = Magnitude(); Set(x / Mag, y / Mag, z / Mag); return *this; }
	T Dot(const Vector3<T> &other) const { return ((x * other.x) + (y * other.y) + (z * other.z)); }
	Vector3<T> OneOverNormal() const { Vector3<T> ret(*this); ret.Normal(); ret.Set(1.0f / (ret.x + FLT_MIN), 1.0f / (ret.y + FLT_MIN), 1.0f / (ret.z + FLT_MIN)); return ret; }
	T Distance(const Vector3<T> &other) const { return (*this - other).Magnitude(); }
	T QuickDistance(const Vector3<T> &other) const
	{
		T _x = x - other.x;
		T _y = y - other.y;
		T _z = z - other.z;

		return (_x * _x + _y * _y + _z * _z);
	}
	Vector3<T>& Abs() { Set(abs(x), abs(y), abs(z)); return *this; }

	void MinusOne() { x *= -1; y *= -1; z *= -1; }

	T x, y, z;

protected:

private:

};

// 4d vector class
template<typename T>
class Vector4 {
public:
	Vector4() { x = 0; y = 0; z = 0; w = 0; }
	Vector4(const Vector4<T> &other) { Set(other); }
	Vector4(const Vector3<T> &other) { Set(other.x, other.y, other.z, 0); }
	Vector4(const Vector2<T> &other) { Set(other.x, other.y, 0, 0); }
	Vector4(T X, T Y, T Z, T W) { Set(X, Y, Z, W); }
	Vector4(T vals[]) { Set(vals); }

	void Set(const Vector4<T> &other) { x = other.x; y = other.y; z = other.z; w = other.w; }
	void Set(T X, T Y, T Z, T W) { x = X; y = Y; z = Z; w = W; }
	void Set(T vals[]) { Set(vals[0], vals[1], vals[2], vals[3]); }

	void operator=(const Vector4<T> &other) { Set(other); }
	Vector4<T> operator+(const Vector4<T> &other) const { return Vector4<T>(x + other.x, y + other.y, z + other.z, w + other.w); }
	Vector4<T> operator-(const Vector4<T> &other) const { return Vector4<T>(x - other.x, y - other.y, z - other.z, w + other.w); }
	void operator+=(const Vector4<T> &other) { Set(x + other.x, y + other.y, z + other.z, w + other.w); }
	void operator-=(const Vector4<T> &other) { Set(x - other.x, y - other.y, z - other.z, w + other.w); }
	bool operator==(const Vector4<T> &other) const { return (x == other.x && y == other.y && z == other.z && w == other.w); }
	bool operator!=(const Vector4<T> &other) const { return !operator==(other); }
	Vector4<T> operator*(const Vector4<T> &other) const { return Vector4<T>(x * other.x, y * other.y, z * other.z, w * other.w); }
	Vector4<T> operator*(const T other) const { return Vector4<T>(x * other, y * other, z * other, w * other); }
	void operator*=(const T other) { x *= other; y *= other; z *= other; w *= other; }
	Vector4<T> operator/(const T other) const { return Vector4<T>(x / other, y / other, z / other, w / other); }
	Vector4<T> operator/(const Vector4<T>& other) const { return Vector4<T>(x / other.x, y / other.y, z / other.z, w / other.w); }
	T& operator[](int i)
	{
		switch (i)
		{
			default:
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
			case 2:
				return z;
				break;
			case 3:
				return w;
				break;
		}
	}

	void MinusOne() { x *= -1; y *= -1; z *= -1; w *= -1; }
	void ToBuffer(T b[4]) const { b[0] = x; b[1] = y; b[2] = z; b[3] = w; }

	T x, y, z, w;

protected:

private:

};

// quaternion class
class quaternion {
public:
	quaternion() { x = 0; y = 0; z = 0; w = 0; }
	quaternion(const quaternion& other) { Set(other); }
	quaternion(const Vector4<float>& other) { Set(other); }
	quaternion(float X, float Y, float Z, float W) { Set(X, Y, Z, W); }

	void Set(const quaternion& other) { w = other.w; x = other.x; y = other.y; z = other.z; }
	void Set(const Vector4<float>& other) { w = other.w; x = other.x; y = other.y; z = other.z; }
	void Set(float X, float Y, float Z, float W) { x = X; y = Y; z = Z; w = W; }
	void Set(float vals[]) { Set(vals[0], vals[1], vals[2], vals[3]); }

	quaternion operator*(const quaternion& other) const
	{
		quaternion out;

		out.x = w * other.x + x * other.w + y * other.z - z * other.y;
		out.y = w * other.y - x * other.z + y * other.w + z * other.x;
		out.z = w * other.z + x * other.y - y * other.x + z * other.w;
		out.w = w * other.w - x * other.x - y * other.y - z * other.z;

		return out;
	}

	float Magnitude() const { return sqrt(x * x + y * y + z * z + w * w); }
	quaternion& Normalise()
	{
		float mag = 1.0f / Magnitude();

		x *= mag;
		y *= mag;
		z *= mag;
		w *= mag;

		return *this;
	}
	quaternion Conjugate() const
	{
		quaternion out(-x, -y, -z, w);
		return out;
	}
	void SetRotation(float angle, const Vector3<float>& axis)
	{
		float _angle = angle * 0.5f;

		w = cos(_angle);
		x = axis.x * sin(_angle);
		y = axis.y * sin(_angle);
		z = axis.z * sin(_angle);
	}
	const bool operator!() const { return (x == 0 && y == 0 && z == 0 && w == 0); }
	void operator=(const quaternion& quat) { x = quat.x; y = quat.y; z = quat.z; w = quat.w; }

	float x, y, z, w;

protected:

private:

};

typedef Vector2<int> 	vector2di;
typedef Vector2<short>	vector2ds;
typedef Vector2<float>	vector2df;
typedef Vector2<double>	vector2dd;
typedef Vector2<char>	vector2dc;

typedef Vector3<int> 	vector3di;
typedef Vector3<short>	vector3ds;
typedef Vector3<float>	vector3df;
typedef Vector3<double>	vector3dd;
typedef Vector3<char>	vector3dc;

typedef Vector4<int> 	vector4di;
typedef Vector4<short>	vector4ds;
typedef Vector4<float>	vector4df;
typedef Vector4<double>	vector4dd;
typedef Vector4<char>	vector4dc;

// hash key for vector3di
namespace std
{
	template <> struct hash<vector3di>
	{
		size_t operator()(const vector3di& x) const
		{
			return ((x.x * 73856093) ^ (x.y * 19349663) ^ (x.z * 83492791));
		}
	};
}

#endif
