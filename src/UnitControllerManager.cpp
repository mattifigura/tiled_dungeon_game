#include "UnitControllerManager.h"
#include "StatusEffectManager.h"
#include "CameraController.h"

UnitControllerManager::UnitControllerManager()
	: m_CurrentController(nullptr)
	, m_CurrentIndex(0)
{
	REGISTER_INPUT_OBJECT;
}

void UnitControllerManager::Update()
{
	// Ensure we have a controller
	if (m_CurrentController)
	{
		m_CurrentController->Update();
	}
	
	// Update the camera
	Singleton<CameraController>::Instance().Update();
	
	// Update the other controllers in the background
	for (auto& controller : m_UnitControllers) {
		if (controller.get() != m_CurrentController)
			controller->UpdateBackground();
	}
}

void UnitControllerManager::AddUnitController(UnitController* controller)
{
	// Add a controller
	m_UnitControllers.push_back(std::shared_ptr<UnitController>(controller));
	
	if (!m_CurrentController)
	{
		m_CurrentController = controller;
	}
}

UnitController* UnitControllerManager::GetUnitController(size_t id)
{
	return m_UnitControllers.at(id).get();
}

void UnitControllerManager::NextController()
{
	if (++m_CurrentIndex >= m_UnitControllers.size())
	{
		m_CurrentIndex = 0;
	}
	
	m_CurrentController->TurnEnd();
	m_CurrentController = m_UnitControllers.at(m_CurrentIndex).get();
	m_CurrentController->TurnStart();
	
	Singleton<CameraController>::Instance().SetCurrentController(m_CurrentController->GetType());
	
	Singleton<StatusEffectManager>::Instance().TurnStart(m_CurrentController->GetType());
}

bool UnitControllerManager::MouseEvent(int button, int action, int mods)
{
	// Ensure we have a controller
	if (m_CurrentController)
	{
		return m_CurrentController->MouseEvent(button, action, mods);
	}
	
	return false;
}

bool UnitControllerManager::KeyboardEvent(int key, int scancode, int action, int mods)
{
	// Ensure we have a controller
	if (m_CurrentController)
	{
		return m_CurrentController->KeyboardEvent(key, scancode, action, mods);
	}
	
	return false;
}

bool UnitControllerManager::CharEvent(unsigned int codepoint)
{
	// Ensure we have a controller
	if (m_CurrentController)
	{
		return m_CurrentController->CharEvent(codepoint);
	}
	
	return false;
}

bool UnitControllerManager::ScrollEvent(double x, double y)
{
	// Check our camera controller first as this takes precedence
	if (Singleton<CameraController>::Instance().ScrollEvent(x, y))
		return true;
	
	// Ensure we have a controller
	if (m_CurrentController)
	{
		return m_CurrentController->ScrollEvent(x, y);
	}
	
	return false;
}
