#ifndef SCENE_MANAGER_H_
#define SCENE_MANAGER_H_

#include "Billboard.h"
#include "ResourceManager.h"
#include "SceneNode.h"
#include "Mesh.h"
#include "GLTexture.h"
#include "Camera.h"
#include "Terrain.h"
#include "GLShaderProgram.h"
#include "DeltaTime.h"
#include "LightManager.h"
#include "ParticleEmitter.h"
#include "ParticleDefinitionLibrary.h"
#include "FastMutex.h"
#include <vector>
#include <memory>
#include <atomic>
#include <thread>

typedef std::vector<std::unique_ptr<SceneNode>> NodeVector;
typedef std::vector<std::unique_ptr<Camera>> CameraVector;
typedef std::vector<std::unique_ptr<GLShaderProgram>> ShaderVector;
typedef std::vector<std::unique_ptr<ParticleEmitter>> EmitterVector;

// SceneManager class - manages SceneNodes in a current scene, and has a current Camera object
// This is a custom version also including pointers to the Terrain and Player classes

class SceneManager {
public:
	SceneManager();
	~SceneManager();

	void Initialise();
	void Update();
	void Render();
	void RenderMeshes(const std::vector<MeshBase*>& meshes);

	void UseTexture(GLuint texture);

	void AddCamera();
	void SetActiveCamera(int camera);
	const Camera& GetActiveCamera() const;
	Camera& GetActiveCamera();

	const GLTexture& GetTexture(const std::string& filename);
	const MeshBase& GetMesh(const std::string& key);

	void GetRayFromScreenCoords(line3df& out);

	// Creation functions
	SceneNode& CreateSceneNode();
	Billboard& CreateBillboard();
	Light* CreateLight(const vector2df& pos, const GLColour& col, const vector2df& size, bool reserved = false);
	ParticleEmitter* CreateParticleEmitter(const vector3df& pos, size_t size);
	ParticleEmitter* CreateParticleEmitter(const vector3df& pos, const ParticleDefinition& definition);

	// Custom functions for this voxel game to make things easier
	const Terrain& GetTerrain() const;
	Terrain& GetTerrain();

protected:

private:
	NodeVector						m_SceneNodes;
	ResourceManager<MeshBase>		m_MeshMan;
	ResourceManager<GLTexture>		m_TexMan;
	CameraVector					m_Cameras;
	int								m_ActiveCamera;
	ShaderVector					m_Shaders;
	GLuint							m_CurrentActiveTexture;
	std::unique_ptr<LightManager>	m_pLightMan;
	EmitterVector					m_Emitters;
	std::atomic_bool				m_Running;
	FastMutex						m_ParticleMutex;
	std::thread						m_ParticleThread;

	// Shader attrib locations
	int								m_iPositionAttrib;
	int								m_iColourAttrib;
	int								m_iTextureAttrib;
	int								m_iParticlePositionAttrib;
	int								m_iParticleColourAttrib;
	int								m_iParticleSizeAttrib;
	
	// Shader uniform locations
	int								m_iModelUniform;
	int								m_iViewUniform;
	int								m_iProjectionUniform;
	int								m_iTexImageUniform;
	int								m_iLightMapUniform;
	int								m_iOutlineUniform;
	int								m_iParticleViewUniform;
	int								m_iParticleProjectionUniform;
	int								m_iParticleLightMapUniform;
	int								m_iParticleLightingUniform;

	void EnableVertexAttributes();
	void DisableVertexAttributes();
	void StartRender();
	void FinishRender();
	void RenderMesh(const MeshBase* mesh, int mode = GL_TRIANGLES);
	void ParticleThreadUpdate();

	// Custom pointers for this voxel game to make things easier
	std::unique_ptr<Terrain>		m_pTerrain;
};

#endif // SCENE_MANAGER_H_
