#ifndef GAME_SCENE_H_
#define GAME_SCENE_H_

#include "SceneManager.h"
#include "GUIManager.h"
#include "UnitControllerManager.h"

// GameScene class - essentially this controls the creation and running of the game

class GameScene {
public:
	GameScene();

	void CreateScene();
	void Run();

protected:

private:
	std::unique_ptr<SceneManager>			m_pSceneMan;
	std::unique_ptr<GUIManager>				m_pGUIMan;
	std::unique_ptr<UnitControllerManager>	m_UnitControllerManager;
	float									m_fSun;
	GUIText*								tmp;
	GUIText*								m_ControllerMouseText;
	std::vector<GUICharacterPane*>			m_Panes;
	size_t									m_CurrentCharacter;
	size_t									m_PreviousCharacter;
	int										m_CurrentAction;
	int										m_PreviousAction;
	std::vector<GUIAction*>					m_Actions;
	
	std::chrono::time_point<std::chrono::system_clock>	m_FPSStartTime;
	int													m_FPSFrames;
	float												m_FPS;
	GUIText*											m_FPSText;
	
	void UpdateCharacterGUI();
	void UpdateActionAbility(size_t id, Ability& ability, const std::string& name, nsUnitClass::UnitClass unit_class);
};

#endif // GAME_SCENE_H_
