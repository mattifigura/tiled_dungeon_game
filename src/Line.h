#ifndef LINE_H_
#define LINE_H_

// custom implementation of lines done by Matti Figura :)
#include "Vector.h"

template<typename T>
class Line3 {
public:
	Line3() { start = vector3df(); end = vector3df(); }
	Line3(const Vector3<T> &s, const Vector3<T> &e) { Set(s, e); }
	Line3(T xs, T ys, T zs, T xe, T ye, T ze) { start.Set(xs, ys, zs); end.Set(xe, ye, ze); }

	void Set(const Vector3<T> &s, const Vector3<T> &e) { start.Set(s); end.Set(e); }

	Line3<T> operator-(const Line3<T> &other) const { return Line3<T>(start - other.start, end - other.end); }
	Line3<T> operator+(const Line3<T> &other) const { return Line3<T>(start + other.start, end + other.end); }
	void operator-=(const Line3<T> &other) { Set(start - other.start, end - other.end); }
	void operator+=(const Line3<T> &other) { Set(start + other.start, end + other.end); }
	bool operator==(const Line3<T> &other) const { return (start == other.start && end == other.end); }
	bool operator!=(const Line3<T> &other) const { return !operator==(other); }

	T Magnitude() const { return (end - start).Magnitude(); }

	Vector3<T> start, end;

protected:

private:

};

typedef Line3<int>		line3di;
typedef Line3<short>	line3ds;
typedef Line3<float>	line3df;

#endif