#include "SceneManager.h"
#include "UnitController.h"
#include "UnitControllerManager.h"
#include "DataFile.h"
#include <algorithm>
#include <fstream>

PlayerController* UnitController::m_PlayerController = nullptr;
EnemyController* UnitController::m_EnemyController = nullptr;

UnitController::UnitController(SceneManager* sceneman, UnitControllerManager* man, nsControllerNames::Controller type)
	: m_Type(type)
	, m_SceneMan(sceneman)
	, m_Manager(man)
	, m_PathFinder(vector2di(AREA_SIZE, AREA_SIZE))
	, m_AbilityController(this)
	, m_UnitsDie(true)
{
	// Create our billboard
	m_Billboard = &m_SceneMan->CreateBillboard();
	
	// Ensure we always have a texture available
	m_Billboard->SetTexture("media/characters.png");
}

UnitController::~UnitController()
{
	std::vector<Unit*>::iterator it = m_Units.begin();
	while (it != m_Units.end())
	{
		delete *it;
		it = m_Units.erase(it);
	}
}

void UnitController::CreateEffectsBillboard()
{
	// Effects
	m_Effects = &m_SceneMan->CreateBillboard();
	m_Effects->SetTexture("media/effects.png");
	m_Effects->AddAnimation(vector2di(), vector2di(7, 0), vector2df(8.f, 8.f));
	m_Effects->AddBoard();
	m_Effects->SetHideOnAnimationFinish(true);
	m_Effects->SetOutline(true);
}

void UnitController::AddUnit(Unit* unit)
{
	// Add a unit
	if (unit)
	{
		unit->m_Board = m_Billboard->AddBoard();
		unit->CreateUnit();
		m_Units.push_back(unit);
	}
}

Unit* UnitController::CreateUnit(const std::string& name, const vector3df& pos)
{
	// Create a unit
	const UnitData& data = GetUnitData(name);
	Unit* unit = new Unit(GetType());
	unit->m_Data = data;
	unit->m_Name = name;
	unit->SetPosition(pos);
	unit->m_Board = m_Billboard->AddBoard();
	unit->CreateUnit();
	unit->SetAnimation(data.animation);
	unit->m_Health = data.health;
	m_Units.push_back(unit);
	
	return unit;
}

Unit* UnitController::GetUnit(const vector2di& pos)
{
	for (auto& unit : m_Units) {
		if (vector2di(unit->GetPosition().x, unit->GetPosition().z) == pos)
			return unit;
	}
	
	return nullptr;
}

Unit* UnitController::GetClosestUnit(const vector3df& pos, const std::vector<Unit*>* previous)
{
	int dist = 9001;
	Unit* ret = nullptr;
	
	for (auto& unit : m_Units) {
		int temp = Distance(pos, unit->GetPosition());
		if (temp < dist && unit->GetHealth() > 0 && (!previous || std::find(previous->begin(), previous->end(), unit) == previous->end()))
		{
			dist = temp;
			ret = unit;
		}
	}
	
	return ret;
}

void UnitController::Update()
{
	// Call this from derived classes to keep units in position
	// Check if there needs to be any different processing from UpdateBackground?
	// Note am calling UpdateBackground due to the functions doing the same thing currently
	UpdateBackground();
}

void UnitController::UpdateBackground()
{
	// Call this from derived classes to keep units in position
	// Note if adding functionality not designed for foreground updating
	// then need to move common code into new function and change the Update method
	std::vector<Unit*>::iterator it = m_Units.begin();
	while (it != m_Units.end())
	{
		(*it)->Update();
		
		if ((*it)->GetHealth() <= 0)
		{
			(*it)->m_Board->position.y = 9001.f;
			
			if (m_UnitsDie)
			{
				delete *it;
				it = m_Units.erase(it);
			}
			else ++it;
		}
		else ++it;
	}
	
	m_AbilityController.Update();
}

void UnitController::LoadUnitData(const std::string& filename)
{
	// Load the file
	DataFile file(filename);
	
	// String to status effect
	auto string_to_unit_class = [](const std::string& in) -> nsUnitClass::UnitClass {
		nsUnitClass::UnitClass val(nsUnitClass::melee);
		
		if (in == "Melee")
			val = nsUnitClass::melee;
		else if (in == "Mage")
			val = nsUnitClass::mage;
		else if (in == "Tank")
			val = nsUnitClass::tank;
		else if (in == "Rogue")
			val = nsUnitClass::rogue;
		else if (in == "Archer")
			val = nsUnitClass::archer;
		else if (in == "Summoner")
			val = nsUnitClass::summoner;
		
		return val;
	};
	
	// Enum to allow switching on a string in a pretty way
	enum AttributeName {
		attr_class = 0,
		attr_health,
		attr_movement,
		attr_action_count,
		attr_modifier,
		attr_frame_start,
		attr_frame_end,
		attr_frame_size
	};
	
	// Table to allow switching on a string
	std::map<std::string, AttributeName> get_attribute = {
		{ "class", attr_class },
		{ "health", attr_health },
		{ "movement", attr_movement },
		{ "action count", attr_action_count },
		{ "value", attr_modifier },
		{ "frame start", attr_frame_start },
		{ "frame end", attr_frame_end },
		{ "frame size", attr_frame_size }
	};
	
	// New way
	while (file.IsOpen())
	{
		UnitData data;
		std::string name;
		int ability = 0;
		nsDataFileRead::DataFileRead read_status = nsDataFileRead::empty;
		vector2di start, end;
		vector2df frame(8.f, 8.f);
		
		while ((read_status = file.Read()) != nsDataFileRead::empty && read_status != nsDataFileRead::end_of_file)
		{
			switch (read_status)
			{
				case nsDataFileRead::section:
				{
					switch (file.GetDepth())
					{
						case 0: // Name of the unit
							name = file.GetAttributeName();
							break;
						case 1: // Must be an ability
							data.abilities[ability++] = file.GetAttributeName();
							break;
						default:
							// Do nothing
							break;
					}
				}
				break;
				case nsDataFileRead::attribute:
				{
					switch (file.GetDepth())
					{
						case 1:
						{
							// Unit attribute
							switch (get_attribute[file.GetAttributeName()])
							{
								case attr_class:
									data.unit_class = string_to_unit_class(file.ReadString());
									break;
								case attr_health:
									data.health = file.ReadInt();
									break;
								case attr_movement:
									data.movement = file.ReadInt();
									break;
								case attr_action_count:
									data.action_count = file.ReadInt();
									break;
								case attr_frame_start:
									start = file.ReadVector2<int>();
									break;
								case attr_frame_end:
									end = file.ReadVector2<int>();
									break;
								case attr_frame_size:
									frame = file.ReadVector2<float>();
									break;
								default:
									// Does not apply here
									break;
							}
						}
						break;
						case 2:
						{
							// Ability attribute
							// For now there is only 1 - modifier
							data.ability_values[ability - 1] = file.ReadInt();
						}
						default:
							// Do nothing
							break;
					}
				}
				break;
				default:
					// Do nothing
					break;
			}
		}
		
		if (!name.empty())
		{
			data.animation = AddUnitAnimation(start, end, frame);
			m_UnitDataMap.emplace(name, data);
		}
	}
}

const UnitData& UnitController::GetUnitData(const std::string& name) const
{
	// Find the data in the map
	auto found = m_UnitDataMap.find(name);
	if (found != m_UnitDataMap.end())
		return found->second;
	
	// Doesn't exist, return default data
	static UnitData empty;
	return empty;
}

void UnitController::PlayEffect(int id, const vector3df& pos, float speed)
{
	// Play an effect
	Board* board = m_Effects->GetBoard(0);
	board->size.Set(vector2df(0.5f, 0.5f));
	board->position = pos;
	board->animation = id;
	board->animation_speed = speed;
	board->frame = 0.f;
}

vector2di UnitController::GetUnitPortrait(Unit* unit)
{
	// Usual units are 8x8 sprite, override if need any different sizes
	vector2df uv(m_Billboard->GetAnimation(unit->GetUnitData().animation).frames.at(0));
	vector2df scale(m_Billboard->GetTexture()->Dimensions() / 8.f);
	return vector2di(uv.x * scale.x, uv.y * scale.y);
}

void UnitController::GetUnitCounts(size_t id, int& act, int& max_act, int& step) const
{
	const UnitData& data = GetUnit(id)->GetUnitData();
	act = data.action_count;
	max_act = data.action_count; // Will need to be overriden
	step = data.movement;
}

void UnitController::SetUnitData(int id, UnitData& data)
{
	m_Units.at(id)->m_Data = data;
}

int UnitController::Distance(Unit* one, Unit* two) const
{
	return Distance(one->GetPosition(), two->GetPosition());
}

int UnitController::Distance(const vector3df& one, const vector3df& two) const
{
	// Chebyshev distance
	vector2di pos1(one.x, one.z);
	vector2di pos2(two.x, two.z);
	
	return (std::max(abs(pos2.x - pos1.x), abs(pos2.y - pos1.y)));
}

int UnitController::Distance(const vector2di& one, const vector2di& two) const
{
	return (std::max(abs(two.x - one.x), abs(two.y - one.y)));
}

// Call this after assigning the correct texture to the billboard
int UnitController::AddUnitAnimation(const vector2di& start, const vector2di& end, const vector2df& size)
{
	int ret = m_Billboard->AddAnimation(start, end, size);
	
	// Now scan the texture and add the pixel colours to an array for this unit
	const GLTexture* tex = m_Billboard->GetTexture();
	if (tex)
	{
		std::vector<GLColour> cols;
		
		vector2di image_size(size);
		vector2di first_pixel(start * image_size);
		int count = image_size.x * image_size.y;
		
		for (int i = 0; i < count; ++i) {
			GLColour col = tex->GetPixel(first_pixel.x + (i % image_size.x), first_pixel.y + (i / image_size.x));
			
			if (col.a > 0 && std::find(cols.begin(), cols.end(), col) == cols.end())
				cols.push_back(col);
		}
		
		cols.push_back(GLColour(140, 30, 5, 200));
		m_UnitColours.push_back(cols);
	}
	
	return ret;
}

// If this returns true, the unit has been deleted and should not be used anymore
bool UnitController::DamageUnit(Unit* unit, int damage, Unit* other)
{
	bool killed(false);
	
	if (unit->Damage(damage, other))
	{
		// Create a particle emitter to animate the death
		ParticleEmitter* emitter = GetSceneManager()->CreateParticleEmitter(unit->GetPosition() + vector3df(0.f, 0.1f, 0.f), 25);
		emitter->Burst();
		
		EmitterBehaviour& behaviour = emitter->GetBehaviour();
		behaviour.start_size.Set(0.0625f, 0.0625f);
		behaviour.end_size.Set(0.0625f, 0.0625f);
		behaviour.force.Set(0.f, 1.75f, 0.f);
		behaviour.force_randomiser_lower.Set(-0.5f, -0.1f, -0.5f);
		behaviour.force_randomiser_upper.Set(0.5f, 0.1f, 0.5f);
		behaviour.gravity.Set(0.f, -9.f, 0.f);
		behaviour.friction.Set(2.5f, 2.5f, 2.5f);
		behaviour.collisions = true;
		behaviour.bounce.Set(-0.1f, -0.4f, -0.1f);
		behaviour.duration = 5000.f;
		behaviour.only_live_once = true;
		
		// Assign random colours from our unit
		const std::vector<GLColour>& cols = m_UnitColours.at(unit->GetBoard()->animation);
		behaviour.random_colours.resize(cols.size());
		behaviour.random_colours.assign(cols.begin(), cols.end());
		
		killed = true;
	}
	
	return killed;
}

bool UnitController::HealUnit(Unit* unit, int heal, Unit* other)
{
	bool healed(false);
	
	if (unit->Heal(heal, other))
	{
		// Create a particle emitter to show a nice heal :)
		ParticleEmitter* emitter = GetSceneManager()->CreateParticleEmitter(vector3df(unit->GetPosition().x, 0.1f, unit->GetPosition().z), 25);
		
		EmitterBehaviour& behaviour = emitter->GetBehaviour();
		behaviour.start_colour.Set(30, 240, 50, 255);
		behaviour.end_colour.Set(30, 240, 50, 80);
		behaviour.position_randomiser_lower.Set(-0.25f, 0.f, -0.25f);
		behaviour.position_randomiser_upper.Set(0.25f, 0.1f, 0.25f);
		behaviour.start_size.Set(0.05f, 0.05f);
		behaviour.end_size.Set(0.03f, 0.03f);
		behaviour.force.Set(0.f, 0.75f, 0.f);
		behaviour.duration = 600.f;
		behaviour.duration_randomiser = 600.f;
		behaviour.only_live_once = true;
		
		emitter->SetAffectedByLighting(false);
		
		healed = true;
	}
	
	return healed;
}

void UnitController::UpdateUnitGridPositions()
{
	m_UnitPlaces.clear();
	for (auto& unit : m_Units) {
		if (unit->GetHealth() > 0)
			m_UnitPlaces.push_back(vector2di(unit->GetPosition().x, unit->GetPosition().z));
	}
}
