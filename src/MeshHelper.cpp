#include "MeshHelper.h"
#include "Mesh.h"
#include <fstream>

// .obj file loading
// Helper functions
static void ReadFloat(float& out, const std::string& line, int& offset)
{
	// used in OBJ loading to read a float
	size_t place = line.find(' ', offset + 1);
	if (place == std::string::npos)
		place = line.length();
	int length = -offset + static_cast<int>(place);
	out = (float)atof(line.substr(offset, length).c_str());
	offset += length + 1;
}

static bool IsNear(const float& one, const float& two)
{
	return (abs(one - two) < 0.01f);
}

static bool DoesVertexExist(const std::vector<GLVertex>& verts, const vector3df& pos, const vector2df& uv, GLuint& index)
{
	for (GLuint i = 0; i < verts.size(); ++i) {
		if (IsNear(verts[i].pos.x, pos.x) &&
			IsNear(verts[i].pos.y, pos.y) &&
			IsNear(verts[i].pos.z, pos.z) &&
			IsNear((GLfloat)verts[i].uv.u / 8192.f, uv.x) &&
			IsNear((GLfloat)verts[i].uv.v / 8192.f, uv.y))
		{
			index = i;
			return true;
		}
	}

	return false;
}

static GLuint CountChar(const std::string& str, const char c)
{
	int count = 0;

	for (unsigned int i = 0; i < str.length(); ++i) {
		if (str[i] == c)
			++count;
	}

	return count;
}

static void Limit(float &var, float lower, float upper)
{
	if (var < lower)
	{
		var = lower;
	}
	else if (var > upper)
	{
		var = upper;
	}
}

// Function to read in an .obj file and create a mesh
MeshBase* MeshHelper::CreateMesh(const std::string& filename)
{
	// Load a mesh from a .obj file
	std::vector<vector3df> vertices;
	std::vector<vector2df> uvs;
	std::vector<GLVertex> meshVerts;

	// Create a new mesh
	Mesh<GLVertex>* mesh = new Mesh<GLVertex>;

	// Open the file for reading
	std::ifstream file(filename, std::ios::in);

	// If the file isn't open return an empty mesh
	if (!file.is_open())
		return mesh;

	// Read the data into the vectors to assemble later
	std::string line;
	while (std::getline(file, line))
	{
		if (line[0] == 'v')
		{
			if (line[1] == 't')
			{
				// uv coordinates
				float x, y;
				int offset = 3;

				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				Limit(x, 0.0f, 1.0f);
				Limit(y, 0.0f, 1.0f);

				uvs.push_back(vector2df(x, y));
			}
			else
			{
				// vertex positions
				float x, y, z;
				int offset = 2;

				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				ReadFloat(z, line, offset);

				vertices.push_back(vector3df(x, y, z));
			}
		}
		else if (line[0] == 'f')
		{
			// faces
			GLuint count = CountChar(line, '/') / 2;
			int vi = 0, ui = 0, offset = 2, tmp = 0;

			for (unsigned int i = 0; i < count; ++i) {
				tmp = static_cast<int>(line.find('/', offset));
				vi = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
				offset += (tmp - offset) + 1;

				if (uvs.size() > 0)
				{
					tmp = static_cast<int>(line.find('/', offset));
					ui = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
					offset += (tmp - offset) + 1;
				}
				else ++offset;

				tmp = static_cast<int>(line.find(' ', offset));

				if (line.find(' ', offset) == std::string::npos)
					tmp = static_cast<int>(line.length());

				offset += (tmp - offset) + 1;

				GLuint index = static_cast<GLuint>(meshVerts.size());
				vector2df uv;
				if (uvs.size() > 0)
					uv = uvs[ui];
				if (count == 3)
				{
					if (!DoesVertexExist(meshVerts, vertices[vi], uv, index))
						meshVerts.push_back(GLVertex(vertices[vi], GLColour(), uv));

					mesh->AddIndex(index);
				}
				else
				{
					meshVerts.push_back(GLVertex(vertices[vi], GLColour(), uv));
				}
			}

			if (count > 3)
			{
				GLuint index = static_cast<GLuint>(meshVerts.size());
				GLuint start = index - count;
				for (GLuint i = 1; i < count - 1; ++i) {
					mesh->AddIndex(start); mesh->AddIndex(start + i); mesh->AddIndex(start + i + 1);
				}
			}
		}
	}
	file.close();

	// Add the data and build the buffer
	mesh->CopyVertices(meshVerts);
	mesh->Buffer();

	// Return this mesh
	return mesh;
}