#include "GLObject.h"

template<typename T>
GLObject<T>::GLObject()
	: m_bufVBO(0u)
	, m_bufEBO(0u)
{

}

template<typename T>
GLObject<T>::~GLObject()
{
	ClearBuffers();
}

template<typename T>
bool GLObject<T>::BufferData(const std::vector<T>& VBO, const std::vector<GLuint>& EBO)
{
	// Firstly clear down our existing buffers
	ClearBuffers();
	
	// Attempt to generate buffers and buffer data
	glGenBuffers(1, &m_bufVBO);
	if (VALID_BUFFER(m_bufVBO))
	{
		// We have a VBO, fill the data
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBufferData(GL_ARRAY_BUFFER, VBO.size() * sizeof(T), &VBO[0], GL_STATIC_DRAW);

		// Now try the element buffer
		glGenBuffers(1, &m_bufEBO);
		if (VALID_BUFFER(m_bufEBO))
		{
			// We now have our VBO and EBO, fill the EBO with data
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, EBO.size() * sizeof(GLuint), &EBO[0], GL_STATIC_DRAW);

			// Unbind the buffers and return success
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			return true;
		}

		// We have failed, clear our VBO and delete it
		ClearBuffers();
	}

	// We have failed to generate buffers, return false
	return false;
}

template<typename T>
bool GLObject<T>::UpdateVBO(const std::vector<T>& VBO, size_t size)
{
	// Check we have a valid buffer
	if (VALID_BUFFER(m_bufVBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, (size > 0 ? size : VBO.size()) * sizeof(T), &VBO[0]);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Return success
		return true;
	}

	// We have been unsuccessful
	return false;
}

template<typename T>
GLuint GLObject<T>::GetVBO() const
{
	return m_bufVBO;
}

template<typename T>
GLuint GLObject<T>::GetEBO() const
{
	return m_bufEBO;
}

template<typename T>
void GLObject<T>::ClearBuffers()
{
	// Clear our buffers
	if (VALID_BUFFER(m_bufVBO))
		glDeleteBuffers(1, &m_bufVBO);

	if (VALID_BUFFER(m_bufEBO))
		glDeleteBuffers(1, &m_bufEBO);

	// Reset the buffer pointers to null
	m_bufVBO = 0u;
	m_bufEBO = 0u;
}