#include "ResourceManager.cpp"
#include "Mesh.h"
#include "GLTexture.h"
#include "Font.h"

// Implementation file for the ResourceManager template class

template ResourceManager<MeshBase>::ResourceManager(MeshBase*(*func)(const std::string& key));
template ResourceManager<MeshBase>::~ResourceManager();
template void ResourceManager<MeshBase>::Clear();
template const MeshBase& ResourceManager<MeshBase>::GetResource(const std::string& key);

template ResourceManager<GLTexture>::ResourceManager(GLTexture*(*func)(const std::string& key));
template ResourceManager<GLTexture>::~ResourceManager();
template void ResourceManager<GLTexture>::Clear();
template const GLTexture& ResourceManager<GLTexture>::GetResource(const std::string& key);

template ResourceManager<Font>::ResourceManager(Font*(*func)(const std::string& key));
template ResourceManager<Font>::~ResourceManager();
template void ResourceManager<Font>::Clear();
template const Font& ResourceManager<Font>::GetResource(const std::string& key);
