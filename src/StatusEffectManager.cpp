#include "StatusEffectManager.h"

StatusEffectManager::StatusEffectManager()
{
}

StatusEffectManager::~StatusEffectManager()
{
	Kill();
}

void StatusEffectManager::Kill()
{
	for (auto& effects : m_StatusEffects) {
		for (auto& effect : effects.second) {
			if (effect.effect && (effect.owner || effect.self_owned))
				delete effect.effect;
			
			effect.effect = nullptr;
		}
	}
	
	m_StatusEffects.clear();
}

void StatusEffectManager::AddStatusEffect(size_t target, size_t owner, StatusEffect* effect)
{
	// Add this effect to the target's effect vector
	bool is_owner(target == owner);
	StatusEffectVector& effects = GetEffectVector(target);
	
	EffectDetails details;
	details.effect = effect;
	details.self_owned = is_owner;
	
	effects.push_back(details);
	
	if (!is_owner)
	{
		// Add this effect to the owner's effect vector
		StatusEffectVector& owner_effects = GetEffectVector(owner);
		
		EffectDetails owner_details;
		owner_details.effect = effect;
		owner_details.owner = true;
		
		owner_effects.push_back(owner_details);
	}
}

void StatusEffectManager::UnitKilled(size_t id)
{
	// Remove the status effects from this unit and any owners affecting it
	StatusEffectVector& effects = GetEffectVector(id);
	StatusEffectVector::iterator it = effects.begin();
	while (it != effects.end())
	{
		EffectDetails& details = (*it);
		details.effect->OnKill();
		
		size_t target = details.effect->GetTarget()->GetID();
		
		if (details.effect->GetOwner() != details.effect->GetTarget())
		{
			RemoveStatusEffect(target == id ? details.effect->GetOwner()->GetID() : id, details.effect);
		}
		
		it = effects.erase(it);
	}
}

bool StatusEffectManager::StatusEffectAlreadyApplied(size_t id, nsStatusEffect::EffectType type, bool check_for_owned)
{
	if (type == nsStatusEffect::none)
		return false;
	
	StatusEffectVector& effects = GetEffectVector(id);
	for (auto& it : effects) {
		if (it.effect->GetType() == type && it.owner == check_for_owned)
			return true;
	}
	
	return false;
}

void StatusEffectManager::UpdateEffects()
{
	// Keep all effects updated (ie positioned correctly)
	for (auto& effects : m_StatusEffects) {
		for (auto& effect : effects.second) {
			if (effect.owner || effect.self_owned)
				effect.effect->UpdatePosition();
		}
	}
}

void StatusEffectManager::TurnStart(nsControllerNames::Controller controller)
{
	// Update the status effects if it's the owners turn
	for (auto& effects : m_StatusEffects) {
		StatusEffectVector::iterator effect = effects.second.begin();
		while (effect != effects.second.end())
		{
			EffectDetails& details = (*effect);
			if ((details.owner || details.self_owned) && details.effect->GetOwner()->GetControllerName() == controller && details.effect->TurnStart())
			{
				// The effect is finished, removed it from us and our target
				if (!details.self_owned)
					RemoveStatusEffect(details.effect->GetTarget()->GetID(), details.effect);
				
				details.effect->OnKill();
				delete details.effect;
				effect = effects.second.erase(effect);
			}
			else ++effect;
		}
	}
}

bool StatusEffectManager::OnDamage(size_t id, int& value, Unit* other)
{
	return OnApply(id, value, [&value, &other](StatusEffect* effect) {
		return effect->OnDamage(value, other);
	});
}

bool StatusEffectManager::OnHeal(size_t id, int& value, Unit* other)
{
	return OnApply(id, value, [&value, &other](StatusEffect* effect) {
		return effect->OnHeal(value, other);
	});
}

bool StatusEffectManager::OnAbilityUse(size_t id, int& value, Unit* other)
{
	return OnApply(id, value, [&value, &other](StatusEffect* effect) {
		return effect->OnAbilityUse(value, other);
	});
}

bool StatusEffectManager::OnMovement(size_t id, int& value)
{
	return OnApply(id, value, [&value](StatusEffect* effect) {
		return effect->OnMovement(value);
	});
}

StatusEffectVector& StatusEffectManager::GetEffectVector(size_t id)
{
	return m_StatusEffects[id];
}

void StatusEffectManager::RemoveStatusEffect(size_t id, const StatusEffect* effect)
{
	StatusEffectVector& effects = GetEffectVector(id);
	StatusEffectVector::iterator it = effects.begin();
	while (it != effects.end())
	{
		if ((*it).effect == effect)
		{
			effects.erase(it);
			break;
		}
		
		++it;
	}
}

bool StatusEffectManager::OnApply(size_t id, int& value, std::function<bool(StatusEffect* effect)> event)
{
	// Call the event for any status effects
	StatusEffectVector& effects = GetEffectVector(id);
	StatusEffectVector::iterator effect = effects.begin();
	while (effect != effects.end())
	{
		EffectDetails& details = (*effect);
		if (!details.owner)
		{
			bool applied = event(details.effect);
			bool deleted(false);
			
			if (applied && details.effect->Damage())
			{
				// The effect is finished, removed it from us and our target
				if (!details.self_owned)
				{
					size_t who = id == details.effect->GetTarget()->GetID() ? details.effect->GetOwner()->GetID() : id;
					RemoveStatusEffect(who, details.effect);
				}
				
				deleted = true;
				delete details.effect;
				
				effect = effects.erase(effect);
			}
			
			// Quit early if we have reduced value to 0
			if (applied && value == 0)
				return true;
			
			if (!deleted)
				++effect;
		}
		else ++effect;
	}
	
	// Return false if we hit here as there is still value to apply
	return false;
}