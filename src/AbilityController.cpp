#include "AbilityController.h"
#include "UnitController.h"
#include "PlayerController.h"
#include "DataFile.h"
#include "SceneManager.h"
#include "StatusEffectManager.h"
#include "CameraController.h"
#include <fstream>

AbilityController::AbilityMap AbilityController::m_Abilities;

AbilityController::AbilityController(UnitController* owner)
	: m_Owner(owner)
	, m_Target(nullptr)
	, m_PathFinder(vector2di(AREA_SIZE, AREA_SIZE))
{
	// Add the level walls to our pathfinder (TODO: Move this into a post-generation function as we'll need to do this when moving to a new room)
	const Terrain& terrain = m_Owner->GetSceneManager()->GetTerrain();
	for (int x = 0; x < AREA_SIZE; ++x) {
		for (int y = 0; y < AREA_SIZE; ++y) {
			vector3df pos(static_cast<float>(x) + 0.5f, 0.5f, static_cast<float>(y) + 0.5f);
			if (terrain.GetHeightBelow(pos) != 0.f)
			{
				// We can't move here, add a wall to the pathfinder
				m_PathFinder.SetWall(vector2di(x, y), true);
			}
		}
	}
}

void AbilityController::Update()
{
	// Update our projectiles
	ProjectileVector::iterator it = m_Projectiles.begin();
	while (it != m_Projectiles.end())
	{
		if ((*it)->IsAlive())
		{
			(*it)->Update();
			++it;
		}
		else
		{
			// Don't need to worry about the ability not being found as it was checked when the projectile was created
			PerformAbilityAction(m_Abilities.find((*it)->GetName())->second, (*it)->GetTarget(), (*it)->GetValue(), (*it)->GetOwner());
			it = m_Projectiles.erase(it);
		}
	}
}

bool AbilityController::UseAbility(const std::string& name, Unit* user, const vector2di& pos, int value)
{
	bool ability_used(false);
	bool do_hit(false);
	vector3df action_position(user->GetPosition());

	AbilityMap::iterator it = m_Abilities.find(name);
	if (it != m_Abilities.end())
	{
		const Ability& ability = it->second;
		UnitController* target_controller = m_Target;
		
		// If this is a friendly ability, swap controllers
		bool support = (ability.category == nsAbilityCategory::support || ability.category == nsAbilityCategory::special_support);
		if (support)
			target_controller = m_Owner;
		
		Unit* target = target_controller->GetUnit(pos);
		
		if ((ability.type == nsAbilityType::aoe_anywhere || (target && target->GetHealth() > 0)) &&
			!Singleton<StatusEffectManager>::Instance().OnAbilityUse(user, value, target))
		{
			if (ability.range > 2)
			{
				// Ranged attack, create a projectile
				if (target || ability.type == nsAbilityType::aoe_anywhere)
				{
					action_position.Set(pos.x + 0.5f, 0.3f, pos.y + 0.5f);
					m_Projectiles.push_back(std::unique_ptr<Projectile>(new Projectile(m_Owner->GetSceneManager(), user, name, value, user->GetPosition(), action_position)));
					Singleton<CameraController>::Instance().SetTargetProjectile(m_Projectiles.at(m_Projectiles.size() - 1).get());
					
					ability_used = true;
				}
			}
			else
			{
				// Melee attack, go straight to performing the ability
				if (target)
				{
					action_position = target->GetPosition();
					ability_used = PerformAbilityAction(ability, action_position, value, user);
					do_hit = !support;
				}
			}
		}
	}
	
	if (ability_used)
	{
		// Animate the current character
		user->AttackAnimate(action_position);
		
		if (do_hit)
		{
			// Play an effect (if we want to)
			Camera& camera = m_Owner->GetSceneManager()->GetActiveCamera();
			float offset_x = -0.1f * sin(-camera.GetDirection() + PI_OVER_2);
			float offset_z = -0.1f * cos(-camera.GetDirection() + PI_OVER_2);
			m_Owner->PlayEffect(0, action_position + vector3df(offset_x, 0.f, offset_z), 35.f);
		}
	}
	
	return ability_used;
}

const Projectile* AbilityController::GetFirstProjectile() const
{
	const Projectile* proj = nullptr;
	
	if (!m_Projectiles.empty())
	{
		proj = m_Projectiles.at(0).get();
	}
	
	return proj;
}

bool AbilityController::PerformAbilityAction(const Ability& ability, const vector3df& target, int value, Unit* user)
{
	// Actually use the ability, called either for a melee attack straight away or for a ranged attack when the projectile dies
	bool ability_used(false);
	bool killed(false);
	UnitController* target_controller = m_Target;
	vector2di pos(target.x, target.z);
	PlayerController* player_controller = dynamic_cast<PlayerController*>(m_Owner);
	
	// If this is a friendly ability, switch controller	
	bool support = (ability.category == nsAbilityCategory::support || ability.category == nsAbilityCategory::special_support);
	if (support)
		target_controller = m_Owner;
	
	// Get ability use area
	std::vector<vector2di> area;
	m_PathFinder.GetFillArea(pos, ability.area, area);
	
	auto create_status_effect = [&](Unit* target_unit) {
		// The effect modifier will be taken from the unit when I get around to doing that
		StatusEffect* effect = StatusEffect::CreateStatusEffect(ability.status_effect, m_Owner->GetSceneManager(), user, target_unit, ability.effect_life, ability.round_count, ability.effect_modifier);
		
		if (effect)
		{
			Singleton<StatusEffectManager>::Instance().AddStatusEffect(target_unit, user, effect);
			
			ability_used = true;
		}
	};
	
	auto apply_status_effect = [&](Unit* target_unit) {
		switch (ability.type)
		{
			case nsAbilityType::single_target:
			case nsAbilityType::self:
			{
				create_status_effect(target_unit);
				ability_used = true;
			}
			break;
			case nsAbilityType::aoe_anywhere:
			case nsAbilityType::self_aoe:
			{
				// Get the targets in our area and hurt them
				for (auto& point : area) {
					Unit* unit = target_controller->GetUnit(point);

					if (unit && (unit != user || ability.affects_self))
					{
						create_status_effect(unit);
					}
				}
				ability_used = true;
			}
			break;
			default:
				break;
		}
	};
	
	auto adjust_unit_health = [&](Unit* target_unit, bool damage) {
		switch (ability.type)
		{
			case nsAbilityType::single_target:
			case nsAbilityType::self:
			{
				if (damage)
					killed = target_controller->DamageUnit(target_unit, value, user);
				else
					target_controller->HealUnit(target_unit, value, user);
				ability_used = true;
			}
			break;
			case nsAbilityType::aoe_anywhere:
			case nsAbilityType::self_aoe:
			{
				// Get the targets in our area and adjust their health
				for (auto& point : area) {
					Unit* unit = target_controller->GetUnit(point);

					if (unit && (unit != user || ability.affects_self))
					{
						if (damage && target_controller->DamageUnit(unit, value, user) && player_controller)
						{
							player_controller->EnemyKilled(point);
						}
						else if (!damage)
							target_controller->HealUnit(unit, value, user);
					}
				}
				ability_used = true;
			}
			break;
			default:
				break;
		}
	};
	
	// Attack / support abilities
	if (ability.category != nsAbilityCategory::summon && ability.category != nsAbilityCategory::special_summon)
	{
		Unit* target_unit = target_controller->GetUnit(pos);
		if (target_unit || ability.type == nsAbilityType::aoe_anywhere)
		{
			// Right, we have a target, now decide what to do
			if (support)
			{
				switch (ability.status_effect)
				{
					case nsStatusEffect::heal:
					{
						// Call the adjust health function but using the heal function instead
						adjust_unit_health(target_unit, false);
					}
					break;
					default:
						apply_status_effect(target_unit);
						break;
				}
			}
			else
			{
				// Attack our target(s)
				adjust_unit_health(target_unit, true);
			}
		}
	}
	else // Summon abilities
	{
		///TODO: This - will probably use value to determine what to summon from a list? Or just use the name of the ability
	}
	
	if (killed)
	{
		// If we are owned by the player controller, tell it to update its pathfinding
		if (player_controller)
		{
			player_controller->EnemyKilled(vector2di(target.x, target.z));
		}
	}
	
	return ability_used;
}

bool AbilityController::LoadAbilities(const std::string& filename)
{
	bool any_added(false);
	DataFile file(filename);
	
	// String to category
	auto string_to_category = [](const std::string& in) -> nsAbilityCategory::AbilityCategory {
		nsAbilityCategory::AbilityCategory val(nsAbilityCategory::attack);
		
		if (in == "Attack")
			val = nsAbilityCategory::attack;
		else if (in == "Support")
			val = nsAbilityCategory::support;
		else if (in == "Summon")
			val = nsAbilityCategory::summon;
		else if (in == "Special Attack")
			val = nsAbilityCategory::special_attack;
		else if (in == "Special Support")
			val = nsAbilityCategory::special_attack;
		else if (in == "Special Summon")
			val = nsAbilityCategory::special_attack;
		
		return val;
	};
	
	// String to type
	auto string_to_type = [](const std::string& in) -> nsAbilityType::AbilityType {
		nsAbilityType::AbilityType val(nsAbilityType::single_target);
		
		if (in == "Single")
			val = nsAbilityType::single_target;
		else if (in == "Targeted AOE")
			val = nsAbilityType::aoe_target;
		else if (in == "AOE")
			val = nsAbilityType::aoe_anywhere;
		else if (in == "Lance")
			val = nsAbilityType::lance;
		else if (in == "Self")
			val = nsAbilityType::self;
		else if (in == "Self AOE")
			val = nsAbilityType::self_aoe;
		
		return val;
	};
	
	// String to status effect
	auto string_to_status_effect = [](const std::string& in) -> nsStatusEffect::EffectType {
		nsStatusEffect::EffectType val(nsStatusEffect::none);
		
		if (in == "None")
			val = nsStatusEffect::none;
		else if (in == "Fire")
			val = nsStatusEffect::fire;
		else if (in == "Ice")
			val = nsStatusEffect::ice;
		else if (in == "Poison")
			val = nsStatusEffect::poison;
		else if (in == "Life Steal")
			val = nsStatusEffect::life_steal;
		else if (in == "Life Transfer")
			val = nsStatusEffect::life_transfer;
		else if (in == "Heal")
			val = nsStatusEffect::heal;
		else if (in == "Shield")
			val = nsStatusEffect::shield;
		else if (in == "Damage Transfer")
			val = nsStatusEffect::damage_transfer;
		else if (in == "Buff Damage")
			val = nsStatusEffect::buff_damage;
		else if (in == "Buff Health")
			val = nsStatusEffect::buff_health;
		else if (in == "Buff Movement")
			val = nsStatusEffect::buff_movement;
		
		return val;
	};
	
	// Enum to allow switching on a string in a pretty way
	enum AttributeName {
		attr_category = 0,
		attr_range,
		attr_area,
		attr_cost,
		attr_cooldown,
		attr_type,
		attr_modifier,
		attr_modifier_value,
		attr_life,
		attr_round_count,
		attr_self_use
	};
	
	// Table to allow switching on a string
	std::map<std::string, AttributeName> get_attribute = {
		{ "category", attr_category },
		{ "range", attr_range },
		{ "area", attr_area },
		{ "cost", attr_cost },
		{ "cooldown", attr_cooldown },
		{ "type", attr_type },
		{ "modifier", attr_modifier },
		{ "value", attr_modifier_value },
		{ "life", attr_life },
		{ "round count", attr_round_count },
		{ "self use", attr_self_use }
	};
	
	// New way
	while (file.IsOpen())
	{
		Ability data;
		std::string name;
		nsDataFileRead::DataFileRead read_status = nsDataFileRead::empty;
		
		while ((read_status = file.Read()) != nsDataFileRead::empty && read_status != nsDataFileRead::end_of_file)
		{
			switch (read_status)
			{
				case nsDataFileRead::section:
				{
					switch (file.GetDepth())
					{
						case 0: // Name of the ability
							name = file.GetAttributeName();
							break;
						default:
							// Do nothing
							break;
					}
				}
				break;
				case nsDataFileRead::attribute:
				{
					switch (file.GetDepth())
					{
						case 1:
						{
							// Attribute
							switch (get_attribute[file.GetAttributeName()])
							{
								case attr_category:
									data.category = string_to_category(file.ReadString());
									break;
								case attr_range:
									data.range = file.ReadInt();
									break;
								case attr_area:
									data.area = file.ReadInt();
									break;
								case attr_cost:
									data.cost = file.ReadInt();
									break;
								case attr_cooldown:
									data.cooldown = file.ReadInt();
									break;
								case attr_type:
									data.type = string_to_type(file.ReadString());
									break;
								case attr_modifier:
									data.status_effect = string_to_status_effect(file.ReadString());
									break;
								case attr_self_use:
									data.affects_self = file.ReadBool();
									break;
								default:
									// Does not apply here
									break;
							}
						}
						break;
						case 2:
						{
							// Modifier settings
							switch (get_attribute[file.GetAttributeName()])
							{
								case attr_modifier_value:
									data.effect_modifier = file.ReadFloat();
									break;
								case attr_life:
									data.effect_life = file.ReadInt();
									break;
								case attr_round_count:
									data.round_count = file.ReadInt();
									break;
								default:
									// Does not apply here
									break;
							}
						}
						break;
						default:
							// Do nothing
							break;
					}
				}
				break;
				default:
					// Do nothing
					break;
			}
		}
		
		if (!name.empty())
		{
			m_Abilities.emplace(name, data);
			any_added = true;
		}
	}
	
	return any_added;
}

Ability& AbilityController::GetAbility(const std::string& name)
{
	AbilityMap::iterator it = m_Abilities.find(name);
	if (it != m_Abilities.end())
	{
		return it->second;
	}
	
	static Ability empty;
	return empty;
}