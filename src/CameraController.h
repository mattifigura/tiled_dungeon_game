#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include "Camera.h"
#include "ControllerNames.h"
#include "Singleton.h"

class Unit;
class Projectile;

class CameraController
{
public:
	CameraController();
	~CameraController();

	void SetCamera(Camera* camera) { m_Camera = camera; }
	void SetCurrentController(nsControllerNames::Controller controller) { m_CurrentController = controller; }

	void Update();

	void SetTargetUnit(const Unit* target, float animation_delay = 0.5f);
	void SetTargetProjectile(const Projectile* projectile);

	const Unit* GetTargetUnit() const { return m_TargetUnit; }
	const Projectile* GetTargetProjectile() const { return m_TargetProjectile; }
	
	bool IsAnimating() const { return m_Animation > 0.f; }

	// Note LookAt will do nothing if there is a target unit or projectile
	void LookAt(const vector3df& pos);

	bool ScrollEvent(double x, double y);

protected:

private:
	Camera*							m_Camera;
	const Unit*						m_TargetUnit;
	const Projectile*				m_TargetProjectile;
	vector2df						m_TargetPosition;
	vector2df						m_CurrentPosition;
	float							m_Animation;
	float							m_CameraRotation;
	nsControllerNames::Controller	m_CurrentController;
	bool							m_ProjectileDied;
};

#endif // CAMERACONTROLLER_H
