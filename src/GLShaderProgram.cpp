#include "GLShaderProgram.h"
#include <iostream>
#include <vector>

GLShaderProgram::GLShaderProgram(const std::string& vertex, const std::string& fragment, const std::string& geometry)
	: m_VertexShader(0)
	, m_FragmentShader(0)
	, m_GeometryShader(0)
	, m_fAngle(85.f)
	, m_fRatio(16.f / 9.f)
	, m_fNear(0.1f)
	, m_fFar(800.f)
	, m_fCurrentAngle(m_fAngle)
	, m_Vertex(vertex)
	, m_Fragment(fragment)
	, m_Geometry(geometry)
{
	CompileShaders();
}

GLShaderProgram::~GLShaderProgram()
{
	Clear();
}

void GLShaderProgram::Clear()
{
	glDeleteShader(m_VertexShader);
	glDeleteShader(m_FragmentShader);
	
	if (m_GeometryShader > 0)
		glDeleteShader(m_GeometryShader);
	
	glDeleteProgram(m_ShaderProgram);
	
	m_VertexShader = 0;
	m_FragmentShader = 0;
	m_GeometryShader = 0;
}

void GLShaderProgram::CompileShaders()
{
	// Load the given shaders (supplied as strings) and compile a shader program
	const GLchar* czVertex = m_Vertex.c_str();
	m_VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(m_VertexShader, 1, &czVertex, NULL);
	glCompileShader(m_VertexShader);

	const GLchar* czFragment = m_Fragment.c_str();
	m_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_FragmentShader, 1, &czFragment, NULL);
	glCompileShader(m_FragmentShader);
	
	// Geometry shaders are optional so check if we have a valid one before trying
	bool bGeometryShader = !m_Geometry.empty();
	if (bGeometryShader)
	{
		const GLchar* czGeometry = m_Geometry.c_str();
		m_GeometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(m_GeometryShader, 1, &czGeometry, NULL);
		glCompileShader(m_GeometryShader);
	}

	// Check that the shaders have compiled correctly
	GLint ShaderStatus(GL_FALSE);
	glGetShaderiv(m_VertexShader, GL_COMPILE_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		//cout << "Vertex shader failed to compile!" << endl;
		GLint maxLength = 0;
		glGetShaderiv(m_VertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(m_VertexShader, maxLength, &maxLength, &errorLog[0]);

		std::cout << "Vertex shader failed to compile! Details: " << std::endl;
		for (GLint i = 0; i < maxLength; ++i) {
			std::cout << errorLog[i];
		}
		std::cout << std::endl;
		return;
	}

	glGetShaderiv(m_FragmentShader, GL_COMPILE_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		//cout << "Fragment shader failed to compile!" << endl;
		GLint maxLength = 0;
		glGetShaderiv(m_VertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(m_VertexShader, maxLength, &maxLength, &errorLog[0]);

		std::cout << "Fragment shader failed to compile! Details: " << std::endl;
		for (GLint i = 0; i < maxLength; ++i) {
			std::cout << errorLog[i];
		}
		std::cout << std::endl;
		return;
	}

	if (bGeometryShader)
	{
		glGetShaderiv(m_GeometryShader, GL_COMPILE_STATUS, &ShaderStatus);
		if (ShaderStatus != GL_TRUE)
		{
			//cout << "Fragment shader failed to compile!" << endl;
			GLint maxLength = 0;
			glGetShaderiv(m_VertexShader, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(m_VertexShader, maxLength, &maxLength, &errorLog[0]);

			std::cout << "Geometry shader failed to compile! Details: " << std::endl;
			for (GLint i = 0; i < maxLength; ++i) {
				std::cout << errorLog[i];
			}
			std::cout << std::endl;
			return;
		}
	}

	// Create the shader program
	m_ShaderProgram = glCreateProgram();
	glAttachShader(m_ShaderProgram, m_VertexShader);
	glAttachShader(m_ShaderProgram, m_FragmentShader);
	
	if (bGeometryShader)
		glAttachShader(m_ShaderProgram, m_GeometryShader);
	
	glLinkProgram(m_ShaderProgram);

	// Check that the program was created successfully and use it
	glGetProgramiv(m_ShaderProgram, GL_LINK_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		std::cout << "Shader program failed to link!" << std::endl;
		return;
	}
}

void GLShaderProgram::RecompileShaders()
{
	Clear();
	CompileShaders();
}

void GLShaderProgram::SetShaders(const std::string& vertex, const std::string& fragment, const std::string& geometry)
{
	m_Vertex = vertex;
	m_Fragment = fragment;
	m_Geometry = geometry;
}

void GLShaderProgram::Bind()
{
	glUseProgram(m_ShaderProgram);
}

void GLShaderProgram::Unbind()
{
	glUseProgram(0);
}

GLint GLShaderProgram::GetAttribLocation(const GLchar* attrib)
{
	// Remember to call Bind() first!
	return glGetAttribLocation(m_ShaderProgram, attrib);
}

GLint GLShaderProgram::GetUniformLocation(const GLchar* attrib)
{
	// Remember to call Bind() first!
	return glGetUniformLocation(m_ShaderProgram, attrib);
}

void GLShaderProgram::SetOrthoProjection(int width, int height)
{
	Bind();
	m_ProjMatrix.Orthographic(0, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.f, -100.f, 100.f);
	glUniformMatrix4fv(GetUniformLocation("projection"), 1, GL_TRUE, m_ProjMatrix.GetDataPointer());
	Unbind();
}

void GLShaderProgram::SetPerspectiveProjection(float angle, float ratio, float near, float far, bool bDefaults)
{
	Bind();
	m_ProjMatrix.Perspective(angle, ratio, near, far);
	glUniformMatrix4fv(GetUniformLocation("projection"), 1, GL_TRUE, m_ProjMatrix.GetDataPointer());
	Unbind();

	// If we are setting the defaults, do so now
	if (bDefaults)
	{
		m_fAngle = angle;
		m_fRatio = ratio;
		m_fNear = near;
		m_fFar = far;
	}
}

const matrix4f& GLShaderProgram::GetProjectionMatrix() const
{
	return m_ProjMatrix;
}
