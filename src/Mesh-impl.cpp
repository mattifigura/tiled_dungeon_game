#include "Mesh.cpp"
#include "GLBase.h"

// Implementation file for the Mesh template class

template Mesh<GLVertex>::Mesh();
template GLuint Mesh<GLVertex>::GetVBO() const;
template GLuint Mesh<GLVertex>::GetEBO() const;
template bool Mesh<GLVertex>::Buffer(bool bKeepData, size_t size);
template bool Mesh<GLVertex>::Update();
template size_t Mesh<GLVertex>::GetIndexBufferSize() const;
template void Mesh<GLVertex>::SetDirty();
template bool Mesh<GLVertex>::IsDirty() const;
template void Mesh<GLVertex>::Clear();
template void Mesh<GLVertex>::AddTriangle(const GLVertex& ver1, const GLVertex& ver2, const GLVertex& ver3);
template GLuint Mesh<GLVertex>::AddQuad(const GLVertex& ver1, const GLVertex& ver2, const GLVertex& ver3, const GLVertex& ver4, bool flip);
template void Mesh<GLVertex>::AddVertex(const GLVertex& ver);
template void Mesh<GLVertex>::AddIndex(GLuint index);
template void Mesh<GLVertex>::CopyVertices(const std::vector<GLVertex>& data);
template void Mesh<GLVertex>::CopyIndices(const std::vector<GLuint>& data);
template std::vector<GLVertex>& Mesh<GLVertex>::GetVertexData();
template const std::vector<GLVertex>& Mesh<GLVertex>::GetVertexData() const;
template const std::vector<GLuint>& Mesh<GLVertex>::GetIndexData() const;

template Mesh<GLVertex2D>::Mesh();
template GLuint Mesh<GLVertex2D>::GetVBO() const;
template GLuint Mesh<GLVertex2D>::GetEBO() const;
template bool Mesh<GLVertex2D>::Buffer(bool bKeepData, size_t size);
template bool Mesh<GLVertex2D>::Update();
template size_t Mesh<GLVertex2D>::GetIndexBufferSize() const;
template void Mesh<GLVertex2D>::SetDirty();
template bool Mesh<GLVertex2D>::IsDirty() const;
template void Mesh<GLVertex2D>::Clear();
template void Mesh<GLVertex2D>::AddTriangle(const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3);
template GLuint Mesh<GLVertex2D>::AddQuad(const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3, const GLVertex2D& ver4, bool flip);
template void Mesh<GLVertex2D>::AddVertex(const GLVertex2D& ver);
template void Mesh<GLVertex2D>::AddIndex(GLuint index);
template void Mesh<GLVertex2D>::CopyVertices(const std::vector<GLVertex2D>& data);
template void Mesh<GLVertex2D>::CopyIndices(const std::vector<GLuint>& data);
template std::vector<GLVertex2D>& Mesh<GLVertex2D>::GetVertexData();
template const std::vector<GLVertex2D>& Mesh<GLVertex2D>::GetVertexData() const;
template const std::vector<GLuint>& Mesh<GLVertex2D>::GetIndexData() const;