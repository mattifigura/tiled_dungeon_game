#ifndef PATH_FINDER_H_
#define PATH_FINDER_H_

#include "Vector.h"
#include <memory>
#include <set>
#include <vector>

namespace Path
{
	struct Node {
		Node* previous;
		vector2di position;
		int path_score;
		int heuristic;
		
		Node(const vector2di& pos, Node* prev = nullptr)
			: previous(prev)
			, position(pos)
			, path_score(0)
			, heuristic(0)
		{}
		
		int score() const { return path_score + heuristic; }
	};
	
	struct FillNode {
		vector2di position;
		int value;
		
		FillNode(const vector2di& pos, int val)
			: position(pos)
			, value(val)
		{}
	};
}

// PathFinder class - uses A* to find a path from tile A to tile B

class PathFinder {
public:
	PathFinder(const vector2di& size);

	void SetWall(const vector2di& pos, bool wall);
	bool GetWall(const vector2di& pos) const { return m_Walls.at(pos.x * m_Size.y + pos.y); }
	bool GetWall(int x, int y) const { return m_Walls.at(x * m_Size.y + y); }
	void ClearWalls();
	
	bool FindPath(const vector2di& start, const vector2di& end, std::vector<vector2di>& path, int max_calculations = -1);
	
	void GetFillArea(const vector2di& start, int value, std::vector<vector2di>& area) const;

protected:

private:
	std::vector<bool>	m_Walls;
	vector2di			m_Size;
		
	int Heuristic(const vector2di& one, const vector2di& two) const;
	template<class T> T* FindNodeInSet(std::set<std::shared_ptr<T>>& node_set, const vector2di& position) const;
};

#endif // PATH_FINDER_H_
