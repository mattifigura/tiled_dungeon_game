#include "PerlinNoise.h"
#include <numeric>
#include <random>
#include <algorithm>

PerlinNoise::PerlinNoise(unsigned int seed)
{
	SetSeed(seed);
}

void PerlinNoise::SetSeed(unsigned int seed)
{
	m_Permutations.resize(256);

	// fill p with values from 0 to 255
	std::iota(m_Permutations.begin(), m_Permutations.end(), 0);

	// initialize a random engine with seed
	std::default_random_engine engine(seed);

	// shuffle using the above random engine
	std::shuffle(m_Permutations.begin(), m_Permutations.end(), engine);

	// duplicate the permutation vector
	m_Permutations.insert(m_Permutations.end(), m_Permutations.begin(), m_Permutations.end());
}

double PerlinNoise::GetNoise(double x, double y, double z) const
{
	// find the unit cube that contains the point
	int X = (int)floor(x) & 255;
	int Y = (int)floor(y) & 255;
	int Z = (int)floor(z) & 255;

	// find relative x, y, z of point in cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// compute fade curves for each of x, y, z
	double u = _Fade(x);
	double v = _Fade(y);
	double w = _Fade(z);

	// hash coordinates of the 8 cube corners
	int A = m_Permutations[X] + Y;
	int AA = m_Permutations[A] + Z;
	int AB = m_Permutations[A + 1] + Z;
	int B = m_Permutations[X + 1] + Y;
	int BA = m_Permutations[B] + Z;
	int BB = m_Permutations[B + 1] + Z;

	// add blended results from 8 corners of cube
	double res = _Lerp(w, _Lerp(v, _Lerp(u, _Grad(m_Permutations[AA], x, y, z), _Grad(m_Permutations[BA], x - 1, y, z)),
		_Lerp(u, _Grad(m_Permutations[AB], x, y - 1, z), _Grad(m_Permutations[BB], x - 1, y - 1, z))),
		_Lerp(v, _Lerp(u, _Grad(m_Permutations[AA + 1], x, y, z - 1), _Grad(m_Permutations[BA + 1], x - 1, y, z - 1)),
			_Lerp(u, _Grad(m_Permutations[AB + 1], x, y - 1, z - 1), _Grad(m_Permutations[BB + 1], x - 1, y - 1, z - 1))));
	return (res + 1.0) / 2.0;
}

double PerlinNoise::_Fade(double t) const
{
	return (t * t * t * (t * (t * 6 - 15) + 10));
}

double PerlinNoise::_Lerp(double t, double a, double b) const
{
	return (a + t * (b - a));
}

double PerlinNoise::_Grad(int hash, double x, double y, double z) const
{
	int h = hash & 15;
	double u = (h < 8) ? x : y;
	double v = (h < 4) ? y : ((h == 12 || h == 14) ? x : z);
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}