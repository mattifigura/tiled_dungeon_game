#ifndef GL_TEXTURE_H_
#define GL_TEXTURE_H_

#include "GLBase.h"
#include <string>
#include <vector>

// GLTexture class - this is essentially a texture in the game but self-loads and self-cleans up on destruction

class GLTexture {
public:
	GLTexture(const std::string& filename = "");
	~GLTexture();

	GLuint ID() const;
	const vector2di& Dimensions() const;
	const GLColour& GetPixel(int x, int y) const;

	void CreateTextureFromBuffer(const GLubyte* buffer, int width, int height, GLenum type);

protected:

private:
	GLuint					m_Texture;
	vector2di				m_Dimensions;
	std::vector<GLColour>	m_Pixels;

	void LoadFromPNG(const std::string& filename);
};

#endif
