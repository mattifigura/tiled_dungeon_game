#ifndef FAST_MUTEX_H
#define FAST_MUTEX_H

#include <atomic>

// FastMutex class - really it's a spinlock mutex but with an extra try_lock function

class FastMutex {
public:
	FastMutex() : m_Flag(ATOMIC_FLAG_INIT) {}

	void lock()
	{
		while (m_Flag.test_and_set(std::memory_order_acquire))
			;
	}
	
	bool try_lock()
	{
		return (!m_Flag.test_and_set(std::memory_order_acquire));
	}
	
	void unlock()
	{
		m_Flag.clear(std::memory_order_release);
	}

protected:

private:
	std::atomic_flag	m_Flag;
};

#endif // FAST_MUTEX_H