#ifndef STATUS_EFFECT_IMPLEMENTATIONS_H
#define STATUS_EFFECT_IMPLEMENTATIONS_H

#include "StatusEffect.h"

// Define all status effects here and then implement them
// As a lot of these will be fairly simple and small I didn't want to do these in separate files

// Shield effect

class StatusEffect_Shield : public StatusEffect {
public:
	StatusEffect_Shield(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier);

	virtual bool OnDamage(int& value, Unit* other = 0);

protected:

private:

};

// Damage Transfer effect

class StatusEffect_DamageTransfer : public StatusEffect {
public:
	StatusEffect_DamageTransfer(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier);

	virtual bool OnDamage(int& value, Unit* other = 0);

protected:

private:

};

#endif // STATUS_EFFECT_IMPLEMENTATIONS_H
