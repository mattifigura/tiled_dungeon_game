#include "GUIButton.h"
#include "GUIManager.h"

GUIButton::GUIButton(GUIManager* guiMan, const std::string& text, const std::string& font, const vector2df& size, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_pGUILabel(NULL)
	, m_Function(NULL)
{
	// Set our type
	SetType(GUIType::gui_button);
	SetSize(size);

	// Create our label
	m_pGUILabel = GetGUIManager()->CreateText(vector2df(0.f, 4.f), text, font, this);
	m_pGUILabel->SetSize(size);
	m_pGUILabel->SetAlignment(TextAlignment::centre);
	SetBoundingBox(vector2df(), size);
}

void GUIButton::Update()
{
	// Check for hover and update our mesh
	/// TODO: Decide whether I really care about this right now...

	// Call base class update
	GUIElement::Update();
}

void GUIButton::SetFunction(const std::function<bool(GUIElement*)>& func)
{
	m_Function = func;
}

bool GUIButton::OnClick()
{
	// Click us!
	return (m_Function && m_Function(this));
}

void GUIButton::SetText(const std::string& text)
{
	m_pGUILabel->SetText(text);
}

void GUIButton::BuildElement()
{
	// Add a pane
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), GLColour(0, 100)),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(0, 100)),
		GLVertex2D(vector2df(size.x, size.y), GLColour(0, 100)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(0, 100)));
}
