#include "ParticleDefinitionLibrary.h"
#include "DataFile.h"
#include <iostream>

ParticleDefinitionLibrary::DefinitionMap ParticleDefinitionLibrary::m_Definitions;

bool ParticleDefinitionLibrary::LoadDefinitions(const std::string& filename)
{
	// Load the file
	DataFile file(filename);
	bool any_read(false);
	
	// Enum to allow switching on a string in a pretty way
	enum AttributeName {
		attr_count = 0,
		attr_lights,
		attr_affected_by_lights,
		attr_burst,
		attr_force,
		attr_force_lower,
		attr_force_upper,
		attr_force_from_start,
		attr_friction,
		attr_start_colour,
		attr_end_colour,
		attr_duration,
		attr_duration_randomiser,
		attr_gravity,
		attr_gravity_anchor,
		attr_gravity_anchor_rotate,
		attr_collisions,
		attr_bounce,
		attr_position_lower,
		attr_position_upper,
		attr_minimum_start_distance,
		attr_start_size,
		attr_end_size,
		attr_only_live_once
	};
	
	// Table to allow switching on a string
	std::map<std::string, AttributeName> get_attribute = {
		{ "count", attr_count },
		{ "lights", attr_lights },
		{ "affected by lights", attr_affected_by_lights },
		{ "burst", attr_burst },
		{ "force", attr_force },
		{ "force lower", attr_force_lower },
		{ "force upper", attr_force_upper },
		{ "force from start", attr_force_from_start },
		{ "friction", attr_friction },
		{ "start colour", attr_start_colour },
		{ "end colour", attr_end_colour },
		{ "duration", attr_duration },
		{ "duration randomiser", attr_duration_randomiser },
		{ "gravity", attr_gravity },
		{ "gravity anchor", attr_gravity_anchor },
		{ "gravity anchor rotate", attr_gravity_anchor_rotate },
		{ "collisions", attr_collisions },
		{ "bounce", attr_bounce },
		{ "position lower", attr_position_lower },
		{ "position upper", attr_position_upper },
		{ "minimum start distance", attr_minimum_start_distance },
		{ "start size", attr_start_size },
		{ "end size", attr_end_size },
		{ "only live once", attr_only_live_once }
	};
	
	// New way
	while (file.IsOpen())
	{
		ParticleDefinition data;
		std::string name;
		nsDataFileRead::DataFileRead read_status = nsDataFileRead::empty;
		
		while ((read_status = file.Read()) != nsDataFileRead::empty && read_status != nsDataFileRead::end_of_file)
		{
			switch (read_status)
			{
				case nsDataFileRead::section:
				{
					switch (file.GetDepth())
					{
						case 0: // Name of the particle effect
							name = file.GetAttributeName();
							break;
						default:
							// Do nothing
							break;
					}
				}
				break;
				case nsDataFileRead::attribute:
				{
					switch (file.GetDepth())
					{
						case 1:
						{
							// Attribute
							switch (get_attribute[file.GetAttributeName()])
							{
								case attr_count:
									data.count = file.ReadInt();
									break;
								case attr_lights:
									data.lights = file.ReadBool();
									break;
								case attr_affected_by_lights:
									data.affected_by_lights = file.ReadBool();
									break;
								case attr_burst:
									data.burst = file.ReadBool();
									break;
								case attr_force:
									data.behaviour.force = file.ReadVector3<float>();
									break;
								case attr_force_lower:
									data.behaviour.force_randomiser_lower = file.ReadVector3<float>();
									break;
								case attr_force_upper:
									data.behaviour.force_randomiser_upper = file.ReadVector3<float>();
									break;
								case attr_force_from_start:
									data.behaviour.force_from_start = file.ReadBool();
									break;
								case attr_friction:
									data.behaviour.friction = file.ReadVector3<float>();
									break;
								case attr_start_colour:
									data.behaviour.start_colour = file.ReadGLColour();
									break;
								case attr_end_colour:
									data.behaviour.end_colour = file.ReadGLColour();
									break;
								case attr_duration:
									data.behaviour.duration = file.ReadFloat();
									break;
								case attr_duration_randomiser:
									data.behaviour.duration_randomiser = file.ReadFloat();
									break;
								case attr_gravity:
									data.behaviour.gravity = file.ReadVector3<float>();
									break;
								case attr_gravity_anchor:
									data.behaviour.gravity_anchor = file.ReadBool();
									break;
								case attr_gravity_anchor_rotate:
									data.behaviour.gravity_anchor_rotate = file.ReadBool();
									break;
								case attr_collisions:
									data.behaviour.collisions = file.ReadBool();
									break;
								case attr_bounce:
									data.behaviour.bounce = file.ReadVector3<float>();
									break;
								case attr_position_lower:
									data.behaviour.position_randomiser_lower = file.ReadVector3<float>();
									break;
								case attr_position_upper:
									data.behaviour.position_randomiser_upper = file.ReadVector3<float>();
									break;
								case attr_minimum_start_distance:
									data.behaviour.minimum_start_distance = file.ReadFloat();
									break;
								case attr_start_size:
									data.behaviour.start_size = file.ReadVector2<float>();
									break;
								case attr_end_size:
									data.behaviour.end_size = file.ReadVector2<float>();
									break;
								case attr_only_live_once:
									data.behaviour.only_live_once = file.ReadBool();
									break;
								default:
									// Do nothing
									break;
							}
						}
						break;
						default:
							// Do nothing
							break;
					}
				}
				break;
				default:
					// Do nothing
					break;
			}
		}
		
		if (!name.empty())
		{
			DefinitionMap::iterator it = m_Definitions.find(name);
			if (it == m_Definitions.end())
			{
				it = m_Definitions.emplace(name, std::vector<ParticleDefinition>()).first;
			}

			it->second.push_back(data);
			any_read = true;

#ifndef NDEBUG
			std::cout << name << " particles loaded" << std::endl;
#endif
		}
	}
	
	return any_read;
}

std::vector<ParticleDefinition>& ParticleDefinitionLibrary::GetDefinitions(const std::string& name)
{
	DefinitionMap::iterator it = m_Definitions.find(name);
	if (it != m_Definitions.end())
		return it->second;

	static std::vector<ParticleDefinition> empty;
	return empty;
}