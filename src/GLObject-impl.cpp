#include "GLObject.cpp"
#include "GLBase.h"

// Implementation file for the GLObject template class

template GLObject<GLVertex>::GLObject();
template GLObject<GLVertex>::~GLObject();
template bool GLObject<GLVertex>::BufferData(const std::vector<GLVertex>& VBO, const std::vector<GLuint>& EBO);
template bool GLObject<GLVertex>::UpdateVBO(const std::vector<GLVertex>& VBO, size_t size);
template GLuint GLObject<GLVertex>::GetVBO() const;
template GLuint GLObject<GLVertex>::GetEBO() const;
template void GLObject<GLVertex>::ClearBuffers();

template GLObject<GLVertex2D>::GLObject();
template GLObject<GLVertex2D>::~GLObject();
template bool GLObject<GLVertex2D>::BufferData(const std::vector<GLVertex2D>& VBO, const std::vector<GLuint>& EBO);
template bool GLObject<GLVertex2D>::UpdateVBO(const std::vector<GLVertex2D>& VBO, size_t size);
template GLuint GLObject<GLVertex2D>::GetVBO() const;
template GLuint GLObject<GLVertex2D>::GetEBO() const;
template void GLObject<GLVertex2D>::ClearBuffers();