#include "InputManager.h"
#include <algorithm>

InputObject::~InputObject()
{
	// Automatically un-register us from the input manager
	Singleton<InputManager>::Instance().RemoveInputObject(this);
}

InputManager::InputManager()
	: m_pWindow(NULL)
	, m_CursorPosition()
	, m_Lock(NULL)
{

}

void InputManager::AttachWindow(GLFWwindow* window)
{
	// Set the window
	m_pWindow = window;

	// If we have a valid window set the callback functions
	if (m_pWindow)
	{
		glfwSetMouseButtonCallback(m_pWindow, [](GLFWwindow* window, int button, int action, int mods) {
			Singleton<InputManager>::Instance().MouseEvent(button, action, mods);
		});
		glfwSetKeyCallback(m_pWindow, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
			Singleton<InputManager>::Instance().KeyboardEvent(key, scancode, action, mods);
		});
		glfwSetCharCallback(m_pWindow, [](GLFWwindow* window, unsigned int codepoint) {
			Singleton<InputManager>::Instance().CharEvent(codepoint);
		});
		glfwSetScrollCallback(m_pWindow, [](GLFWwindow* window, double x, double y) {
			Singleton<InputManager>::Instance().ScrollEvent(x, y);
		});

		// Get the screen size
		glfwGetWindowSize(m_pWindow, &m_ScreenSize.x, &m_ScreenSize.y);
	}
}

void InputManager::Update()
{
	// Get the cursor position
	if (m_pWindow)
	{
		double x, y;
		glfwGetCursorPos(m_pWindow, &x, &y);
		m_CursorPosition.Set(static_cast<int>(x), static_cast<int>(y));
	}
}

const vector2di& InputManager::GetScreenSize() const
{
	return m_ScreenSize;
}

const vector2di& InputManager::GetCursorPosition() const
{
	return m_CursorPosition;
}

bool InputManager::SetCursorPosition(InputObject* who, const vector2di& pos)
{
	if (m_pWindow && (Locked(who) || who->IsOverride()))
	{
		glfwSetCursorPos(m_pWindow, static_cast<double>(pos.x), static_cast<double>(pos.y));
		m_CursorPosition = pos;

		return true;
	}

	return false;
}

void InputManager::SetCursorVisible(bool visible)
{
	if (m_pWindow)
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, visible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_HIDDEN);
}

bool InputManager::IsCursorVisible() const
{
	if (m_pWindow)
		return (glfwGetInputMode(m_pWindow, GLFW_CURSOR) == GLFW_CURSOR_NORMAL);

	return false;
}

bool InputManager::IsKeyPressed(InputObject* who, int key) const
{
	if (m_pWindow && (Locked(who) || who->IsOverride()))
		return glfwGetKey(m_pWindow, key) == GLFW_PRESS;

	return false;
}

bool InputManager::IsButtonPressed(InputObject* who, int button) const
{
	if (m_pWindow && (Locked(who) || who->IsOverride()))
		return glfwGetMouseButton(m_pWindow, button);

	return false;
}

bool InputManager::Lock(InputObject* who)
{
	// We will return true if we are not already locked and can change lock to who
	if (!m_Lock && m_Lock != who)
	{
		m_Lock = who;
		return true;
	}

	// Return false if who has us locked already, or if we can't change
	return false;
}

void InputManager::Unlock(InputObject* who)
{
	if (m_Lock == who)
		m_Lock = NULL;
}

void InputManager::AddInputObject(InputObject* object)
{
	// Add a new input object at the level if there isn't one there already
	if (std::find(m_InputObjects.begin(), m_InputObjects.end(), object) == m_InputObjects.end())
		m_InputObjects.push_back(object);
}

void InputManager::RemoveInputObject(InputObject* object)
{
	// Find the object in our map and delete it if it exists
	InputVector::iterator it = m_InputObjects.begin();
	while (it != m_InputObjects.end())
	{
		// If this is us, delete us
		if (*it == object)
		{
			m_InputObjects.erase(it);
			break;
		}

		// Move onto the next object
		++it;
	}
}

void InputManager::MouseEvent(int button, int action, int mods)
{
	// Iterate through our input objects and stop when one claims the input
	for (auto& it : m_InputObjects) {
		if ((Locked(it) || it->IsOverride()) && it->MouseEvent(button, action, mods))
			break;
	}
}

void InputManager::KeyboardEvent(int key, int scancode, int action, int mods)
{
	// Iterate through our input objects and stop when one claims the input
	for (auto& it : m_InputObjects) {
		if ((Locked(it) || it->IsOverride()) && it->KeyboardEvent(key, scancode, action, mods))
			break;
	}
}

void InputManager::CharEvent(unsigned int codepoint)
{
	// Iterate through our input objects and stop when one claims the input
	for (auto& it : m_InputObjects) {
		if ((Locked(it) || it->IsOverride()) && it->CharEvent(codepoint))
			break;
	}
}

void InputManager::ScrollEvent(double x, double y)
{
	// Iterate through our input objects and stop when one claims the input
	for (auto& it : m_InputObjects) {
		if ((Locked(it) || it->IsOverride()) && it->ScrollEvent(x, y))
			break;
	}
}

bool InputManager::Locked(InputObject* who) const
{
	return (!m_Lock || m_Lock == who);
}
