#ifndef PERLIN_NOISE_H_
#define PERLIN_NOISE_H_

#include <vector>

class PerlinNoise {
public:
	PerlinNoise(unsigned int seed);

	void SetSeed(unsigned int seed);
	double GetNoise(double x, double y, double z) const;

protected:

private:
	double _Fade(double t) const;
	double _Lerp(double t, double a, double b) const;
	double _Grad(int hash, double x, double y, double z) const;

	std::vector<int> m_Permutations;
};

#endif // PERLIN_NOISE_H_