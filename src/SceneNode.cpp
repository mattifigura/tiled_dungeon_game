#include "SceneNode.h"
#include "SceneManager.h"
#include "Mesh.h"
#include "GLTexture.h"

SceneNode::SceneNode(SceneManager* sceneMan)
	: m_Mesh(NULL)
	, m_Texture(NULL)
	, m_pSceneMan(sceneMan)
	, m_Transformation()
	, m_Position()
	, m_Scale(1.f)
	, m_Rotation()
	, m_bDead(false)
	, m_bVisible(true)
	, m_iRenderMode(GL_TRIANGLES)
	, m_bClearDepthBuffer(false)
	, m_bCullFrontFace(false)
	, m_bOutline(false)
{

}

SceneNode::~SceneNode()
{

}

void SceneNode::Update()
{
	// Construct our transformation matrix
	m_Transformation.Identity();
	
	// Scale
	m_Transformation.Scale(GetScale());
	
	// Rotation
	if (!m_Quaternion)
		m_Transformation.SetRotation(GetRotation(), GetRotationOffset());
	else
	{
		// Set our rotation from a quaternion
		matrix4f m;
		m_Transformation.SetTranslation(GetRotationOffset());
		m.SetFromQuaternion(m_Quaternion);
		m_Transformation = m_Transformation * m;
		m_Transformation.SetTranslation(-GetRotationOffset());
	}
	
	// Translation
	m_Transformation.SetTranslation(GetPosition());
	
	// Calculate normal matrix
	m_NormalMatrix = m_Transformation;
	m_NormalMatrix.Inverse();
	m_NormalMatrix.Transpose();
}

void SceneNode::SetPosition(const vector3df& position)
{
	m_Position = position;
}

const vector3df& SceneNode::GetPosition() const
{
	return m_Position;
}

void SceneNode::SetScale(const vector3df& scale)
{
	m_Scale = scale;
}

const vector3df& SceneNode::GetScale() const
{
	return m_Scale;
}

void SceneNode::SetRotation(const vector3df& rotation)
{
	m_Rotation = rotation;
}

const vector3df& SceneNode::GetRotation() const
{
	return m_Rotation;
}

void SceneNode::SetRotationOffset(const vector3df& offset)
{
	m_RotationOffset = offset;
}

const vector3df& SceneNode::GetRotationOffset() const
{
	return m_RotationOffset;
}

void SceneNode::SetMesh(const MeshBase* mesh)
{
	m_Mesh = mesh;
}

void SceneNode::SetMesh(const std::string& key)
{
	m_Mesh = &m_pSceneMan->GetMesh(key);
}

const MeshBase* SceneNode::GetMesh() const
{
	return m_Mesh;
}

void SceneNode::SetTexture(const GLTexture* tex)
{
	m_Texture = tex;
}

void SceneNode::SetTexture(const std::string& filename)
{
	m_Texture = &m_pSceneMan->GetTexture(filename);
}

const GLTexture* SceneNode::GetTexture() const
{
	return m_Texture;
}

const matrix4f& SceneNode::GetTransformationMatrix() const
{
	return m_Transformation;
}

const matrix4f& SceneNode::GetNormalMatrix() const
{
	return m_NormalMatrix;
}

void SceneNode::SetQuaternion(const quaternion& quat)
{
	m_Quaternion = quat;
}

void SceneNode::Kill()
{
	m_bDead = true;
}

bool SceneNode::IsDead() const
{
	return m_bDead;
}

void SceneNode::SetVisible(bool visible)
{
	m_bVisible = visible;
}

bool SceneNode::IsVisible() const
{
	return m_bVisible;
}

void SceneNode::SetRenderMode(int mode)
{
	m_iRenderMode = mode;
}

int SceneNode::GetRenderMode() const
{
	return m_iRenderMode;
}

void SceneNode::SetClearsDepthBuffer(bool clears)
{
	m_bClearDepthBuffer = clears;
}

bool SceneNode::ClearsDepthBuffer() const
{
	return m_bClearDepthBuffer;
}

void SceneNode::SetCullFrontFace(bool cull)
{
	m_bCullFrontFace = cull;
}

bool SceneNode::CullFrontFace() const
{
	return m_bCullFrontFace;
}

void SceneNode::SetOutline(bool outline)
{
	m_bOutline = outline;
}

bool SceneNode::IsOutline() const
{
	return m_bOutline;
}

void SceneNode::AssignNewDynamicMesh()
{
	m_DynamicMesh.reset(new Mesh<GLVertex>);
	m_Mesh = m_DynamicMesh.get();
}

Mesh<GLVertex>* SceneNode::GetDynamicMesh()
{
	return m_DynamicMesh.get();
}
