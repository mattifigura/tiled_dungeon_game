#include "EnemyController.h"
#include "UnitControllerManager.h"
#include "SceneManager.h"
#include "Functions.h"
#include "CameraController.h"

EnemyController::EnemyController(SceneManager* sceneman, UnitControllerManager* man)
	: UnitController(sceneman, man, nsControllerNames::enemy)
	, m_CurrentEnemy(-1)
	, m_Mode(nsEnemyMode::movement)
	, m_TransitionMode(nsEnemyMode::movement)
	, m_Moving(false)
	, m_Target(nullptr)
	, m_PlayerController(nullptr)
	, m_EnemyLight(nullptr)
	, m_Active(false)
	, m_JustAttacked(false)
	, m_Delayed(false)
{
	// Set our texture
	GetBillboard()->SetTexture("media/characters.png");
	
	// Add the level walls to our pathfinder (TODO: Move this into a post-generation function as we'll need to do this when moving to a new room)
	const Terrain& terrain = sceneman->GetTerrain();
	for (int x = 0; x < AREA_SIZE; ++x) {
		for (int y = 0; y < AREA_SIZE; ++y) {
			vector3df pos(static_cast<float>(x) + 0.5f, 0.5f, static_cast<float>(y) + 0.5f);
			if (terrain.GetHeightBelow(pos) != 0.f)
			{
				// We can't move here, add a wall to the pathfinder
				GetPathFinder().SetWall(vector2di(x, y), true);
			}
		}
	}
	
	m_PlayerController = GetPlayerController();
	
	m_EnemyLight = sceneman->CreateLight(vector2df(-9001.f, -9001.f), GLColour(150, 70, 70, 255), vector2df(2.f, 2.f));
	
	// Set us as the enemy controller
	SetEnemyController(this);
}

void EnemyController::Update()
{
	// Call individual update methods
	UpdateUnits();
	UpdateCamera();
	
	// Call base class update to keep our enemy characters updated
	UnitController::Update();
}

void EnemyController::UpdateBackground()
{
	// Call base class update to keep our enemy characters updated
	UnitController::UpdateBackground();
}

void EnemyController::TurnStart()
{
	// At the beginning of the turn (enemy's turn) we'll need to reset our unit data
	m_Active = true;
	m_UnitData.clear();
	size_t count = GetUnitCount();
	for (size_t i = 0; i < count; ++i) {
		m_UnitData.push_back(GetUnit(i)->GetUnitData());
	}
	m_CurrentEnemy = -1;
	m_Mode = nsEnemyMode::movement;
	m_Target = nullptr;
	
	// Add our units to the pathfinder
	UpdateUnitGridPositions();
	for (auto pos : GetUnitGridPositions()) {
		GetPathFinder().SetWall(pos, true);
	}
	
	// Add the players to the pathfinder
	m_PlayerController->UpdateUnitGridPositions();
	for (auto pos : m_PlayerController->GetUnitGridPositions()) {
		GetPathFinder().SetWall(pos, true);
	}
	
	// Populate our units to process vector
	m_UnitsToProcess.clear();
	if (m_PlayerController->GetClosestUnit(vector3df()))
	{
		for (size_t i = 0; i < count; ++i) {
			Unit* unit = GetUnit(i);
			if (unit)
			{
				if (Distance(unit, m_PlayerController->GetClosestUnit(unit->GetPosition())) <= 12)
					m_UnitsToProcess.push_back(static_cast<int>(i));
			}
		}
		
		// Sort the units to process vector so that units closest to players go first
		std::sort(m_UnitsToProcess.begin(), m_UnitsToProcess.end(), [&](const int& one, const int& two) -> bool {
			Unit* enemy_one = GetUnit(one);
			Unit* enemy_two = GetUnit(two);
			
			return enemy_one->GetPosition().QuickDistance(m_PlayerController->GetClosestUnit(enemy_one->GetPosition())->GetPosition()) <
				enemy_two->GetPosition().QuickDistance(m_PlayerController->GetClosestUnit(enemy_two->GetPosition())->GetPosition());
		});
	}
}

void EnemyController::TurnEnd()
{
	// Remove our units from the pathfinder
	m_Active = false;
	UpdateUnitGridPositions();
	for (auto pos : GetUnitGridPositions()) {
		GetPathFinder().SetWall(pos, false);
	}
	
	// Remove the players from the pathfinder
	m_PlayerController->UpdateUnitGridPositions();
	for (auto pos : m_PlayerController->GetUnitGridPositions()) {
		GetPathFinder().SetWall(pos, false);
	}
	
	m_EnemyLight->position.Set(-9001.f, -9001.f);
}

void EnemyController::PlayerKilled(const vector2df& pos)
{
	// Remove the character from our path finder
	GetPathFinder().SetWall(pos, false);
}

void EnemyController::UpdateCamera()
{
	// Stop crashes
	if (!m_Active)
		return;
	
	// Update the camera based on what we're doing
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	bool animating = camera_controller.IsAnimating();
	
	Unit* unit = GetUnit(m_CurrentEnemy);
	if (unit->IsMoving())
		camera_controller.SetTargetUnit(unit, 1.f);
	
	if (m_Mode != nsEnemyMode::camera && animating)
	{
		m_TransitionMode = m_Mode;
		m_Mode = nsEnemyMode::camera;
	}
	else if (m_Mode == nsEnemyMode::camera && !animating)
	{
		m_Mode = m_TransitionMode;
	}
}

void EnemyController::UpdateUnits()
{
	// Choose the next unit
	if (!ChooseUnit())
	{
		GetManager()->NextController();
		m_Active = false;
		return;
	}
	
	// Control units
	switch (m_Mode)
	{
		case nsEnemyMode::movement:
		{
			if (UpdateMovement())
			{
				// Check if we have any attack points to spend
				if (m_UnitData.at(m_CurrentEnemy).action_count > 0)
				{
					m_Mode = nsEnemyMode::attack;
				}
				else
				{
					// Move onto the next enemy
					m_Moving = false;
				}
			}
		}
		break;
		case nsEnemyMode::attack:
		{
			if (UpdateAttack())
			{
				// Firstly check if we used all our movement points
				if (m_UnitData.at(m_CurrentEnemy).movement <= 0)
				{
					// Move onto the next enemy
				}
				else
				{
					m_JustAttacked = true;
					m_Moving = false;
				}
				
				m_Mode = nsEnemyMode::movement;
			}
		}
		break;
		case nsEnemyMode::camera:
		{
			UpdateLight();
		}
		default:
			break;
	}
}

bool EnemyController::UpdateMovement()
{
	bool finished(false);
	Unit* current = GetUnit(m_CurrentEnemy);
	UnitData& data = m_UnitData.at(m_CurrentEnemy);
	const Terrain& terrain = GetSceneManager()->GetTerrain();
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	bool animating = camera_controller.IsAnimating();
	
	if (current && !current->IsMoving() && !animating)
	{
		// Finished our movement?
		if (m_Moving)
		{
			m_Moving = false;
			finished = true;
			
			// Refresh our unit positions
			for (auto pos : GetUnitGridPositions()) {
				GetPathFinder().SetWall(pos, false);
			}
			
			UpdateUnitGridPositions();

			for (auto pos : GetUnitGridPositions()) {
				GetPathFinder().SetWall(pos, true);
			}
		}
		else
		{
			/// TODO: Tactical movement, ie consider how far we can go, our attack range etc
			// Limit the path to our max movement, and remove the player position
			int amount = data.movement + 1;
			std::vector<vector2di> path;
			vector2di end(m_Target->GetPosition().x, m_Target->GetPosition().z);
			size_t ability_index = 0;
			const Ability& ability = AbilityController::GetAbility(data.abilities[ability_index]);
			
			for (auto& pos : m_TempPath) {
				// Stop if we have reached the end of our path or have run out of movement
				if (pos == end || amount == 0)
					break;
				
				// If our range is greater than one, consider if we can attack our target from here
				///TODO: Check all our abilities (that can be used) for range checks
				// NOTE: The above TODO will be behaviour specific, this is the basic "move until we can attack" AI
				// so we need not worry about anything more complicated than "can our first ability attack from here?"
				if (ability.range > 1 && Distance(pos, end) <= ability.range)
				{
					// Check if we can attack from here
					vector3df here(pos.x + 0.5f, 0.25f, pos.y + 0.5f);
					if (!terrain.DoesLineCollideWithTerrain(line3df(here, m_Target->GetPosition())))
					{
						// This will complete our path
						path.push_back(pos);
						--amount;
						break;
					}
				}
				
				// This position can be pushed back
				path.push_back(pos);
				--amount;
			}
			
			// Now move our unit
			if (path.size() > 0)
			{
				for (auto pos : path) {
					current->AddMovementPoint(vector3df(static_cast<float>(pos.x) + 0.5f, 0.f, static_cast<float>(pos.y) + 0.5f));
				}
				data.movement -= path.size();
				
				m_Moving = true;
			}
			else
			{
				finished = true;
			}
		}
	}
	
	UpdateLight();
	
	return finished;
}

bool EnemyController::UpdateAttack()
{
	bool finished(false);
	Unit* current = GetUnit(m_CurrentEnemy);
	UnitData& data = m_UnitData.at(m_CurrentEnemy);
	int ability_index = 0; ///TODO: AI PROGRAMMING: Enemies may have multiple abilities
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	bool animating = camera_controller.IsAnimating();
	
	if (current && !current->IsMoving())
	{
		const Ability& ability = AbilityController::GetAbility(data.abilities[ability_index]);
		
		if (data.action_count >= ability.cost)
		{
			auto use_ability = [&]() {
				// Hit the player
				if (GetAbilityController().UseAbility(data.abilities[ability_index], current, vector2di(m_Target->GetPosition().x, m_Target->GetPosition().z), data.ability_values[ability_index]))
				{
					data.action_count -= ability.cost;
					camera_controller.SetTargetUnit(current, 1.25f);
				}
			};
			
			if (CanAttackTarget(current->GetPosition(), ability))
			{
				use_ability();
			}
			else
			{
				// Try another target
				Unit* previous_target = m_Target;
				std::vector<Unit*> previous;
				previous.push_back(m_Target);
				
				bool valid = false;
				UpdateTarget(valid, previous);
				
				if (m_Target && previous_target != m_Target && CanAttackTarget(current->GetPosition(), ability))
				{
					use_ability();
				}
				else
					finished = true;
			}
		}
		else if (!animating)
		{
			finished = true;
		}
	}
	else if (!animating)
	{
		finished = true;
	}
	
	return finished;
}

bool EnemyController::ChooseUnit()
{
	// Check in case we want to move onto the next enemy
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	bool animating = camera_controller.IsAnimating();
	bool valid(m_CurrentEnemy > -1 && (m_Moving || m_JustAttacked || m_Delayed || animating || m_Mode == nsEnemyMode::camera || m_Mode == nsEnemyMode::attack));
	Unit* enemy = nullptr;

	// AAAAH well as we're using the camera controller IsAnimating we need to add a delay before units attack
	// Otherwise they'll attack as soon as they become the current unit (if they are in range)
	// This is bad as it is not clear to the player what has just happened in some cases
	if (m_Delayed && m_Mode != nsEnemyMode::camera)
	{
		m_Delayed = false;
	}

	// Used to allow us to move after attacking if our enemy has any movement left
	// NOTE: Never used to need this but after fixing another bug this had to be introduced :(
	if (m_JustAttacked)
	{
		m_JustAttacked = false;
		valid = false;
		
		// Update our target to choose a new player to attack
		std::vector<Unit*> previous;
		UpdateTarget(valid, previous);
	}

	while (!valid && !m_UnitsToProcess.empty())
	{
		m_CurrentEnemy = *m_UnitsToProcess.begin();
		m_UnitsToProcess.erase(m_UnitsToProcess.begin());
		
		enemy = GetUnit(m_CurrentEnemy);
		
		// If the enemy isn't valid, our turn is over
		if (!enemy)
		{
			valid = false;
			break;
		}
		
		std::vector<Unit*> previous;
		UpdateTarget(valid, previous);
		
		// We might need to check the next enemy if this one isn't valid
		if (!valid)
		{
			m_Moving = false;
		}
#ifndef NDEBUG
		else
		{
			std::cout << "Next chosen enemy unit: " << m_CurrentEnemy << std::endl;
		}
#endif
	}
	
	return valid;
}

void EnemyController::UpdateLight()
{
	Unit* current = GetUnit(m_CurrentEnemy);
	if (current)
	{
		m_EnemyLight->position.Set(current->GetPosition().x, current->GetPosition().z);
	}
}

void EnemyController::UpdateTarget(bool& valid, std::vector<Unit*>& previous)
{
	// 3 attempts to get the closest player, max team size
	int attempts = 0;
	Unit* enemy = GetUnit(m_CurrentEnemy);
	UnitData& data = m_UnitData.at(m_CurrentEnemy);
	int ability_index = 0; ///TODO: AI PROGRAMMING: Enemies may have multiple abilities
	const Terrain& terrain = GetSceneManager()->GetTerrain();
	
	if (enemy)
	{
		vector2di start(enemy->GetPosition().x, enemy->GetPosition().z);
		
		while (!valid && attempts++ < 3)
		{
			m_Target = m_PlayerController->GetClosestUnit(enemy->GetPosition(), &previous);
			if (m_Target)
			{
				// Add this player to the previous list so the next attempt gets a different player
				previous.push_back(m_Target);
				m_TempPath.clear();
				
				// Check if we can attack this target before calculating a movement path
				const Ability& ability = AbilityController::GetAbility(data.abilities[ability_index]);
				if (CanAttackTarget(enemy->GetPosition(), ability))
				{
					valid = true;
					m_Delayed = true;
					Singleton<CameraController>::Instance().SetTargetUnit(enemy, 0.6f);
				}
				
				// If we can't attack see if we have a path to move towards them
				if (!valid)
				{
					// We need to remove our target from the pathfinder walls
					vector2di end(m_Target->GetPosition().x, m_Target->GetPosition().z);
					
					if (GetPathFinder().FindPath(start, end, m_TempPath, 320))
					{
						valid = true;
						
						// If this path doesn't lead to war (if we can't attack at the end) then
						// check every step and see if there's a better target to go for
						vector2di check_point;
						int step_count = 0;
						for (auto& point : m_TempPath) {
							check_point = point;
							if (++step_count == data.movement)
								break;
						}
						
						step_count = 0;
						if (!CanAttackTarget(vector3df(check_point.x + 0.5f, 0.25f, check_point.y + 0.5f), ability))
						{
							bool switch_target(false);
							for (auto& it : m_TempPath) {
								vector3df point(it.x + 0.5f, 0.25f, it.y + 0.5f);
								Unit* closest = m_PlayerController->GetClosestUnit(point, &previous);
								if (closest && closest != m_Target && (point.QuickDistance(closest->GetPosition()) < point.QuickDistance(m_Target->GetPosition())) &&
									!terrain.DoesLineCollideWithTerrain(line3df(point, closest->GetPosition())))
								{
									// There is a target that is closer (ignoring walls) to us at this point than our target
									// Do a path check to see if it is possible to path to this target
									vector2di potential_end(closest->GetPosition().x, closest->GetPosition().z);
									std::vector<vector2di> potential_path;
									size_t distance_to_current = m_TempPath.size() - ++step_count;
									
									if (GetPathFinder().FindPath(it, potential_end, potential_path, 240) && potential_path.size() < distance_to_current)
									{
										m_Target = closest;
										switch_target = true;
									}
								}
							}
							
							if (switch_target)
							{
								// Recalculate path to new target
								m_TempPath.clear();
								
								end = vector2di(m_Target->GetPosition().x, m_Target->GetPosition().z);
								GetPathFinder().FindPath(start, end, m_TempPath);
							}
						}
						
#ifndef NDEBUG
						std::cout << "EnemyController::UpdateTarget: Target: " << m_Target->GetID() << std::endl;
#endif
					}
				}
			}
		}
	}
}

bool EnemyController::CanAttackTarget(const vector3df& pos, const Ability& ability)
{
	const Terrain& terrain = GetSceneManager()->GetTerrain();
	
	return Distance(pos, m_Target->GetPosition()) <= ability.range + ability.area &&
		(ability.range < 2 || !terrain.DoesLineCollideWithTerrain(line3df(pos, m_Target->GetPosition())));
}