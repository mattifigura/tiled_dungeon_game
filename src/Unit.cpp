#include "DeltaTime.h"
#include "Unit.h"
#include "StatusEffectManager.h"

size_t Unit::m_GlobalID = 0;
std::vector<Unit*> Unit::m_GlobalUnits;

Unit::Unit(nsControllerNames::Controller controller)
	: m_ID(m_GlobalID++)
	, m_Position()
	, m_Destination()
	, m_Board(nullptr)
	, m_Data()
	, m_MovementQueue()
	, m_Moving(false)
	, m_Name("")
	, m_Health(1)
	, m_AttackAnimation(0.f)
	, m_AttackOffset(0.f)
	, m_AttackDirection(0.f)
	, m_HitAnimation(0.f)
	, m_Owner(controller)
{
	UnitCreated();
}

Unit::~Unit()
{
	// I'm probably being paranoid about memory handling
	std::string().swap(m_Data.abilities[0]);
	std::string().swap(m_Data.abilities[1]);
	std::string().swap(m_Data.abilities[2]);
	std::string().swap(m_Data.abilities[3]);
	
	std::queue<vector3df> empty;
	std::swap(m_MovementQueue, empty);
	
	// We are no more
	UnitDestroyed();
}

void Unit::CreateUnit()
{
	// This should be overriden by classes to create unique units
	m_Board->size.Set(vector2df(0.5f, 0.5f));
	m_Board->position = vector3df(11.5f, 0.25f, 10.5f);
	m_Board->animation = 0;
	m_Board->animation_speed = 2.5f;
}

void Unit::Update()
{
	// Handle moving to the next point
	double delta = Singleton<DeltaTime>::Instance().GetDelta();
	m_Moving = false;
	if (abs(m_Position.x - m_Destination.x) > 0.02f || abs(m_Position.z - m_Destination.z) > 0.02f)
	{
		m_Moving = true;
		
		if (m_Position.x < m_Destination.x)
			m_Position.x += 2.f * delta;
		else if (m_Position.x > m_Destination.x)
			m_Position.x -= 2.f * delta;
			
		if (m_Position.z < m_Destination.z)
			m_Position.z += 2.f * delta;
		else if (m_Position.z > m_Destination.z)
			m_Position.z -= 2.f * delta;
		
		if (abs(m_Position.x - m_Destination.x) <= 0.02f)
			m_Position.x = m_Destination.x;
		if (abs(m_Position.z - m_Destination.z) <= 0.02f)
			m_Position.z = m_Destination.z;
	}
	
	if (!m_MovementQueue.empty() && !m_Moving)
	{
		m_Destination = m_MovementQueue.front();
		m_MovementQueue.pop();
		m_Moving = true;
	}
	
	// Attack animations
	if (m_AttackAnimation != 0.f)
	{
		m_AttackOffset += m_AttackAnimation * delta;
		if (m_AttackOffset > 0.2f && m_AttackAnimation > 0.f)
			m_AttackAnimation *= -1;
		
		if (m_AttackOffset <= 0.f)
		{
			m_AttackAnimation = 0.f;
			m_AttackOffset = 0.f;
		}
	}
	vector3df attack(sin(m_AttackDirection) * m_AttackOffset, 0.f, cos(m_AttackDirection) * m_AttackOffset);
	
	// Hit animations
	vector3df hit;
	if (m_HitAnimation > 0.f)
	{
		m_HitAnimation -= 5.f * delta;
		
		hit.Set(static_cast<float>(-5 + rand() % 11) / 50.f, 0.f, static_cast<float>(-5 + rand() % 11) / 50.f);
	}
	
	// Update our billboard's position
	m_Board->position = m_Position + attack + hit;
}

void Unit::AddMovementPoint(const vector3df& point)
{
	m_MovementQueue.push(point);
}

bool Unit::Damage(int amount, Unit* other)
{
	// Apply any status effects
	if (Singleton<StatusEffectManager>::Instance().OnDamage(GetID(), amount, other))
		return false;

	// Returns true if we are dead
	m_HitAnimation = 1.f;
	if (m_Health - amount <= 0)
	{
		m_Health = 0;
		return true;
	}
	
	m_Health -= amount;
	return false;
}

bool Unit::Heal(int amount, Unit* other)
{
	// Apply any status effects
	if (Singleton<StatusEffectManager>::Instance().OnHeal(GetID(), amount, other))
		return false;

	// Returns true if we have been healed
	if (m_Health < m_Data.health)
	{
		m_Health = std::min(m_Health + amount, m_Data.health);
		return true;
	}
	
	return false;
}

void Unit::AttackAnimate(const vector3df& pos)
{
	m_AttackDirection = -atan2(pos.z - m_Position.z, pos.x - m_Position.x) + PI_OVER_2;
	m_AttackAnimation = 2.f;
}