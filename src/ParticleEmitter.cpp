#include "ParticleEmitter.h"
#include "SceneManager.h"
#include "Functions.h"
#include <iostream>

ParticleEmitter::ParticleEmitter(SceneManager* sceneman, size_t size)
	: m_SceneMan(sceneman)
	, m_Size(size)
	, m_Particles(new Particle[m_Size])
	, m_ParticleData(new ParticleData[m_Size])
	, m_Behaviour()
	, m_CanBuffer(true)
	, m_DataChanged(true)
	, m_Dead(false)
	, m_AffectedByLighting(true)
	, m_BufferObject(0)
{
	// Give our particles different negative life values
	for (size_t i = 0; i < m_Size; ++i) {
		m_ParticleData[i].life = -m_Behaviour.duration - static_cast<float>(rand() % static_cast<int>(m_Behaviour.duration));
	}
}

ParticleEmitter::~ParticleEmitter()
{
	ClearBuffers();
	
	delete[] m_Particles;
	delete[] m_ParticleData;
	
	for (auto& light : m_Lights) {
		if (light)
		{
			light->alive = false;
		}
	}

#ifndef NDEBUG
	std::cout << "ParticleEmitter is dead" << std::endl;
#endif
}

void ParticleEmitter::Translate(const vector3df& pos)
{
	m_Position += pos;
	
	for (size_t i = 0; i < m_Size; ++i) {
		m_Particles[i].pos += pos;
	}
}

void ParticleEmitter::Burst()
{
	for (size_t i = 0; i < m_Size; ++i) {
		m_ParticleData[i].life = -1.f;
	}
}

void ParticleEmitter::Stop()
{
	m_Behaviour.only_live_once = true;
	
	// Stop any unmade particles from spawning
	for (size_t i = 0; i < m_Size; ++i) {
		if (m_ParticleData[i].life <= 0.f)
			m_ParticleData[i].life = m_ParticleData[i].end_life + 10.f;
	}
}

void ParticleEmitter::SpawnAll()
{
	// Spawn all valid particles
	for (size_t i = 0; i < m_Size; ++i) {
		if (m_ParticleData[i].life >= -1000.f)
			SpawnParticle(i);
	}
}

void ParticleEmitter::AddLighting()
{
	GLColour col(m_Behaviour.start_colour);
	//col /= 3;
	for (size_t i = 0; i < m_Size; ++i) {
		m_Lights.push_back(m_SceneMan->CreateLight(vector2df(), col, vector2df(0.1f, 0.1f)));
	}
}

Particle* ParticleEmitter::GetParticles(size_t& size) const
{
	size = m_Size;
	return m_Particles;
}

GLuint ParticleEmitter::GetBufferObject(size_t& size) const
{
	size = m_Size;
	return m_BufferObject;
}

void ParticleEmitter::Update(float delta)
{
	// Loop through the particles and update them based on the particle behaviours
	//float delta = Singleton<DeltaTime>::Instance().GetDelta();
	const GLColour& s_col = m_Behaviour.start_colour;
	const GLColour& e_col = m_Behaviour.end_colour;
	const vector2df& s_size = m_Behaviour.start_size;
	const vector2df& e_size = m_Behaviour.end_size;
	const Terrain& terrain = m_SceneMan->GetTerrain();
	vector3df move;
	bool do_colours = m_Behaviour.random_colours.empty();
	
	// Slightly different from the static function, used for applying friction
	auto apply_friction = [&](float& current, float fric, float ground = 0.f) {
		float fDif = ground - current; // 0.f - current

		if (abs(fDif) < 0.01f)
		{
			current = ground;
		}
		else
		{
			current += fDif * fric * delta;
		}
	};
	
	m_CanBuffer = false;
	
	size_t count = 0;
	for (size_t i = 0; i < m_Size; ++i) {
		Particle* part = &m_Particles[i];
		ParticleData* data = &m_ParticleData[i];
		
		// Re-spawn if necessary
		if ((data->life < 0.f && data->life >= -1000.f) || data->life >= data->end_life)
		{
			if (m_Behaviour.only_live_once && data->life >= data->end_life)
			{
				part->col.Set(0, 0, 0, 0);
				++count;
			}
			else
			{
				SpawnParticle(i);
			}
		}
		else if (data->life < -1000.f)
		{
			// Waiting to spawn, increase our life
			part->col.Set(0, 0, 0, 0);
			data->life += delta * 1000.f;
		}
		else
		{
			// Update this particle
			float age = data->life / data->end_life;
			
			// Position
			move = data->force * delta;
			
			// Handle collisions
			if (m_Behaviour.collisions)
			{
				float height = part->size.y * 0.5f;
				if (part->pos.y + move.y < height || part->pos.y + move.y > 1.f - height)
				{
					move.y = 0.f;
					
					data->force.y *= m_Behaviour.bounce.y;
					
					float mult = 1.f;
					if (data->force.y > 0.f)
					{
						mult = 10.f;
					}
					
					apply_friction(data->force.x, m_Behaviour.friction.x * mult);
					apply_friction(data->force.y, m_Behaviour.friction.y, height);
					apply_friction(data->force.z, m_Behaviour.friction.z * mult);
				}
				
				height = terrain.GetHeightBelow(part->pos + vector3df(move.x, 0.f, 0.f));
				if (height != 0.f)
				{
					move.x = 0.f;
					
					data->force.x *= m_Behaviour.bounce.x;
					apply_friction(data->force.x, m_Behaviour.friction.x);
				}
				
				height = terrain.GetHeightBelow(part->pos + vector3df(0.f, 0.f, move.z));
				if (height != 0.f)
				{
					move.z = 0.f;
					
					data->force.z *= m_Behaviour.bounce.z;
					apply_friction(data->force.z, m_Behaviour.friction.z);
				}
				
				if (move.x != 0.f && move.z != 0.f)
				{
					height = terrain.GetHeightBelow(part->pos + vector3df(move.x, 0.f, move.z));
					if (height != 0.f)
					{
						move.x = 0.f;
						
						data->force.x *= m_Behaviour.bounce.x;
						apply_friction(data->force.x, m_Behaviour.friction.x);
					}
				}
			}
			
			part->pos += move;
			
			// Gravity
			vector3df gravity = m_Behaviour.gravity;
			if (m_Behaviour.gravity_anchor)
			{
				float dist = part->pos.Distance(m_Behaviour.gravity_anchor_position);
				vector3df grav((m_Behaviour.gravity_anchor_position - part->pos).Normal());
				
				gravity = grav * (1.f - m_Behaviour.minimum_start_distance/dist) * 1.f;
				
				if (m_Behaviour.gravity_anchor_rotate)
				{
					float angle = -atan2(part->pos.z - m_Behaviour.gravity_anchor_position.z, part->pos.x - m_Behaviour.gravity_anchor_position.x);
					gravity += vector3df(sin(angle), 0.f, cos(angle));
				}
			}
			data->force += gravity * delta;
			
			// Colour
			GLColour col(m_Behaviour.start_colour);
			if (do_colours)
			{
				col.r += (e_col.r - s_col.r) * age;
				col.g += (e_col.g - s_col.g) * age;
				col.b += (e_col.b - s_col.b) * age;
				col.a += (e_col.a - s_col.a) * age;

				part->col = col;
			}

			// Size
			part->size = m_Behaviour.start_size;
			part->size.x += (e_size.x - s_size.x) * age;
			part->size.y += (e_size.y - s_size.y) * age;
			
			// Age us
			data->life += delta * 1000.f;
			
			// Update lighting (if we have it set)
			if (!m_Lights.empty())
			{
				Light* light = m_Lights.at(i);
				if (light)
				{
					light->position.Set(part->pos.x, part->pos.z);
					
					if (do_colours)
					{
						light->colour = col;
					}
				}
			}
		}
	}
	
	m_DataChanged = true;
	m_CanBuffer = true;

	// Kill us if we're done
	if (count >= m_Size)
		m_Dead = true;
}

bool ParticleEmitter::Buffer()
{
	bool buffered = false;
	
	if (m_CanBuffer && m_DataChanged)
	{
		if (VALID_BUFFER(m_BufferObject))
		{
			// Just update our existing buffer
			glBindBuffer(GL_ARRAY_BUFFER, m_BufferObject);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_Size * sizeof(Particle), m_Particles);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			buffered = true;
		}
		else
		{
			// Create new buffers
			// Call ClearBuffers just to be sure
			ClearBuffers();
			
			glGenBuffers(1, &m_BufferObject);
			if (VALID_BUFFER(m_BufferObject))
			{
				glBindBuffer(GL_ARRAY_BUFFER, m_BufferObject);
				glBufferData(GL_ARRAY_BUFFER, m_Size * sizeof(Particle), m_Particles, GL_STATIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				
				buffered = true;
			}
		}
		
		m_DataChanged = false;
	}
	
	return buffered;
}

void ParticleEmitter::ClearBuffers()
{
	if (VALID_BUFFER(m_BufferObject))
		glDeleteBuffers(1, &m_BufferObject);
	
	m_BufferObject = 0;
}

void ParticleEmitter::SpawnParticle(size_t id)
{
	// Create a particle based on our defined parameters
	Particle* part = &m_Particles[id];
	ParticleData* data = &m_ParticleData[id];
	
	if (part && data)
	{
		// Set the position
		const vector3df& lower = m_Behaviour.position_randomiser_lower;
		const vector3df& upper = m_Behaviour.position_randomiser_upper;
		vector3df offset(util::RandomBounds(lower.x, upper.x),
				util::RandomBounds(lower.y, upper.y),
				util::RandomBounds(lower.z, upper.z));
		
		// If a minimum start distance is defined, move us until we're there
		if (m_Behaviour.minimum_start_distance > 0.f && offset.Magnitude() < m_Behaviour.minimum_start_distance)
		{
			vector3df direction = offset.Normal();
			offset = direction * m_Behaviour.minimum_start_distance;
		}
		
		part->pos = GetPosition() + offset;

		// Set the colour
		if (m_Behaviour.random_colours.empty())
		{
			part->col = m_Behaviour.start_colour;
		}
		else
		{
			part->col = m_Behaviour.random_colours.at(rand() % m_Behaviour.random_colours.size());
		}

		// Set the size
		part->size = m_Behaviour.start_size;

		// Set the force
		data->force = m_Behaviour.force;
		const vector3df& force_lower = m_Behaviour.force_randomiser_lower;
		const vector3df& force_upper = m_Behaviour.force_randomiser_upper;
		data->force += vector3df(util::RandomBounds(force_lower.x, force_upper.x),
			util::RandomBounds(force_lower.y, force_upper.y),
			util::RandomBounds(force_lower.z, force_upper.z));

		// If we are moving away from the start position, adjust the force
		if (m_Behaviour.force_from_start)
		{
			vector3df n = (part->pos - GetPosition()).Normal();
			data->force *= n;
		}
		
		// Set the duration
		data->life = 0.f;
		data->end_life = m_Behaviour.duration + (m_Behaviour.duration_randomiser > 0.f ?
			static_cast<float>((rand() % static_cast<int>(m_Behaviour.duration_randomiser))) : 0.f);
	}
}
