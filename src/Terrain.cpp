#include "Terrain.h"
#include "SceneManager.h"
#include "zlib.h"
#include <algorithm>
#include <chrono>
#include <cstring>
#include <iostream>

Terrain::Terrain(SceneManager* sceneMan)
	: m_pSceneMan(sceneMan)
	, m_Texture(NULL)
	, m_TileMesh()
	, m_CurrentChunk()
	, m_PreviousChunk(-1, -1)
	, m_iCurrentArea(0)
	, m_iCurrentLevel(0)
{
	m_Meshes.push_back(&m_TileMesh);

	// Create our entity meshes
	for (auto& it : m_EntityMeshes) {
		it.reset(new Mesh<GLVertex>);
	}

	// Push back the default test level
	Area test;
	test.name = "Test Level";
	test.levels.push_back(AreaLevel());
	m_Areas.push_back(test);

	//GenerateAreaLevel();
}

Terrain::~Terrain()
{
	// Clean up
	CleanUp();
}

void Terrain::Update(const vector3df& position, const ViewFrustum& frustum)
{
	// The loading/reloading of chunks
	const vector3df& cameraPos = m_pSceneMan->GetActiveCamera().GetPosition();
	m_CurrentChunk.Set(static_cast<int>(cameraPos.x) / MESH_SIZE, static_cast<int>(cameraPos.z) / MESH_SIZE);

	if (m_CurrentChunk != m_PreviousChunk)
	{
		// Clear our meshes list and reload
		m_Meshes.clear();
		m_Meshes.push_back(&m_TileMesh);

		// Add the current chunk and the 9 surrounding
		for (int x = m_CurrentChunk.x - 1; x <= m_CurrentChunk.x + 1; ++x) {
			for (int y = m_CurrentChunk.y - 1; y <= m_CurrentChunk.y + 1; ++y) {
				if (x > -1 && y > -1 && x < MESH_COUNT && y < MESH_COUNT)
					m_Meshes.push_back(GetMesh(x, y));
			}
		}
	}
	m_PreviousChunk = m_CurrentChunk;
}

const std::vector<MeshBase*>& Terrain::GetRenderList() const
{
	return m_Meshes;
}

void Terrain::SetTexture(const GLTexture* tex)
{
	m_Texture = tex;
}

const GLTexture* Terrain::GetTexture() const
{
	return m_Texture;
}

const MeshBase* Terrain::GetEntityMesh(const nsTileEntity::TileEntity entity) const
{
	// If this returns 0 then we will create a dynamic SceneNode so we can have an animated entity
	switch (entity)
	{
	case nsTileEntity::stone_bridge:
		return &m_pSceneMan->GetMesh("media/stone_bridge_0.vxla");
	case nsTileEntity::sign:
		return &m_pSceneMan->GetMesh("media/sign.vxla");
	case nsTileEntity::chest:
		return &m_pSceneMan->GetMesh("media/chest.vxla");
	default:
		return NULL;
	}
}

vector3df Terrain::GetEntityOffset(const TileEntity& entity) const
{
	vector3df offset;

	switch (entity.type)
	{
	case nsTileEntity::stone_door:
	{
		// We need to move the door into position
		switch (entity.rot)
		{
		case 0:
			offset.Set(0.265f, 0.f, 0.f);
			break;
		case 4:
			offset.Set(0.f, 0.f, -0.265f);
			break;
		case 8:
			offset.Set(-0.265f, 0.f, 0.f);
			break;
		case 12:
			offset.Set(0.f, 0.f, 0.265f);
			break;
		default:
			// Do nothing
			break;
		}
	}
	break;
	default:
		// Do nothing
		break;
	}

	return offset;
}

void Terrain::GenerateAreaLevel()
{
	// Generate a voronoi level
	//PerlinNoise noise(time.count());
	std::chrono::duration<double> time = std::chrono::system_clock::now().time_since_epoch();
	srand(time.count());
	srand(1001001);

	// Voronoi diagram
	auto dist = [](const vector2di& one, const vector2di& two) -> double {
		return (std::max(abs(two.x - one.x), abs(two.y - one.y)));
	};

	struct BiomePoint {
		vector2di pos;
		bool land;
		int biome;
	};

	// Generate the texture and bind it
	const int imageSize = AREA_SIZE;
	std::vector<unsigned char> biome(imageSize * imageSize, 0);
	std::vector<bool> walls(imageSize * imageSize, false);
	std::vector<std::vector<vector2di>> rooms;

	// Create the voronoi points
	int numpoints = 30;
	std::vector<BiomePoint> voronoiPoints(numpoints);
	for (int i = 0; i < numpoints; i++) {
		int x = rand() % imageSize;
		int y = rand() % imageSize;
		
		x -= (x % 5);
		y -= (y % 5);

		voronoiPoints[i].land = 1;
		voronoiPoints[i].pos = vector2di(x, y);
		voronoiPoints[i].biome = i;
		rooms.push_back(std::vector<vector2di>());
	}

	auto voronoi = [&voronoiPoints, dist, &numpoints, &biome, &rooms](int x, int y, int& out) -> bool {
		bool ret = false;
		double _dist = 99999999;
		for (int i = 0; i < numpoints; i++) {
			double tmp = dist(vector2di(x, y), voronoiPoints[i].pos);
			if (tmp < _dist)
			{
				_dist = tmp;
				ret = voronoiPoints[i].land;
				out = voronoiPoints[i].biome;
				biome[x * imageSize + y] = out;
				rooms.at(out).push_back(vector2di(x, y));
			}
		}

		return ret;
	};

	// Fill out the new island
	for (int x = 0; x < imageSize; ++x) {
		for (int y = 0; y < imageSize; ++y) {
			int n = 0;
			if (voronoi(x, y, n))
			{
				SetTile(nsTile::dirt, x, y);
			}
			
			if (x == 0 || x == imageSize - 1 || y == 0 || y == imageSize - 1)
			{
				SetTile(nsTile::stone_wall_1, x, y);
				walls[x * imageSize + y] = true;
			}
		}
	}
	
	// Walls
	for (int x = 1; x < imageSize - 1; ++x) {
		for (int y = 1; y < imageSize - 1; ++y) {
			if (biome[x * imageSize + y] != biome[(x - 1) * imageSize + y] ||
				biome[x * imageSize + y] != biome[x * imageSize + y - 1] ||
				biome[x * imageSize + y] != biome[(x - 1) * imageSize + y - 1])
			{
				SetTile(nsTile::stone_wall_1, x, y);
				walls[x * imageSize + y] = true;
			}
		}
	}
	
	// Connect rooms
	for (int x = 1; x < imageSize - 1; ++x) {
		for (int y = 1; y < imageSize - 1; ++y) {
			if (walls[x * imageSize + y] &&
				biome[x * imageSize + y - 1] + biome[(x - 1) * imageSize + y] + biome[(x + 1) * imageSize + y] + biome[x * imageSize + y + 1] >= 2 &&
				rand() % 20 > 13)
				
			{
				SetTile(nsTile::dirt, x, y);
				biome[x * imageSize + y] = 1;
				walls[x * imageSize + y] = false;
			}
		}
	}
	
	// Function to ensure walls are proper innit
	auto check_corner = [&](const vector2di& here, const vector2di& there) -> bool {
		return (!walls[there.x * imageSize + there.y] && walls[here.x * imageSize + there.y] && walls[there.x * imageSize + here.y]);
	};
	
	// Make sure there are no diagonal gaps in walls
	for (int x = 1; x < imageSize - 1; ++x) {
		for (int y = 1; y < imageSize - 1; ++y) {
			if (!walls[x * imageSize + y] &&
				(check_corner(vector2di(x, y), vector2di(x - 1, y - 1)) || check_corner(vector2di(x, y), vector2di(x + 1, y - 1)) ||
				check_corner(vector2di(x, y), vector2di(x - 1, y + 1)) || check_corner(vector2di(x, y), vector2di(x + 1, y + 1))))
			{
				SetTile(nsTile::stone_wall_1, x, y);
				walls[x * imageSize + y] = true;
				biome[x * imageSize + y] = 0;
			}
		}
	}

	// Spawn enemies
	m_SpawnPoints.clear();
	for (auto& room : rooms) {
		int spawn_num = room.size() / 64;
		
		if (room.size() > 8)
		{
			for (int i = 0; i < spawn_num; ++i) {
				// Choose a random point in the room
				vector2di point = room.at(rand() % room.size());
				while (std::find(m_SpawnPoints.begin(), m_SpawnPoints.end(), point) != m_SpawnPoints.end() || walls[point.x * imageSize + point.y])
				{
					point = room.at(rand() % room.size());
				}
				
				m_SpawnPoints.push_back(point);
			}
		}
	}
	
	// Now build our mesh
	m_TileMesh.CreateMesh(this);
	
	// Add lights
	/*for (int i = 0; i < AREA_SIZE; ++i) {
		for (int j = 0; j < AREA_SIZE; ++j) {
			if (GetTile(i, j).tile != nsTile::stone_wall_1)
				m_pSceneMan->CreateLight(vector2df(i + 0.5f, j + 0.5f), GLColour(80, 70, 50, 255), 28);
		}
	}*/
	
	return;
}

bool Terrain::DoesLineCollideWithTerrain(const line3df& ray, vector3df* out) const
{
	// Do a line trace to determine whether the given line collides with the terrain and if so, where
	vector3df pos(ray.start);
	
	vector3df dir = (ray.end - pos).Normal() * 0.1f;
	
	while (vector3di(pos) != vector3di(ray.end))
	{
		const Tile& entity = GetTile(pos);
		if (entity.tile != nsTile::empty && entity.tile < nsTile::last_wall_type)
		{
			if (out)
			{
				out->Set(pos.x, pos.y, pos.z);
			}
			
			return true;
		}
		
		pos += dir;
	}
	
	return false;
}

float Terrain::GetHeightBelow(const vector3df& pos) const
{
	// Based on the tile type
	const Tile& tile = GetTile(static_cast<int>(pos.x), static_cast<int>(pos.z));
	vector2df _where(pos.x - floor(pos.x), pos.z - floor(pos.z));
	float height = std::max(GetTileHeight(tile.tile), GetTileEntityHeight(GetTileEntity(static_cast<int>(pos.x), static_cast<int>(pos.z)), _where));

	return height;
}

const Tile& Terrain::GetTile(const vector3df& pos) const
{
	// Get the tile in the current area/level
	return GetTile(static_cast<int>(pos.x), static_cast<int>(pos.z));
}

const Tile& Terrain::GetTile(const vector2di& pos) const
{
	// Get the tile in the current area/level
	return GetTile(pos.x, pos.y);
}

const Tile& Terrain::GetTile(int x, int y) const
{
	// Get the tile in the current area/level
	if (x < 0) x = 0;
	else if (x >= AREA_SIZE) x = AREA_SIZE - 1;

	if (y < 0) y = 0;
	else if (y >= AREA_SIZE) y = AREA_SIZE - 1;

	return (m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).tiles.at(x * AREA_SIZE + y));
}

const TileArray& Terrain::GetTiles() const
{
	// Get the tiles of the current area/level
	return m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).tiles;
}

void Terrain::SetTile(const Tile& tile, int x, int y)
{
	// Set the tile in the current area/level
	m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).tiles.at(x * AREA_SIZE + y) = tile;
}

void Terrain::SetTile(nsTile::Tile tile, int x, int y)
{
	// Set the tile in the current area/level
	m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).tiles.at(x * AREA_SIZE + y).tile = tile;
}

const TileEntity& Terrain::GetTileEntity(const vector3df& pos) const
{
	// Get the tile entity in the current area/level
	return GetTileEntity(static_cast<int>(pos.x), static_cast<int>(pos.z));
}

const TileEntity& Terrain::GetTileEntity(const vector2di& pos) const
{
	// Get the tile entity in the current area/level
	return GetTileEntity(pos.x, pos.y);
}

const TileEntity& Terrain::GetTileEntity(int x, int y) const
{
	// Get the tile entity in the current area/level
	if (x < 0) x = 0;
	else if (x >= AREA_SIZE) x = AREA_SIZE - 1;

	if (y < 0) y = 0;
	else if (y >= AREA_SIZE) y = AREA_SIZE - 1;

	return (m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).entities.at(x * AREA_SIZE + y));
}

TileEntity& Terrain::_GetTileEntity(const vector2di& pos)
{
	// Get the tile entity in the current area/level
	// Private function for this class to modify entities easily
	int x = pos.x;
	int y = pos.y;
	if (x < 0) x = 0;
	else if (x >= AREA_SIZE) x = AREA_SIZE - 1;

	if (y < 0) y = 0;
	else if (y >= AREA_SIZE) y = AREA_SIZE - 1;

	return (m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).entities.at(x * AREA_SIZE + y));
}

void Terrain::SetTileEntity(const TileEntity& entity, int x, int y)
{
	// Set the tile entity in the current area/level
	m_Areas.at(m_iCurrentArea).levels.at(m_iCurrentLevel).entities.at(x * AREA_SIZE + y) = entity;
}

float Terrain::GetTileHeight(const nsTile::Tile tile) const
{
	// All walls have a height of 1.f
	if (tile < nsTile::last_wall_type)
		return 1.f;
	
	// As tiles are hard programmed we need a function to get the height of a tile type
	switch (tile)
	{
	case nsTile::water:
	case nsTile::lava:
	case nsTile::slime:
	case nsTile::mana:
	case nsTile::sludge:
	case nsTile::blood:
		return -0.25f; // -(4.f / 16.f);
	case nsTile::grass:
	//	return 0.06f; // 1.f / 16.f;
	default:
		return 0.f;
	}
}

float Terrain::GetTileEntityHeight(const TileEntity& entity, const vector2df pos) const
{
	// As tile entities are hard programmed we need a function to get the height of an entity type
	switch (entity.type)
	{
	case nsTileEntity::empty:
		return -1.f;
	case nsTileEntity::stone_bridge:
	case nsTileEntity::sign:
		return 0.f;
	case nsTileEntity::stone_door:
	{
		// Based off of door state and position in tile
		if (entity.state == 0)
		{
			// Door is closed
			switch (entity.rot)
			{
			case 0:
				return (pos.x >= 0.9f ? 10.f : 0.f);
			case 4:
				return (pos.y <= 0.1f ? 10.f : 0.f);
			case 8:
				return (pos.x <= 0.1f ? 10.f : 0.f);
			case 12:
				return (pos.y >= 0.9f ? 10.f : 0.f);
			default:
				return 10.f; // A door shouldn't have any other rotation
			}
		}
		else return 0.f; // Door is open
	}
	default:
		return 10.f; // So we can't walk on entities that have no height defined
	}
}

bool Terrain::IsEntityInteractable(const nsTileEntity::TileEntity entity) const
{
	// As tile entities are hard programmed we need a function to determine whether an entity is interactable
	switch (entity)
	{
	case nsTileEntity::sign:
	case nsTileEntity::chest:
	case nsTileEntity::stone_door:
		return true;
	default:
		return false;
	}
}

void Terrain::CleanUp()
{

}

void Terrain::AddMesh(const MeshBase* mesh, const vector3df& offset, float rot)
{
	const Mesh<GLVertex>* orig = dynamic_cast<const Mesh<GLVertex>*>(mesh);
	if (orig)
	{
		// Get the "chunk" this mesh should be added to
		Mesh<GLVertex>* mesh = GetMesh(offset);

		// Indices will need an offset
		GLuint start = mesh->GetVertexData().size();

		// If we need to rotate the mesh, sort out a transformation matrix
		matrix4f m;
		if (rot != 0.f)
		{
			m.SetRotation(vector3df(0.f, rot, 0.f));

			if (m.Inverse())
				m.Transpose();
		}

		// Add the vertices
		const std::vector<GLVertex>& data = orig->GetVertexData();
		for (auto& it : data) {
			GLVertex vert(it);

			// Rotate if we need to
			if (rot != 0.f)
				vert.pos = m.Multiply(vector4df(vert.pos));

			// Now offset the vertex into place
			vert.pos += offset;

			mesh->AddVertex(vert);
		}

		// Now add the indices
		const std::vector<GLuint>& indices = orig->GetIndexData();
		for (auto& it : indices) {
			mesh->AddIndex(start + it);
		}
	}
}

Mesh<GLVertex>* Terrain::GetMesh(const vector3df& pos)
{
	return GetMesh(static_cast<int>(pos.x) / MESH_SIZE, static_cast<int>(pos.z) / MESH_SIZE);
}

Mesh<GLVertex>* Terrain::GetMesh(int x, int y)
{
	return m_EntityMeshes.at(x * MESH_COUNT + y).get();
}
