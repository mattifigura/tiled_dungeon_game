#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "ParticleEmitter.h"
#include <string>

class SceneManager;
class Light;
class Unit;

// Projectile class - moves towards a target and then does something to the target, looking pretty all the while

class Projectile {
public:
	Projectile(SceneManager* sceneman, Unit* owner, const std::string& name, int value, const vector3df& position, const vector3df& target, float speed = 5.f);
	~Projectile();

	void Update();

	bool IsAlive() const { return m_Alive; }
	const std::string& GetName() const { return m_Name; }
	const vector3df& GetPosition() const { return m_Position; }
	const vector3df& GetTarget() { return m_Destination; }
	int GetValue() { return m_Value; }
	Unit* GetOwner() { return m_Owner; }

protected:

private:
	SceneManager*					m_SceneMan;
	Unit*							m_Owner;
	std::string						m_Name;
	int								m_Value;
	ParticleEmitter*				m_Projectile;
	ParticleEmitter*				m_Tail;
	std::vector<ParticleEmitter*>	m_Explosion;
	vector3df						m_Destination;
	vector3df						m_Position;
	float							m_Speed;
	bool							m_Alive;
	Light*							m_Light;
};

#endif // PROJECTILE_H
