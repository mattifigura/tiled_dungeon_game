// Tiled Dungeon Game, created by Matti Figura 2018
#include "GameManager.h"

int main(int argc, char** argv)
{
	// Create a game object and run it!
	GameManager Game;

	// Try and initialise and run
	if (Game.Initialise())
		Game.Run();

	// We have finished execution
	return 0;
}
