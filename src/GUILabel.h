#ifndef GUI_LABEL_H_
#define GUI_LABEL_H_

#include "GUIElement.h"
#include "GUIText.h"
#include "Mesh.h"
#include "Matrix.h"
#include <memory>

// GUILabel class - a custom class with a unique mesh for display a label above everything else - used mainly in the inventory and menus

class GUILabel : public GUIElement {
public:
	GUILabel(GUIManager* guiMan);

	virtual void Update();

	void SetTitle(const std::string& text = "");
	void SetDescription(const std::string& text = "");

	const Mesh<GLVertex2D>* GetMesh() const;
	const GUIText* GetTitle() const;
	const GUIText* GetDescription() const;
	const matrix4f& GetTransformationMatrix() const;

protected:
	virtual void BuildElement();

private:
	std::unique_ptr<GUIText>			m_Title;
	std::unique_ptr<GUIText>			m_Description;
	std::unique_ptr<Mesh<GLVertex2D>>	m_Mesh;
	matrix4f							m_Transformation;

};

#endif // GUI_LABEL_H_
