#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Line.h"

template<typename T>
class Triangle3 {
public:
	Triangle3() : p1(), p2(), p3() {}
	Triangle3(const Vector3<T>& one, const Vector3<T>& two, const Vector3<T>& three) : p1(one), p2(two), p3(three) {}

	bool operator==(const Triangle3<T>& other) const { return (p1 == other.p1 && p2 == other.p2 && p3 == other.p3); }
	bool operator!=(const Triangle3<T>& other) const { return !operator==(other); }

	Vector3<T> Normal() const { return (p2 - p1).CrossProduct(p3 - p1); }

	bool IsPointInside(const Vector3<T>& p) const
	{
		return (SameSide(p, p1, p2, p3) && SameSide(p, p2, p1, p3) && SameSide(p, p3, p1, p2));
	}
	bool IntersectOfPlaneWithLine(const Vector3<T>& p, const Vector3<T>& l, Vector3<T>& out) const
	{
		Vector3<T> normal = Normal().Normal();
		T intersect = normal.Dot(l);

		if (intersect == 0)
			return false;

		T d = p1.Dot(normal);
		T t = -(normal.Dot(p) - d) / intersect;
		out = p + (l * t);
		return true;
	}
	bool IntersectWithLine(const Vector3<T>& p, const Vector3<T>& l, Vector3<T>& out) const
	{
		if (IntersectOfPlaneWithLine(p, l, out))
			return IsPointInside(out);

		return false;
	}

	Vector3<T> p1, p2, p3;
protected:

private:
	bool SameSide(const Vector3<T>& p1, const Vector3<T>& p2, const Vector3<T>& a, const Vector3<T>& b) const
	{
		Vector3<T> bminusa = b - a;
		Vector3<T> cp1 = bminusa.CrossProduct(p1 - a);
		Vector3<T> cp2 = bminusa.CrossProduct(p2 - a);
		T res = cp1.Dot(cp2);
		if (res < 0)
		{
			Vector3<T> cp1 = bminusa.Normal().CrossProduct((p1 - a).Normal());
			if (abs(cp1.x) < 0.00000001 && abs(cp1.y) < 0.00000001 && abs(cp1.z) < 0.00000001)
			{
				res = 0;
			}
		}
		return (res >= 0.0f);
	}
};

typedef Triangle3<int>		triangle3di;
typedef Triangle3<short>	triangle3ds;
typedef Triangle3<float>	triangle3df;
typedef Triangle3<double>	triangle3dd;

#endif