#include "DeltaTime.h"
#include "InputManager.h"
#include "PlayerController.h"
#include "UnitControllerManager.h"
#include "SceneManager.h"
#include "Functions.h"
#include "CameraController.h"
#include <algorithm>

PlayerController::PlayerController(SceneManager* sceneman, UnitControllerManager* man)
	: UnitController(sceneman, man, nsControllerNames::player)
	, m_CurrentCharacter(0)
	, m_CharacterCount(3) // 3 players max, can have anywhere from 1 to 3
	, m_Mode(nsPlayerMode::movement)
	, m_ControlNode(nullptr)
	, m_GridNode(nullptr)
	, m_Animation(0.2f)
	, m_AnimationDirection(1.f)
	, m_MousePosition()
	, m_PreviousAbility(0)
	, m_TileHeight(0.f)
	, m_CollisionPlane(vector3df(0.f, 1.f, 0.f), vector3df(10.f, 0.f, 10.f))
	, m_ValidMovement(false)
	, m_Attacking(false)
{
	// Our units don't die
	SetUnitsDie(false);
	
	// Set our texture
	GetBillboard()->SetTexture("media/characters.png");
	
	// Create our player lights
	m_Lights.emplace(0, sceneman->CreateLight(vector2df(), GLColour(255, 180, 150, 255), vector2df(2.f, 2.f), true));
	m_Lights.emplace(1, sceneman->CreateLight(vector2df(), GLColour(255, 180, 150, 255), vector2df(2.f, 2.f), true));
	m_Lights.emplace(2, sceneman->CreateLight(vector2df(), GLColour(255, 180, 150, 255), vector2df(2.f, 2.f), true));
	
	// Create our control node and create the mesh
	m_ControlNode = &sceneman->CreateSceneNode();
	m_ControlNode->SetTexture("media/misc.png");
	m_ControlNode->AssignNewDynamicMesh();
	m_ControlNode->SetOutline(true);
	Mesh<GLVertex>* mesh = m_ControlNode->GetDynamicMesh();
	
	// Create our movement steps node
	m_GridNode = &sceneman->CreateSceneNode();
	m_GridNode->SetTexture("media/misc.png");
	m_GridNode->AssignNewDynamicMesh();
	m_GridNode->SetOutline(true);
	
	float uv_scale = 8.f / 64.f;
	
	// Add the selected unit quads
	mesh->AddQuad(GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f - uv_scale)));
	
	mesh->AddQuad(GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f)));
	
	mesh->AddQuad(GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f)));
	
	mesh->AddQuad(GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(2.f * uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(100, 160, 255, 255), vector2df(uv_scale, 1.f - uv_scale)));
	
	mesh->AddQuad(GLVertex(vector3df(), GLColour(255, 70), vector2df(0.f, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(255, 70), vector2df(uv_scale, 1.f - uv_scale)),
		GLVertex(vector3df(), GLColour(255, 70), vector2df(uv_scale, 1.f)),
		GLVertex(vector3df(), GLColour(255, 70), vector2df(0.f, 1.f)));
	
	// Add the level walls to our pathfinder (TODO: Move this into a post-generation function as we'll need to do this when moving to a new room)
	const Terrain& terrain = sceneman->GetTerrain();
	for (int x = 0; x < AREA_SIZE; ++x) {
		for (int y = 0; y < AREA_SIZE; ++y) {
			vector3df pos(static_cast<float>(x) + 0.5f, 0.5f, static_cast<float>(y) + 0.5f);
			if (terrain.GetHeightBelow(pos) != 0.f)
			{
				// We can't move here, add a wall to the pathfinder
				GetPathFinder().SetWall(vector2di(x, y), true);
			}
		}
	}
	
	// Set us as the player controller
	SetPlayerController(this);
}

void PlayerController::Update()
{
	// Call individual update methods
	UpdateCamera();
	UpdateIndicators();
	
	switch (m_Mode)
	{
		case nsPlayerMode::movement:
			UpdateMovement();
			break;
		case nsPlayerMode::ability_1:
		case nsPlayerMode::ability_2:
		case nsPlayerMode::ability_3:
		case nsPlayerMode::ability_4:
			UpdateAbility();
			break;
		default:
			break;
	}
	
	// Now update the mesh
	Mesh<GLVertex>* mesh = m_ControlNode->GetDynamicMesh();
	if (mesh->IsDirty())
		mesh->Buffer();
	else
		mesh->Update();
	
	// Call base class update to keep our player characters updated
	UnitController::Update();
	
	// Update our lights
	UpdateLighting();
}

void PlayerController::UpdateBackground()
{
	// Call base class update to keep our player characters updated
	UnitController::UpdateBackground();
	
	// Update our lights
	UpdateLighting();
}

void PlayerController::TurnStart()
{
	// At the beginning of the turn (player's turn) we'll need to reset our unit data
	//m_UnitData.clear();
	if (m_UnitData.empty())
	{
		for (size_t i = 0; i < m_CharacterCount; ++i) {
			if (GetUnit(i)->GetHealth() > 0)
				m_UnitData.push_back(GetUnit(i)->GetUnitData());
		}
	}
	else
	{
		for (size_t i = 0; i < m_CharacterCount; ++i) {
			UnitData& data = m_UnitData.at(i);
			short cooldowns[5] = {0};
			for (size_t j = 0; j < 5; ++j) {
				cooldowns[j] = std::max(data.cooldowns[j] - 1, 0);
			}
			
			data = GetUnit(i)->GetUnitData();
			
			for (size_t j = 0; j < 5; ++j) {
				data.cooldowns[j] = cooldowns[j];
			}
		}
	}
	
	// Add the enemies to the pathfinder
	EnemyController* enemy_controller = GetEnemyController();
	if (enemy_controller)
	{
		enemy_controller->UpdateUnitGridPositions();
		for (auto pos : enemy_controller->GetUnitGridPositions()) {
			GetPathFinder().SetWall(pos, true);
		}
	}
	
	// Make our nodes visible
	if (m_ControlNode)
		m_ControlNode->SetVisible();
	
	if (m_GridNode)
		m_GridNode->SetVisible();
	
	// Reset things
	m_MovementPath.clear();
	m_PreviousGridPosition.Set(0, 0);
	m_AbilityArea.clear();
	m_Mode = nsPlayerMode::movement;
	Singleton<CameraController>::Instance().SetTargetUnit(GetUnit(m_CurrentCharacter));
}

void PlayerController::TurnEnd()
{
	// Make our nodes invisible
	m_ControlNode->SetVisible(false);
	m_GridNode->SetVisible(false);
	
	// Remove the enemies from the pathfinder
	EnemyController* enemy_controller = GetEnemyController();
	if (enemy_controller)
	{
		enemy_controller->UpdateUnitGridPositions();
		for (auto pos : enemy_controller->GetUnitGridPositions()) {
			GetPathFinder().SetWall(pos, false);
		}
	}
}

bool PlayerController::MouseEvent(int button, int action, int mods)
{
	switch (button)
	{
		case GLFW_MOUSE_BUTTON_1:
		{
			// Left click, do action
			if (action == GLFW_PRESS)
			{
				return LeftClick();
			}
		}
		break;
		default:
			break;
	}
	
	return false;
}

bool PlayerController::KeyboardEvent(int key, int scancode, int action, int mods)
{
	// If we're moving, don't do anything
	Unit* character = GetUnit(m_CurrentCharacter);
	if (character->IsMoving() || GetAbilityController().GetFirstProjectile())
		return false;
	
	// Function to clear grid/movement
	auto clear_grid = [&]() {
		m_MovementPath.clear();
		m_GridNode->GetDynamicMesh()->Clear();
		m_GridNode->GetDynamicMesh()->Buffer();
		m_PreviousGridPosition.Set(0, 0);
		m_PreviousCharacterPosition.Set(0, 0);
		
		// Remove the enemies from the pathfinder
		EnemyController* enemy_controller = GetEnemyController();
		if (enemy_controller)
		{
			enemy_controller->UpdateUnitGridPositions();
			for (auto pos : enemy_controller->GetUnitGridPositions()) {
				GetPathFinder().SetWall(pos, m_Mode == nsPlayerMode::movement);
			}
		}
		m_MovementPath.clear();
		m_PreviousGridPosition.Set(0, 0);
	};
	
	// Handle certain keypresses
	switch (key)
	{
		case GLFW_KEY_TAB:
		{
			// Cycle through units
			if (action == GLFW_PRESS)
			{
				ChooseNextCharacter();
				
				return true;
			}
		}
		break;
		case GLFW_KEY_1:
		case GLFW_KEY_2:
		case GLFW_KEY_3:
		case GLFW_KEY_4:
		case GLFW_KEY_5:
		case GLFW_KEY_6:
			if (action == GLFW_PRESS)
			{
				m_Mode = static_cast<nsPlayerMode::PlayerMode>(key - GLFW_KEY_1);
				clear_grid();
			}
			break;
		case GLFW_KEY_ENTER:
			if (action == GLFW_PRESS)
			{
				GetManager()->NextController();
				return true;
			}
			break;
		case GLFW_KEY_SPACE:
			Singleton<CameraController>::Instance().LookAt(character->GetPosition());
			break;
		default:
			break;
	}
	
	return false;
}

std::string PlayerController::GetMouseText()
{
	if (!m_ControlNode->IsVisible())
		return "";
	
	switch (m_Mode)
	{
		case nsPlayerMode::movement:
		{
			int movement = GetMovementLength();
			if (movement > 0)
				return std::to_string(movement);
		}
		break;
		case nsPlayerMode::ability_1:
		case nsPlayerMode::ability_2:
		case nsPlayerMode::ability_3:
		case nsPlayerMode::ability_4:
		{
			EnemyController* controller = GetEnemyController();
			if (controller)
			{
				// We have the enemy unit controller, ensure our mouse placement is within the attack area
				//if (std::find(m_AbilityArea.begin(), m_AbilityArea.end(), m_MouseGridPosition) != m_AbilityArea.end())
				{
					// We have a valid attack position, now see if we have a valid target
					Unit* enemy = controller->GetUnit(m_MouseGridPosition);
					if (enemy)
					{
						std::string ret(enemy->GetName() + " - ");
						ret += std::to_string(enemy->GetHealth()) + "/" + std::to_string(enemy->GetUnitData().health);
						
						return ret;
					}
				}
			}
		}
		break;
		default:
			break;
	}
	
	return "";
}

void PlayerController::EnemyKilled(const vector2di& pos)
{
	// Remove the enemy from our pathfinder
	GetPathFinder().SetWall(pos, false);
	
	// Slightly move the previous character position to update us
	--m_PreviousCharacterPosition.x;
}

bool PlayerController::DamageUnit(Unit* unit, int damage, Unit* other)
{
	bool killed = UnitController::DamageUnit(unit, damage, other);
	
	// Custom code to extend the base class DamageUnit
	// Update the lighting of this unit
	if (killed)
	{
		Light* light = m_Lights.at(unit->GetID());
		if (light)
		{
			light->colour.Set(150, 40, 20, 180);
			light->size.Set(2.3f, 2.3f);
			light->fading = true;
		}
		
		// Get the enemy controller and let them know this unit has died
		EnemyController* enemy_controller = GetEnemyController();
		if (enemy_controller)
		{
			enemy_controller->PlayerKilled(vector2df(unit->GetPosition().x, unit->GetPosition().z));
		}
		
		// Choose another character
		if (m_CurrentCharacter == unit->GetID() && !ChooseNextCharacter())
		{
			// The game is over, all the player characters are dead
			///TODO: What to do in this scenario, game over screen I suppose?
			///      Will be based on difficulty? Restart the level / return to level select if not hardcore?
		}
	}
	
	return killed;
}

bool PlayerController::HealUnit(Unit* unit, int heal, Unit* other)
{
	bool healed = UnitController::HealUnit(unit, heal, other);
	
	// Custom code to extend the base cass HealUnit
	// Update the lighting of this unit
	if (healed)
	{
		Light* light = m_Lights.at(unit->GetID());
		if (light)
		{
			light->colour.Set(255, 180, 150, 255);
			light->size.Set(2.f, 2.f);
			light->fading = false;
			light->alive = true;
		}
	}
	
	return healed;
}

void PlayerController::GetUnitCounts(size_t id, int& act, int& max_act, int& step) const
{
	act = m_UnitData.at(id).action_count;
	max_act = GetUnit(id)->GetUnitData().action_count;
	step = m_UnitData.at(id).movement;
}

void PlayerController::GetUnitHealth(size_t id, int& health, int& max_health) const
{
	max_health = m_UnitData.at(id).health;
	health = GetUnit(id)->GetHealth();
}

void PlayerController::UpdateCamera()
{
	// Tell the camera controller what to do
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	Unit* character = GetUnit(m_CurrentCharacter);
	bool animating = camera_controller.IsAnimating();
	
	if (character->IsMoving())
		camera_controller.SetTargetUnit(character);
	
	m_ControlNode->SetVisible(!animating);
	m_GridNode->SetVisible(!animating);
	
	if (!animating && m_Attacking)
	{
		camera_controller.SetTargetUnit(character);
		m_Attacking = false;
	}
}

void PlayerController::UpdateIndicators()
{
	// Update the scene node mesh
	Mesh<GLVertex>* mesh = m_ControlNode->GetDynamicMesh();
	std::vector<GLVertex>& vertices = mesh->GetVertexData();
	const vector3df& unit_pos = GetUnit(m_CurrentCharacter)->GetPosition();
	double delta = Singleton<DeltaTime>::Instance().GetDelta();
	
	// Animate the unit indicator
	m_Animation += m_AnimationDirection * 0.3f * delta;
	if (m_Animation >= 0.38f)
		m_AnimationDirection = -1.f;
	else if (m_Animation <= 0.26f)
		m_AnimationDirection = 1.f;
	
	// Selected unit indicator
	vertices[0].pos.Set(unit_pos.x - 0.2f, 0.05f, unit_pos.z - 0.2f + m_Animation);
	vertices[1].pos.Set(unit_pos.x + 0.2f, 0.05f, unit_pos.z - 0.2f + m_Animation);
	vertices[2].pos.Set(unit_pos.x + 0.2f, 0.05f, unit_pos.z + 0.2f + m_Animation);
	vertices[3].pos.Set(unit_pos.x - 0.2f, 0.05f, unit_pos.z + 0.2f + m_Animation);
	
	vertices[4].pos.Set(unit_pos.x - 0.2f, 0.05f, unit_pos.z - 0.2f - m_Animation);
	vertices[5].pos.Set(unit_pos.x + 0.2f, 0.05f, unit_pos.z - 0.2f - m_Animation);
	vertices[6].pos.Set(unit_pos.x + 0.2f, 0.05f, unit_pos.z + 0.2f - m_Animation);
	vertices[7].pos.Set(unit_pos.x - 0.2f, 0.05f, unit_pos.z + 0.2f - m_Animation);
	
	vertices[8].pos.Set(unit_pos.x - 0.2f + m_Animation, 0.05f, unit_pos.z - 0.2f);
	vertices[9].pos.Set(unit_pos.x + 0.2f + m_Animation, 0.05f, unit_pos.z - 0.2f);
	vertices[10].pos.Set(unit_pos.x + 0.2f + m_Animation, 0.05f, unit_pos.z + 0.2f);
	vertices[11].pos.Set(unit_pos.x - 0.2f + m_Animation, 0.05f, unit_pos.z + 0.2f);
	
	vertices[12].pos.Set(unit_pos.x - 0.2f - m_Animation, 0.05f, unit_pos.z - 0.2f);
	vertices[13].pos.Set(unit_pos.x + 0.2f - m_Animation, 0.05f, unit_pos.z - 0.2f);
	vertices[14].pos.Set(unit_pos.x + 0.2f - m_Animation, 0.05f, unit_pos.z + 0.2f);
	vertices[15].pos.Set(unit_pos.x - 0.2f - m_Animation, 0.05f, unit_pos.z + 0.2f);
	
	// Get the grid position under the mouse
	if (!Singleton<CameraController>::Instance().IsAnimating())
	{
		line3df ray;
		GetSceneManager()->GetRayFromScreenCoords(ray);
		m_CollisionPlane.IntersectWithLine(ray.start, ray.end, m_MousePosition);
		m_MouseGridPosition.Set(m_MousePosition.x, m_MousePosition.z);
	}
	
	// Get the tile under the mouse
	//const Tile& tile = GetSceneManager()->GetTerrain().GetTile(m_MouseGridPosition);
	m_TileHeight = GetSceneManager()->GetTerrain().GetHeightBelow(m_MousePosition) != 0.f ? 9001.f : 0.01f;
	vector2df selected_pos(static_cast<float>(m_MouseGridPosition.x), static_cast<float>(m_MouseGridPosition.y));
	
	vertices[16].pos.Set(selected_pos.x + 0.15f, m_TileHeight, selected_pos.y + 0.15f);
	vertices[17].pos.Set(selected_pos.x + 0.85f, m_TileHeight, selected_pos.y + 0.15f);
	vertices[18].pos.Set(selected_pos.x + 0.85f, m_TileHeight, selected_pos.y + 0.85f);
	vertices[19].pos.Set(selected_pos.x + 0.15f, m_TileHeight, selected_pos.y + 0.85f);
	
	GLColour col(255, m_ValidMovement ? 255 : 80, m_ValidMovement ? 255 : 80, 70);
	vertices[16].col = col; vertices[17].col = col; vertices[18].col = col; vertices[19].col = col;
}

void PlayerController::UpdateMovement()
{
	// Get the grid position under the mouse
	Mesh<GLVertex>* mesh = m_GridNode->GetDynamicMesh();
	UnitData& data = m_UnitData.at(m_CurrentCharacter);
	
	if (m_TileHeight < 0.1f && data.movement > 0)
	{
		// We have a valid tile selected
		Unit* character = GetUnit(m_CurrentCharacter);
		vector2di character_position(character->GetPosition().x, character->GetPosition().z);
		
		// If any positions have changed, update our path scene node mesh
		if (m_PreviousGridPosition != m_MouseGridPosition || m_PreviousCharacterPosition != character_position || character->IsMoving())
		{
			mesh->Clear();
			
			// Only create the steps if our unit isn't moving and we're withing a reasonable range
			if (!character->IsMoving() && Distance(m_MouseGridPosition, character_position) <= data.movement)
			{
				// Calculate the path from our unit to our goal
				m_MovementPath.clear();
				EnemyController* enemy_controller = GetEnemyController();
				Unit* unit_at_cursor = GetUnit(m_MouseGridPosition);
				
				if ((!unit_at_cursor || unit_at_cursor->GetHealth() <= 0) && (enemy_controller && !enemy_controller->GetUnit(m_MouseGridPosition)) &&
					GetPathFinder().FindPath(character_position, m_MouseGridPosition, m_MovementPath))
				{
					// Check movement against our character's movement stats
					int movement = data.movement + 1;
					m_ValidMovement = (static_cast<int>(m_MovementPath.size()) <= movement ? true : false);
					
					if (m_ValidMovement)
					{
						int count = 0;
						for (auto& pos : m_MovementPath) {
							vector3df place(static_cast<float>(pos.x) + 0.5f, 0.009f, static_cast<float>(pos.y) + 0.5f);
							GLColour col(255, count < movement ? 255 : 80, count < movement ? 255 : 80, 70);
							
							mesh->AddQuad(GLVertex(place + vector3df(-0.1f, 0.f, -0.1f), col, vector2df(0.f, 1.f)),
								GLVertex(place + vector3df(0.1f, 0.f, -0.1f), col, vector2df(0.f, 1.f)),
								GLVertex(place + vector3df(0.1f, 0.f, 0.1f), col, vector2df(0.f, 1.f)),
								GLVertex(place + vector3df(-0.1f, 0.f, 0.1f), col, vector2df(0.f, 1.f)));
							
							++count;
						}
					}
					else
					{
						m_MovementPath.clear();
					}
				}
				else
				{
					m_ValidMovement = false;
				}
				
				m_PreviousCharacterPosition = character_position;
			}
			else
			{
				m_MovementPath.clear();
				m_ValidMovement = false;
				m_PreviousCharacterPosition.Set(0, 0);
			}
		}
		
		// Update previous positions
		m_PreviousGridPosition = m_MouseGridPosition;
	}
	else
	{
		mesh->Clear();
		m_PreviousGridPosition.Set(0, 0);
		m_ValidMovement = false;
		m_MovementPath.clear();
	}
	
	// Update the mesh
	if (mesh->IsDirty())
		mesh->Buffer();
	else
		mesh->Update();
}

void PlayerController::UpdateAbility()
{
	// Get the grid position under the mouse
	Mesh<GLVertex>* mesh = m_GridNode->GetDynamicMesh();
	m_ValidMovement = true;
	
	// We have a valid tile selected
	Unit* character = GetUnit(m_CurrentCharacter);
	vector2di character_position(character->GetPosition().x, character->GetPosition().z);
	int ability_index = GetAbilityIndex();
	GLColour col(255, 80, 80, 70);
	const Terrain& terrain = GetSceneManager()->GetTerrain();
	
	// Set up a grid around the character
	if ((m_PreviousCharacterPosition != character_position || m_PreviousAbility != ability_index || m_PreviousGridPosition != m_MouseGridPosition) && !character->IsMoving())
	{
		mesh->Clear();
		
		// Get an area of all positions usable by the current ability
		m_AbilityArea.clear();
		const Ability& ability = AbilityController::GetAbility(m_UnitData.at(m_CurrentCharacter).abilities[ability_index]);
		
		// If this is a friendly ability, change the colour
		if (ability.status_effect >= nsStatusEffect::heal)
			col.Set(80, 255, 80, 70);
		
		// Self abilities don't need this area
		if (ability.type != nsAbilityType::self && ability.type != nsAbilityType::self_aoe)
			GetPathFinder().GetFillArea(character_position, ability.range, m_AbilityArea);
		else if (ability.type == nsAbilityType::self_aoe)
			GetPathFinder().GetFillArea(character_position, ability.area, m_AbilityArea);
		
		if (!m_AbilityArea.empty())
		{
			std::vector<vector2di>::iterator pos = m_AbilityArea.begin();
			while (pos != m_AbilityArea.end())
			{
				vector3df place(static_cast<float>((*pos).x) + 0.5f, 0.009f, static_cast<float>((*pos).y) + 0.5f);
				
				if (terrain.DoesLineCollideWithTerrain(line3df(place, character->GetPosition())))
				{
					pos = m_AbilityArea.erase(pos);
					continue;
				}
				
				mesh->AddQuad(GLVertex(place + vector3df(-0.5f, 0.f, -0.5f), col, vector2df(0.f, 1.f)),
					GLVertex(place + vector3df(0.5f, 0.f, -0.5f), col, vector2df(0.f, 1.f)),
					GLVertex(place + vector3df(0.5f, 0.f, 0.5f), col, vector2df(0.f, 1.f)),
					GLVertex(place + vector3df(-0.5f, 0.f, 0.5f), col, vector2df(0.f, 1.f)));
				
				++pos;
			}
		}
		
		// Now add an AOE indicator
		if (ability.type != nsAbilityType::self_aoe && ability.area > 0 &&
			std::find(m_AbilityArea.begin(), m_AbilityArea.end(), m_MouseGridPosition) != m_AbilityArea.end())
		{
			std::vector<vector2di> aoe_area;
			GetPathFinder().GetFillArea(m_MouseGridPosition, ability.area, aoe_area);
			col.Set(60, 150, 170, 50);
			
			if (!aoe_area.empty())
			{
				std::vector<vector2di>::iterator pos = aoe_area.begin();
				while (pos != aoe_area.end())
				{
					vector3df place(static_cast<float>((*pos).x) + 0.5f, 0.05f, static_cast<float>((*pos).y) + 0.5f);
					
					mesh->AddQuad(GLVertex(place + vector3df(-0.5f, 0.f, -0.5f), col, vector2df(0.f, 1.f)),
						GLVertex(place + vector3df(0.5f, 0.f, -0.5f), col, vector2df(0.f, 1.f)),
						GLVertex(place + vector3df(0.5f, 0.f, 0.5f), col, vector2df(0.f, 1.f)),
						GLVertex(place + vector3df(-0.5f, 0.f, 0.5f), col, vector2df(0.f, 1.f)));
					
					++pos;
				}
			}
		}
		
		m_PreviousCharacterPosition = character_position;
		m_PreviousAbility = ability_index;
		m_PreviousGridPosition = m_MouseGridPosition;
	}
	
	// Update the mesh
	if (mesh->IsDirty())
		mesh->Buffer();
	else
		mesh->Update();
}

void PlayerController::UpdateLighting()
{
	for (auto& light : m_Lights) {
		Unit* unit = Unit::GetUnitByID(light.first);
		if (unit && light.second)
		{
			light.second->position.Set(unit->GetPosition().x, unit->GetPosition().z);
		}
	}
}

bool PlayerController::ChooseNextCharacter()
{
	size_t next_character = m_CurrentCharacter + 1;
	if (next_character >= 3)
		next_character = 0;
	
	// Loop through our characters getting one which is not dead
	bool chosen = false;
	while (!chosen)
	{
		Unit* character = GetUnit(next_character);
		if (character->GetHealth() > 0)
		{
			chosen = true;
			m_CurrentCharacter = next_character;
		}
		else
		{
			if (++next_character == m_CurrentCharacter)
				break;
			
			if (next_character >= 3)
				next_character = 0;
		}
	}
	
	return chosen;
}

bool PlayerController::LeftClick()
{
	CameraController& camera_controller = Singleton<CameraController>::Instance();
	if (camera_controller.IsAnimating())
		return false;
	
	Unit* character = GetUnit(m_CurrentCharacter);
	UnitData& data = m_UnitData.at(m_CurrentCharacter);
	
	switch (m_Mode)
	{		
		case nsPlayerMode::movement:
		{
			// Move our players
			if (m_ValidMovement)
			{
				for (auto pos : m_MovementPath) {
					character->AddMovementPoint(vector3df(static_cast<float>(pos.x) + 0.5f, 0.f, static_cast<float>(pos.y) + 0.5f));
				}
				
				data.movement -= (m_MovementPath.size() - 1);
			}
			
			return true;
		}
		break;
		case nsPlayerMode::ability_1:
		case nsPlayerMode::ability_2:
		case nsPlayerMode::ability_3:
		case nsPlayerMode::ability_4:
		{
			// Use our current ability
			int index = GetAbilityIndex();
			Ability& ability = AbilityController::GetAbility(data.abilities[index]);
			if (data.cooldowns[index] == 0 && ability.type != nsAbilityType::invalid)
			{
				// Ability is valid, ensure our mouse placement is within the ability area
				if (ability.type == nsAbilityType::self || ability.type == nsAbilityType::self_aoe ||
					std::find(m_AbilityArea.begin(), m_AbilityArea.end(), m_MouseGridPosition) != m_AbilityArea.end())
				{
					// We have a valid position, check we have enough action points
					if (data.action_count >= ability.cost)
					{
						// Use the ability
						vector2di pos(m_MouseGridPosition);
						
						if (ability.type == nsAbilityType::self || ability.type == nsAbilityType::self_aoe)
							pos.Set(character->GetPosition().x, character->GetPosition().z);
						
						if (GetAbilityController().UseAbility(data.abilities[index], character, pos, data.ability_values[index]))
						{
							data.action_count -= ability.cost;
							data.cooldowns[index] = ability.cooldown;
							m_Attacking = true;
							camera_controller.SetTargetUnit(character);
							return true;
						}
					}
				}
			}
		}
		break;
		default:
			break;
	}
	
	return false;
}
