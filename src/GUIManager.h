#ifndef GUI_MANAGER_H_
#define GUI_MANAGER_H_

#include "ResourceManager.h"
#include "GUIElement.h"
#include "GLShaderProgram.h"
#include "GLTexture.h"
#include "Font.h"
#include "Mesh.h"
#include "InputManager.h"
#include <list>
#include <map>

// All GUIElements
#include "GUIPane.h"
#include "GUIText.h"
#include "GUIImage.h"
#include "GUILabel.h"
#include "GUITextBox.h"
#include "GUIButton.h"
#include "GUICharacterPane.h"
#include "GUIAction.h"

// Shader attribute location definitions
//#define SHADER_GUI_POSITION_ATTRIBUTE 0
//#define SHADER_GUI_COLOUR_ATTRIBUTE 1
//#define SHADER_GUI_TEXCOORD_ATTRIBUTE 2

// Shader uniform location definitions
//#define SHADER_GUI_MODEL_UNIFORM 0
//#define SHADER_GUI_PROJECTION_UNIFORM 1
//#define SHADER_GUI_TEXIMAGE_UNIFORM 2
//#define SHADER_GUI_SIZE_UNIFORM 3
//#define SHADER_GUI_ALPHA_UNIFORM 4

// GUIManager class - handles all GUIElements - drawing, creation, updating etc

typedef std::list<std::unique_ptr<GUIElement>> GUIList;
typedef std::list<std::unique_ptr<GUIText>> GUITextList;

struct GUIMesh {
	std::unique_ptr<Mesh<GLVertex2D>> mesh;
	bool needs_building;

	GUIMesh() : mesh(new Mesh<GLVertex2D>), needs_building(true) {}
};

typedef std::map<GLuint, GUIMesh> MeshMap;

class GUIManager : public InputObject {
public:
	GUIManager();

	void Initialise();
	void Update();
	void Render();

	void UseTexture(GLuint texture);

	const GLTexture& GetTexture(const std::string& filename);
	const Font& GetFont(const std::string& key);

	GLuint AddQuad(const GUIElement* who, const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3, const GLVertex2D& ver4, bool flip = false);
	void ChangeQuadColours(const GUIElement* who, GLuint pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4);
	void ChangeQuadPosition(const GUIElement* who, GLuint pos, const vector2df& position);
	void ChangeQuadPositions(const GUIElement* who, GLuint pos, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4);
	void MoveElement(const GUIElement* element, const vector2df& move);

	void RebuildMesh(const GUIElement* who);

	void SetLabel(const std::string& title = "", const std::string& description = "");

	virtual bool MouseEvent(int button, int action, int mods);
	virtual bool CharEvent(unsigned int codepoint);

	// GUI creation methods
	GUIPane* CreatePane(const vector2df& pos, const vector2df& size, GUIElement* parent = NULL);
	GUIText* CreateText(const vector2df& pos, const std::string& text, const std::string& font, GUIElement* parent = NULL);
	GUIImage* CreateImage(const std::string& texture, const vector2df& position, const vector2df& size, GUIElement* parent = NULL);
	GUITextBox* CreateTextBox(const vector2df& pos, const vector2df& size, const std::string& font, GUIElement* parent = NULL);
	GUIButton* CreateButton(const vector2df& pos, const vector2df& size, const std::string& text, const std::string& font, GUIElement* parent = NULL);
	GUICharacterPane* CreateCharacterPane(const vector2df& pos, const std::string& text, GUIElement* parent = NULL);
	GUIAction* CreateAction(const vector2df& pos, GUIElement* parent = NULL);

protected:

private:
	GUIList								m_GUIElements;
	GUITextList							m_GUITextElements;
	ResourceManager<GLTexture>			m_TexMan;
	GLuint								m_CurrentActiveTexture;
	std::unique_ptr<GLShaderProgram>	m_Shader;
	ResourceManager<Font>				m_FontMan;
	MeshMap								m_Meshes;
	GLuint								m_GUITexture;
	std::unique_ptr<GUILabel>			m_GUILabel;
	bool								m_bLabel;

	// Shader attrib locations
	int                                 m_iPositionAttrib;
	int                                 m_iColourAttrib;
	int                                 m_iTextureAttrib;

	// Shader uniform locations
	int                                 m_iModelUniform;
	int                                 m_iProjectionUniform;
	int                                 m_iTexImageUniform;

	void Sort();
	void Reverse();

	void EnableVertexAttributes();
	void DisableVertexAttributes();
	void StartRender();
	void FinishRender();
	void RenderMesh(const MeshBase* mesh);

	void AddElement(GUIElement* element);

	GUIMesh& GetMesh(GLuint tex);
};

#endif // GUI_MANAGER_H_
