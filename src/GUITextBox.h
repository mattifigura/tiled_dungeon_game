#ifndef GUI_TEXTBOX_H_
#define GUI_TEXTBOX_H_

#include "GUIText.h"

// GUITextBox class - an editable text box class to allow for user input strings

class GUITextBox : public GUIElement {
public:
	GUITextBox(GUIManager* guiMan, const std::string& font, const vector2df& size, GUIElement* parent = NULL);
	~GUITextBox();

	virtual void Update();

	void SetCharacterLimit(size_t limit);
	void SetNumericOnly(bool numeric);
	void SetHint(const std::string& text);

	void SetFocus(bool focus = true);
	bool IsFocused() const;

	void CharInput(unsigned int character);
	void EraseCharacter();
	void ResetEraseDelay();

	const std::string& GetText() const;

protected:
	virtual void BuildElement();

private:
	GUIText*	m_pText;
	GUIText*	m_pHint;
	bool		m_bNumbersOnly;
	size_t		m_CharLimit;
	bool		m_bFocused;
	size_t		m_CursorPosition;
	float		m_fEraseDelay;
};

#endif // GUI_TEXTBOX_H_
