#include "Mesh.h"

template<typename T>
Mesh<T>::Mesh()
	: GLObject<T>()
	, m_Vertices()
	, m_Indices()
	, m_Size(0u)
	, m_bDirty(true)
{
	// The constructor does nothing outside of initialisation
}

template<typename T>
Mesh<T>::~Mesh()
{

}

template<typename T>
GLuint Mesh<T>::GetVBO() const
{
	return GLObject<T>::GetVBO();
}

template<typename T>
GLuint Mesh<T>::GetEBO() const
{
	return GLObject<T>::GetEBO();
}

template<typename T>
bool Mesh<T>::Buffer(bool bKeepData, size_t size)
{
	// Check if our current buffer is big enough
	if (VALID_BUFFER(GetVBO()) && m_Indices.size() <= m_Size && GLObject<T>::UpdateVBO(m_Vertices, size))
	{
		// Update our size
		m_Size = m_Indices.size();
		m_bDirty = false;

		// If we aren't keeping the data clear it
		if (!bKeepData)
		{
			m_Vertices.clear();
			m_Indices.clear();
			m_Vertices.shrink_to_fit();
			m_Indices.shrink_to_fit();
		}

		// We have been successful
		return true;
	}
	
	// Buffer our data
	if (!m_Vertices.empty() && !m_Indices.empty() && GLObject<T>::BufferData(m_Vertices, m_Indices))
	{
		// Update our size
		m_Size = m_Indices.size();
		m_bDirty = false;
		
		// If we aren't keeping the data clear it
		if (!bKeepData)
		{
			m_Vertices.clear();
			m_Indices.clear();
			m_Vertices.shrink_to_fit();
			m_Indices.shrink_to_fit();
		}

		// We have been successful
		return true;
	}

	// We have failed
	return false;
}

template<typename T>
bool Mesh<T>::Update()
{
	// Update our VBO
	if (!m_Vertices.empty() && GLObject<T>::UpdateVBO(m_Vertices))
	{
		// We are no longer dirty
		m_bDirty = false;

		// Update our size
		m_Size = m_Indices.size();

		// We have been successful
		return true;
	}

	// We have failed
	return false;
}

template<typename T>
size_t Mesh<T>::GetIndexBufferSize() const
{
	return m_Size;
}

template<typename T>
void Mesh<T>::SetDirty()
{
	m_bDirty = true;
}

template<typename T>
bool Mesh<T>::IsDirty() const
{
	return m_bDirty;
}

template<typename T>
void Mesh<T>::Clear()
{
	// Clear our data
	m_Vertices.clear();
	m_Indices.clear();

	// We are now dirty again
	SetDirty();
}

template<typename T>
void Mesh<T>::AddTriangle(const T& ver1, const T& ver2, const T& ver3)
{
	// Add a triangle to our mesh
	GLuint iIndex = static_cast<GLuint>(m_Vertices.size());

	// Push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3);

	// Add to the indices
	m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);

	// Set us as dirty
	SetDirty();
}

template<typename T>
GLuint Mesh<T>::AddQuad(const T& ver1, const T& ver2, const T& ver3, const T& ver4, bool flip)
{
	// Add a quad to our mesh
	GLuint iIndex = static_cast<GLuint>(m_Vertices.size());

	// Push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3); m_Vertices.push_back(ver4);

	// Add to the indices
	if (flip)
	{
		m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3);
		m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1);
	}
	else
	{
		m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
		m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0);
	}

	// Set us as dirty
	SetDirty();

	// Return the number of this quad
	return (iIndex / 4);
}

template<typename T>
void Mesh<T>::AddVertex(const T& ver)
{
	// Push back a new vertex
	m_Vertices.push_back(ver);

	// Set us as dirty
	SetDirty();
}

template<typename T>
void Mesh<T>::AddIndex(GLuint index)
{
	// Push back a new index
	m_Indices.push_back(index);

	// Set us as dirty
	SetDirty();
}

template<typename T>
void Mesh<T>::CopyVertices(const std::vector<T>& data)
{
	// Copy the data into our data
	m_Vertices = data;
}

template<typename T>
void Mesh<T>::CopyIndices(const std::vector<GLuint>& data)
{
	// Copy the data into our data
	m_Indices = data;
}

template<typename T>
std::vector<T>& Mesh<T>::GetVertexData()
{
	return m_Vertices;
}

template<typename T>
const std::vector<T>& Mesh<T>::GetVertexData() const
{
	return m_Vertices;
}

template<typename T>
const std::vector<GLuint>& Mesh<T>::GetIndexData() const
{
	return m_Indices;
}