#ifndef UNIT_H_
#define UNIT_H_

#include "Billboard.h"
#include "StatusEffect.h"
#include "ControllerNames.h"
#include <queue>

namespace nsUnitClass
{
	enum UnitClass {
		melee = 0,
		mage,
		tank,
		rogue,
		archer,
		summoner
	};
}

struct UnitData {
	nsUnitClass::UnitClass unit_class = {nsUnitClass::melee};
	int health = {1};
	char movement = {5}; // movement of -1 means infinite movement
	char action_count = {1};
	std::string abilities[4] = {"None", "None", "None", "None"};
	int ability_values[4] = {0, 0, 0, 0};
	short cooldowns[5] = {0, 0, 0, 0, 2};
	int animation = {0};
};

class UnitController;

// Unit class - base class for each unit in the game, controlled by a UnitController

class Unit {
friend class UnitController;

public:
	Unit(nsControllerNames::Controller controller);
	virtual ~Unit();
	
	virtual void CreateUnit();
	
	virtual void Update();

	void SetPosition(const vector3df& pos) { m_Position = pos; m_Destination = pos; }
	const vector3df& GetPosition() const { return m_Position; }
	
	void SetAnimation(int anim) { m_Board->animation = anim; }
	
	void AddMovementPoint(const vector3df& point);
	
	bool IsMoving() const { return m_Moving || !m_MovementQueue.empty(); }

	const UnitData& GetUnitData() const { return m_Data; }

	bool Damage(int amount, Unit* other = nullptr);
	bool Heal(int amount, Unit* other = nullptr);
	int GetHealth() const { return m_Health; }

	const std::string& GetName() const { return m_Name; }
	
	void AttackAnimate(const vector3df& pos);

	const size_t GetID() const { return m_ID; }
	nsControllerNames::Controller GetControllerName() const { return m_Owner; }
	
	static void ResetGlobalUnits() { m_GlobalUnits.clear(); m_GlobalID = 0; }
	static Unit* GetUnitByID(size_t id) { return m_GlobalUnits.at(id); }

protected:
	Board* GetBoard() { return m_Board; }
	UnitData& GetData() { return m_Data; }

private:
	const size_t					m_ID;
	vector3df						m_Position;
	vector3df						m_Destination;
	Board*							m_Board;
	UnitData						m_Data;
	std::queue<vector3df>			m_MovementQueue;
	bool							m_Moving;
	std::string						m_Name;
	int								m_Health;
	float							m_AttackAnimation;
	float							m_AttackOffset;
	float							m_AttackDirection;
	float							m_HitAnimation;
	nsControllerNames::Controller	m_Owner;
	
	static size_t					m_GlobalID;
	static std::vector<Unit*>		m_GlobalUnits;
	
	void UnitCreated() { m_GlobalUnits.push_back(this); }
	void UnitDestroyed() { m_GlobalUnits.at(m_ID) = nullptr; }
};

#endif // UNIT_H_
