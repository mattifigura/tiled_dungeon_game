#ifndef MESH_H_
#define MESH_H_

#include "GLObject.h"

// Mesh class - this is used by SceneNodes and is basically a store for the mesh to be displayed

class MeshBase : public GLObjectBase {
public:
	virtual GLuint GetVBO() const = 0;
	virtual GLuint GetEBO() const = 0;
	
	virtual bool Buffer(bool bKeepData = true, size_t size = 0) = 0;
	virtual bool Update() = 0;
	virtual size_t GetIndexBufferSize() const = 0;

	virtual void SetDirty() = 0;
	virtual bool IsDirty() const = 0;

	virtual void Clear() = 0;

	virtual ~MeshBase() {}

protected:

private:

};

template<typename T>
class Mesh : public GLObject<T>, public MeshBase {
public:
	Mesh();
	~Mesh();

	virtual GLuint GetVBO() const;
	virtual GLuint GetEBO() const;

	bool Buffer(bool bKeepData = true, size_t size = 0);
	bool Update();
	size_t GetIndexBufferSize() const;

	void SetDirty();
	bool IsDirty() const;

	void Clear();

	// Functions to assist with manual creation of meshes
	void AddTriangle(const T& ver1, const T& ver2, const T& ver3);
	GLuint AddQuad(const T& ver1, const T& ver2, const T& ver3, const T& ver4, bool flip = false);
	void AddVertex(const T& ver);
	void AddIndex(GLuint index);
	void CopyVertices(const std::vector<T>& data);
	void CopyIndices(const std::vector<GLuint>& data);

	std::vector<T>& GetVertexData();
	const std::vector<T>& GetVertexData() const;
	const std::vector<GLuint>& GetIndexData() const;

protected:

private:
	std::vector<T>			m_Vertices;
	std::vector<GLuint>		m_Indices;
	size_t					m_Size;
	bool					m_bDirty;
};

#endif