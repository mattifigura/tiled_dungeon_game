#include "Font.h"
#include <iostream>
#include <string>
#include <algorithm>
#ifdef __linux__
#include <ft2build.h>
#else
#include "ft2build.h"
#endif // __linux__
#include FT_GLYPH_H
#include FT_FREETYPE_H

Font::Font(const std::string& key)
	: m_Texture(nullptr)
	, m_iHeight(0)
	, m_iFontSize(0)
{
	// Firstly extract information required to read this font
	std::string filename;
	ExtractKey(key, filename, m_iHeight);

	// Print to the console
	std::cout << "Loading font " << filename << ", size " << m_iHeight << std::endl;

	// Create the freetype library object
	FT_Library ft;
	if (FT_Init_FreeType(&ft))
	{
		std::cout << "ERROR: Cannot initialise FreeType!" << std::endl;
		return;
	}

	// Load the font face
	FT_Face face;
	if (FT_New_Face(ft, filename.c_str(), 0, &face))
	{
		std::cout << "ERROR: Cannot open font " << filename << std::endl;
		FT_Done_FreeType(ft);
		return;
	}

	// Set the size
	FT_Set_Pixel_Sizes(face, 0, static_cast<unsigned int>(m_iHeight));

	// We need to read each character now to get the max height of a character
	FT_GlyphSlot g = face->glyph;
	m_iFontSize = m_iHeight;
	for (int u = -128; u < 128; ++u) {
		// Get the char version
		char c = static_cast<char>(u);

		// Load the information about this glyph
		/*FT_Glyph glyph;
		if (FT_Load_Glyph(face, c, FT_LOAD_DEFAULT))
		{
			continue;
		}*/
		FT_Load_Char(face, c, FT_LOAD_RENDER);
		
		// Figure out vertical size of this glyph
		//FT_BBox bbox;
		//FT_Glyph_Get_CBox(glyph, 0, &bbox);
		
		// Adjust the size if necessary
		//m_iHeight = std::max(m_iHeight, static_cast<int>(bbox.yMax - bbox.yMin) / 64);
		//printf("Character: %c, height: %i\n", c, static_cast<int>(bbox.yMax - bbox.yMin) / 64);
		//m_iHeight = std::max(m_iHeight, std::max(g->bitmap_top, static_cast<int>(g->bitmap.rows)) + 1);
		m_iHeight = std::max(m_iHeight, m_iFontSize + (static_cast<int>(g->bitmap.rows) - static_cast<int>(g->bitmap_top)));
	}
	
	//std::cout << m_iHeight << std::endl;

	// Calculate our image size
	int imageSize = 256;
	int needed = m_iHeight * m_iHeight * 256;
	while (needed >= imageSize * imageSize)
	{
		imageSize <<= 1;
	}

	// Our image buffer
	GLubyte* buffer = new GLubyte[imageSize * imageSize * 4];
	memset(buffer, 0, imageSize * imageSize * 4);

	// Function to set the pixel in our buffer
	auto set_pixel = [&buffer, &imageSize](int x, int y, GLubyte val) {
		buffer[(y * imageSize + x) * 4] = 255;
		buffer[(y * imageSize + x) * 4 + 1] = 255;
		buffer[(y * imageSize + x) * 4 + 2] = 255;
		buffer[(y * imageSize + x) * 4 + 3] = val;
	};

	// Now read in each glyph
	int x = 0, y = 0;
	
	//if (face->face_flags & FT_FACE_FLAG_VERTICAL)
	//	printf("Descender: %i\n", face->descender);
	
	for (int u = -128; u < 128; ++u) {
		// Get the char version
		char c = static_cast<char>(u);

		// Load the information about this glyph
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "Loading font " << filename << ": cannot load character " << c << std::endl;
			continue;
		}

		// Create this glyph in our map and texture
		FontGlyph glyph;
		glyph.advance = static_cast<float>(ceil(g->advance.x * 0.015625f));
		glyph.width = glyph.advance;

		// Figure out the position of this glyph
		if (x + glyph.width >= imageSize)
		{
			x = 0;
			y += m_iHeight;
		}
		glyph.pos.Set(x, y);

		// Copy the glyph data into our image
		int startx = std::max(x + g->bitmap_left, 0);
		int starty = std::max(y + m_iFontSize - g->bitmap_top, 0);
		for (int i = 0; static_cast<unsigned int>(i) < g->bitmap.width; ++i) {
			for (int j = 0; static_cast<unsigned int>(j) < g->bitmap.rows; ++j) {
				set_pixel(startx + i, starty + j, g->bitmap.buffer[i + j * g->bitmap.width]);
			}
		}
		x += m_iHeight;

		/*if (c >= ' ')
		{
			printf("Character: %c, top: %i, rows: %i", c, g->bitmap_top, g->bitmap.rows);
			printf(", bearingY: %i, height: %i\n", static_cast<int>(g->metrics.horiBearingY), static_cast<int>(g->metrics.height));
		}*/

		// Put this glyph in the map
		m_Font.emplace(c, glyph);
	}

	// We are now done with freetype
	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	// Create our texture
	GLTexture* tex = new GLTexture;
	tex->CreateTextureFromBuffer(buffer, imageSize, imageSize, GL_RGBA);
	m_Texture.reset(tex);

	// Delete temporary buffers
	delete[] buffer;
}

const FontGlyph& Font::GetCharacterInfo(char c) const
{
	// Find the character in the map
	FontMap::const_iterator it = m_Font.find(c);
	if (it != m_Font.end())
		return it->second;

	// We have been unable to find it, return an empty glyph
	static FontGlyph empty;
	return empty;
}

const GLTexture* Font::GetTexture() const
{
	return m_Texture.get();
}

int Font::GetHeight() const
{
	return m_iHeight;
}

int Font::GetStringWidth(const std::string& in) const
{
	// Get the width of the given string in our font
	int width = 0;

	int i = -1;
	while (true)
	{
		const char c = in[++i];

		if (c == '\0')
		{
			break;
		}

		width += m_Font.at(c).advance;
	}

	return width;
}

void Font::ExtractKey(const std::string& key, std::string& filename, int& size)
{
	// Find the colon in the key
	size_t _size = key.find(':');

	// Get the filename and size
	filename = key.substr(0, _size);
	size = atoi(key.substr(_size + 1, key.length() - size - 1).c_str());

	// If we weren't provided with a size use a default of 16
	if (size == 0)
		size = 16;
}
