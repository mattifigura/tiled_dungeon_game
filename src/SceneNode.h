#ifndef SCENE_NODE_H_
#define SCENE_NODE_H_

#include "Matrix.h"
#include "GLBase.h"
#include <memory>

class MeshBase;
class SceneManager;
class GLTexture;
template <typename T> class Mesh;

// SceneNode base class - this should be the base of every object needing a physical position in the world

class SceneNode {
public:
	SceneNode(SceneManager* sceneMan);
	virtual ~SceneNode();

	virtual void Update();

	void SetMesh(const MeshBase* mesh);
	void SetMesh(const std::string& key);
	const MeshBase* GetMesh() const;

	void SetTexture(const GLTexture* tex);
	void SetTexture(const std::string& filename);
	const GLTexture* GetTexture() const;

	void SetPosition(const vector3df& position);
	const vector3df& GetPosition() const;

	void SetScale(const vector3df& scale);
	const vector3df& GetScale() const;

	void SetRotation(const vector3df& rotation);
	const vector3df& GetRotation() const;
	void SetRotationOffset(const vector3df& offset);
	const vector3df& GetRotationOffset() const;

	const matrix4f& GetTransformationMatrix() const;
	const matrix4f& GetNormalMatrix() const;

	void SetQuaternion(const quaternion& quat);

	void Kill();
	bool IsDead() const;

	void SetVisible(bool visible = true);
	bool IsVisible() const;

	void SetRenderMode(int mode);
	int GetRenderMode() const;

	void SetClearsDepthBuffer(bool clears = true);
	bool ClearsDepthBuffer() const;

	void SetCullFrontFace(bool cull = false);
	bool CullFrontFace() const;

	void SetOutline(bool outline = false);
	bool IsOutline() const;

	void AssignNewDynamicMesh();

	SceneManager* GetSceneManager() { return m_pSceneMan; }
	Mesh<GLVertex>* GetDynamicMesh();

protected:
	const SceneManager* GetSceneManager() const { return m_pSceneMan; }

private:
	const MeshBase*					m_Mesh;
	const GLTexture*				m_Texture;
	SceneManager*					m_pSceneMan;
	matrix4f						m_Transformation;
	matrix4f						m_NormalMatrix;
	vector3df						m_Position;
	vector3df						m_Scale;
	vector3df						m_Rotation;
	vector3df						m_RotationOffset;
	quaternion						m_Quaternion;
	bool							m_bDead;
	bool							m_bVisible;
	int								m_iRenderMode;
	bool							m_bClearDepthBuffer;
	bool							m_bCullFrontFace;
	bool							m_bOutline;
	std::unique_ptr<Mesh<GLVertex>>	m_DynamicMesh;
};

#endif // SCENE_NODE_H_
