#ifndef GUI_PANE_H_
#define GUI_PANE_H_

#include "GUIElement.h"


// GUIPane class - a basic pane for building other elements onto

class GUIPane : public GUIElement {
public:
	GUIPane(GUIManager* guiMan, const vector2df& size, GUIElement* parent = NULL);

	virtual void SetSize(const vector2df& size);

protected:
	virtual void BuildElement();

private:

};

#endif // GUI_PANE_H_
