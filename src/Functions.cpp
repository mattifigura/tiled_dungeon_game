#include "Functions.h"

namespace util
{
	void VariableToBuffer(void* var, size_t size, std::vector<unsigned char>& out)
	{
		// Convert the given variable to an unsigned char buffer
		for (size_t i = 0; i < size; ++i) {
			out.push_back(reinterpret_cast<unsigned char*>(var)[i]);
		}
	}

	std::streampos FileSize(std::fstream& file)
	{
		// get the size of this file using tellg
		file.seekg(0, std::ios::end);
		std::streampos pos = file.tellg();
		file.seekg(0, std::ios::beg);

		// return the stream position
		return pos;
	}

	float RandomBounds(float lower, float upper, float precision)
	{
		float diff = upper - lower;
		int random = rand() % static_cast<int>(std::max(diff * precision, 1.f));
		return (lower + (static_cast<float>(random) / std::max(precision, 1.f)));
	}

	bool file_to_string(const std::string& filename, std::string& out)
	{
		// Read the given file into the string out
		std::ifstream file(filename, std::ios::in);
		if (file.is_open())
		{
			// Read lines and append to out
			std::string line;
			while (getline(file, line))
			{
				out.append(line).append("\n");
			}
			
			// Close the file and return true
			file.close();
			return true;
		}
		
		// Couldn't read the file, return false
		return false;
	}
}