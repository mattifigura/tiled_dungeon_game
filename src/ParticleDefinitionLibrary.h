#ifndef PARTICLE_DEFINITION_LIBRARY_H
#define PARTICLE_DEFINITION_LIBRARY_H

#include "ParticleEmitter.h"
#include <map>
#include <vector>
#include <string>

struct ParticleDefinition {
	size_t count = {10};
	bool lights = {false};
	bool burst = {false};
	bool affected_by_lights = {false};
	EmitterBehaviour behaviour;
};

// ParticleDefinitionLibrary class - just a static class to get particle definitions for creating particle emitters

class ParticleDefinitionLibrary {
public:
	static bool LoadDefinitions(const std::string& filename);
	static std::vector<ParticleDefinition>& GetDefinitions(const std::string& name);

protected:

private:
	typedef std::map<std::string, std::vector<ParticleDefinition>> DefinitionMap;
	static DefinitionMap	m_Definitions;
};

#endif // PARTICLE_DEFINITION_LIBRARY_H
