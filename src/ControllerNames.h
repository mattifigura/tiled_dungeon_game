#ifndef CONTROLLER_NAMES_H
#define CONTROLLER_NAMES_H

// ControllerNames - provides an enum for player, enemy and dungeon controllers

namespace nsControllerNames
{
	enum Controller {
		player = 0,
		dungeon,
		enemy
	};
}

#endif // CONTROLLER_NAMES_H