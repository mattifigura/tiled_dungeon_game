#include "Camera.h"

Camera::Camera(const matrix4f& proj)
	: m_ProjMatrix(proj)
	, m_fDirection(0)
	, m_fDirCos(0)
	, m_fDirSin(0)
{

}

Camera::~Camera()
{
	// This does nothing
}

void Camera::Update()
{
	// update the camera
	m_Matrix.Identity();

	vector3df zaxis = (m_Position - m_Target);
	zaxis.Normal();

	vector3df xaxis(0, 1, 0);
	xaxis.Cross(zaxis);
	xaxis.Normal();

	vector3df yaxis = zaxis;
	yaxis.Cross(xaxis);

	m_Matrix.Set(0, xaxis.x);
	m_Matrix.Set(1, xaxis.y);
	m_Matrix.Set(2, xaxis.z);
	m_Matrix.Set(3, -xaxis.Dot(m_Position));
	m_Matrix.Set(4, yaxis.x);
	m_Matrix.Set(5, yaxis.y);
	m_Matrix.Set(6, yaxis.z);
	m_Matrix.Set(7, -yaxis.Dot(m_Position));
	m_Matrix.Set(8, zaxis.x);
	m_Matrix.Set(9, zaxis.y);
	m_Matrix.Set(10, zaxis.z);
	m_Matrix.Set(11, -zaxis.Dot(m_Position));

	// rotate the camera by our given rotation vector
	matrix4f mat;
	mat.SetRotation(m_Rotation);
	m_Matrix = mat.Multiply(m_Matrix);

	// Calculate our view frustum
	matrix4f m;
	m.MultiplicationByProduct(m_ProjMatrix, m_Matrix);
	m_Frustum.Set(m);

	// Calculate the camera's direction (used for billboards)
	m_fDirection = atan2(m_Target.z - m_Position.z, m_Target.x - m_Position.x);

	// Update the sin and cos of the camera rotation
	m_fDirCos = cos(-m_fDirection);
	m_fDirSin = sin(-m_fDirection);
}
