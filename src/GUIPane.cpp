#include "GUIPane.h"
#include "InputManager.h"

GUIPane::GUIPane(GUIManager* guiMan, const vector2df& size, GUIElement* parent)
	: GUIElement(guiMan, parent)
{
	// Set our type
	SetType(GUIType::gui_pane);

	// Set us up
	SetSize(size);
}

void GUIPane::SetSize(const vector2df& size)
{
	// Overriden so that we can set the bounding box size
	GUIElement::SetSize(size);
	SetBoundingBox(vector2df(), size);
}

void GUIPane::BuildElement()
{
	// Construct a basic pane
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), GLColour(255, 70)),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(255, 70)),
		GLVertex2D(vector2df(size.x, size.y), GLColour(255, 70)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(255, 70)));

	AddQuad(GLVertex2D(vector2df(2.f, 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(size.x - 2.f, 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(size.x - 2.f, size.y - 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(2.f, size.y - 2.f), GLColour(0, 180)));
}
