#include "StatusEffectImplementations.h"
#include "SceneManager.h"
#include "Unit.h"

// Shield

StatusEffect_Shield::StatusEffect_Shield(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier)
	: StatusEffect(sceneman, owner, target, name, life, rounds, modifier, nsStatusEffect::shield)
{
}

bool StatusEffect_Shield::OnDamage(int& value, Unit* other)
{
	if (IsDead())
		return false;
	
	// Stop us from taking any damage
	value = 0;
	return true;
}

// Damage Transfer

StatusEffect_DamageTransfer::StatusEffect_DamageTransfer(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier)
	: StatusEffect(sceneman, owner, target, name, life, rounds, modifier, nsStatusEffect::damage_transfer)
{
}

bool StatusEffect_DamageTransfer::OnDamage(int& value, Unit* other)
{
	if (IsDead())
		return false;
	
	Unit* owner = GetOwner();
	Unit* target = GetTarget();
	if (!owner || owner->GetHealth() == 0)
		return false;
	
	// The owner will take the damage instead, based on the modifier
	int diff = std::max(value * static_cast<int>(GetModifier()) / 100, 1);
	value -= diff;
	
	// Create a particle effect to show that it has worked
	const std::vector<ParticleDefinition>& defs = ParticleDefinitionLibrary::GetDefinitions(GetName());
	if (!defs.empty())
	{
		ParticleEmitter* emitter = GetSceneManager()->CreateParticleEmitter(target->GetPosition() + vector3df(0.f, 0.25f, 0.f), defs.at(0));
		
		vector3df offset(owner->GetPosition() - target->GetPosition());
		EmitterBehaviour& behaviour = emitter->GetBehaviour();
		behaviour.only_live_once = true;
		behaviour.gravity = offset.Normal() * 5.f;
		behaviour.duration_randomiser = 0.f;
		behaviour.force.Set(0.f, 0.f, 0.f);
		behaviour.position_randomiser_lower.Set(-0.25f, -0.25f, -0.25f);
		behaviour.position_randomiser_upper.Set(0.25f, 0.25f, 0.25f);
		behaviour.end_colour.a = 0;
		
		behaviour.duration = (offset.Magnitude() * offset.Magnitude()) * 1000.f;
		
		emitter->SpawnAll();
	}
	
	owner->Damage(diff, other);
	
	return true;
}