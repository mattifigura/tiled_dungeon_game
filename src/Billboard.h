#ifndef BILLBOARD_H_
#define BILLBOARD_H_

#include "SceneNode.h"
#include <vector>

struct Board {
	vector3df position;
	vector2df size;
	float frame;
	float animation_speed;
	int animation;
	
	size_t id;
	
	Board()
		: position()
		, size()
		, frame(0.f)
		, animation_speed(1.5f)
		, animation(-1)
	{
	}
};

struct BoardAnimation {
	std::vector<vector2df> frames;
	vector2df frame_size;
};

// Billboard class - custom SceneNode which is a 2D sprite always pointing towards the camera - supports animations

class Billboard : public SceneNode {
public:
	Billboard(SceneManager* sceneMan);
	
	Board* AddBoard();
	Board* GetBoard(int id) { return m_Boards.at(id).get(); }
	int AddAnimation(const vector2di& start, const vector2di& end, const vector2df& size);
	
	void SetHideOnAnimationFinish(bool hide) { m_HideOnAnimationFinish = hide; }
	
	virtual void Update();

	const BoardAnimation& GetAnimation(size_t id) const { return m_Animations.at(id); }

protected:

private:
	std::vector<std::shared_ptr<Board>>	m_Boards;
	std::vector<BoardAnimation>			m_Animations;
	bool								m_HideOnAnimationFinish;
};

#endif // BILLBOARD_H_
