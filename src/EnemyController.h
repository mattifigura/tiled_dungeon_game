#ifndef ENEMY_CONTROLLER_H_
#define ENEMY_CONTROLLER_H_

#include "UnitController.h"

namespace nsEnemyMode
{
	enum EnemyMode {
		movement,
		attack,
		ability,
		camera
	};
}

class Light;
class PlayerController;

// EnemyController class - handles enemy units

class EnemyController : public UnitController {
public:
	EnemyController(SceneManager* sceneman, UnitControllerManager* man);

	virtual void Update();
	virtual void UpdateBackground();
	virtual void TurnStart();
	virtual void TurnEnd();

	void PlayerKilled(const vector2df& pos);

protected:

private:
	int							m_CurrentEnemy;
	nsEnemyMode::EnemyMode		m_Mode;
	nsEnemyMode::EnemyMode		m_TransitionMode;
	std::vector<UnitData>		m_UnitData;
	bool						m_Moving;
	std::vector<vector2di>		m_TempPath;
	Unit*						m_Target;
	PlayerController*			m_PlayerController;
	Light*						m_EnemyLight;
	bool						m_Active;
	bool						m_JustAttacked;
	bool						m_Delayed;
	std::vector<int>			m_UnitsToProcess;
	
	void UpdateCamera();
	void UpdateUnits();
	
	bool UpdateMovement();
	bool UpdateAttack();
	bool ChooseUnit();
	void UpdateLight();
	void UpdateTarget(bool& valid, std::vector<Unit*>& previous);
	bool CanAttackTarget(const vector3df& pos, const Ability& ability);
};

#endif // ENEMY_CONTROLLER_H_
