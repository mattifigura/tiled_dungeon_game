#include "Projectile.h"
#include "SceneManager.h"
#include "DeltaTime.h"

Projectile::Projectile(SceneManager* sceneman, Unit* owner, const std::string& name, int value, const vector3df& position, const vector3df& target, float speed)
	: m_SceneMan(sceneman)
	, m_Owner(owner)
	, m_Name(name)
	, m_Value(value)
	, m_Projectile(nullptr)
	, m_Tail(nullptr)
	, m_Destination(target)
	, m_Position(position)
	, m_Speed(speed)
	, m_Alive(true)
	, m_Light(nullptr)
{
	// Create our particle effects
	const std::vector<ParticleDefinition>& defs = ParticleDefinitionLibrary::GetDefinitions(m_Name);
	if (defs.size() > 0)
	{
		// We have at least our projectile
		m_Projectile = m_SceneMan->CreateParticleEmitter(m_Position, defs.at(0));
		m_Projectile->SetPosition(m_Position);
		
		// Create a light if we need to
		if (defs.at(0).lights)
			m_Light = m_SceneMan->CreateLight(vector2df(m_Position.x, m_Position.z), defs.at(0).behaviour.start_colour, vector2df(2.f, 2.f));
		
		if (defs.size() > 1)
		{
			// We also have a tail, so create that
			m_Tail = m_SceneMan->CreateParticleEmitter(m_Position, defs.at(1));
			m_Tail->SetPosition(m_Position);
		}
	}
}

Projectile::~Projectile()
{
	// Kill our projectile and tail
	if (m_Projectile)
		m_Projectile->Stop();
	
	if (m_Tail)
		m_Tail->Stop();
	
	// Kill our light
	if (m_Light)
		m_Light->fading = true;
	
	// Create our explosion particle effects
	const std::vector<ParticleDefinition>& defs = ParticleDefinitionLibrary::GetDefinitions(m_Name);
	if (defs.size() > 2)
	{
		for (size_t i = 2; i < defs.size(); ++i) {
			m_SceneMan->CreateParticleEmitter(m_Position, defs.at(i));
		}
	}
}

void Projectile::Update()
{
	// This should never be hit, but just in case
	if (!m_Alive)
		return;
	
	// If we are at our destination, it's time to die
	float delta = Singleton<DeltaTime>::Instance().GetDelta();
	if (m_Position.QuickDistance(m_Destination) <= m_Speed * delta)
	{
		m_Alive = false;
		return;
	}
	
	// Move towards the target
	vector3df step = (m_Destination - m_Position).Normal() * m_Speed * delta;
	
	m_Position += step;
	
	// Move our light
	if (m_Light)
		m_Light->position.Set(m_Position.x, m_Position.z);
	
	// Translate our main projectile
	if (m_Projectile)
		m_Projectile->Translate(step);
	
	// Move our tail
	if (m_Tail)
		m_Tail->SetPosition(m_Position);
}
