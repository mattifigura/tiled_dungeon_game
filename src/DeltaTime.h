#ifndef DELTA_TIME_H_
#define DELTA_TIME_H_

#include <chrono>
#include "Singleton.h"

class DeltaTime {
public:
	DeltaTime()
		: m_Time(std::chrono::system_clock::now())
		, m_Delta(0.f)
	{

	}

	void CalculateDelta()
	{
		std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
		m_Delta = time - m_Time;
		m_Time = time;
	}

	float GetDelta()
	{
		return m_Delta.count();
	}

protected:

private:
	std::chrono::time_point<std::chrono::system_clock>	m_Time;
	std::chrono::duration<float>						m_Delta;
};

#endif