#include "GUICharacterPane.h"
#include "GUIManager.h"

GUICharacterPane::GUICharacterPane(GUIManager* guiMan, const std::string& text, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_Name(NULL)
	, m_Health(NULL)
	, m_Actions(NULL)
	, m_ActionCount(0)
	, m_MaxActionCount(0)
	, m_StepCount(0)
	, m_DefenseCount(0)
	, m_HealthCount(0)
	, m_MaxHealthCount(0)
	, m_Active(false)
{
	// Set our type
	vector2df size(380.f, 48.f);
	SetType(GUIType::gui_character_pane);
	SetSize(size);

	// Create our label
	GUIManager* manager = GetGUIManager();
	m_Name = manager->CreateText(vector2df(134.f, 8.f), text, "media/NotoSans-Bold.ttf:24", this);
	m_Name->SetSize(size);
	m_Name->SetShadowed();
	SetBoundingBox(vector2df(), size);
	
	// Create the indicators image
	//manager->CreateImage("media/indicators.png", vector2df(134.f, 102.f), vector2df(256.f, 32.f), this);
	
	// Create our indicator text
	/*m_Attacks = manager->CreateText(vector2df(172.f, 104.f), "2", "media/NotoSans-Bold.ttf:22", this);
	m_Attacks->SetShadowed();
	
	m_Steps = manager->CreateText(vector2df(252.f, 104.f), "5", "media/NotoSans-Bold.ttf:22", this);
	m_Steps->SetShadowed();
	
	m_Defense = manager->CreateText(vector2df(332.f, 104.f), "0", "media/NotoSans-Bold.ttf:22", this);
	m_Defense->SetShadowed();*/
	
	// HP Text
	m_Health = manager->CreateText(vector2df(136.f, 62.f), "3/3", "media/NotoSans-Bold.ttf:22", this);
	m_Health->SetSize(vector2df(220.f, 40.f));
	m_Health->SetAlignment(centre);
	m_Health->SetShadowed();
	
	// Action Text
	m_Actions = manager->CreateText(vector2df(136.f, 104.f), "2/2", "media/NotoSans-Bold.ttf:18", this);
	m_Actions->SetSize(vector2df(220.f, 40.f));
	m_Actions->SetAlignment(centre);
	m_Actions->SetShadowed();
}

void GUICharacterPane::SetActive(bool active)
{
	if (m_Active != active)
	{
		GLColour col1(active ? 255 : 0, 60);
		GLColour col2(active ? 255 : 0, 0);
		
		ChangeQuadColours(0, col1, col1, col1, col1);
		ChangeQuadColours(1, col1, col2, col2, col1);
		ChangeQuadColours(2, col1, col1, col1, col1);
		m_Active = active;
	}
}

void GUICharacterPane::SetActions(int action, int max_action)
{
	if (action != m_ActionCount || max_action != m_MaxActionCount)
	{
		// Update the health bar
		float size = (static_cast<float>(action) / static_cast<float>(max_action)) * 212.f;
		
		m_ActionCount = action;
		m_MaxActionCount = max_action;
		
		ChangeQuadPositions(6, vector2df(140.f, 104.f), vector2df(140.f + size, 104.f), vector2df(140.f + size, 128.f), vector2df(140.f, 128.f));
		
		// Update the text
		m_Actions->SetText(std::to_string(action) + "/" + std::to_string(max_action) + " AP");
	}
}

void GUICharacterPane::SetHealth(int health, int max_health)
{
	if (health != m_HealthCount || max_health != m_MaxHealthCount)
	{
		// Update the health bar
		float size = (static_cast<float>(health) / static_cast<float>(max_health)) * 212.f;
		
		m_HealthCount = health;
		m_MaxHealthCount = max_health;
		
		ChangeQuadPositions(4, vector2df(140.f, 60.f), vector2df(140.f + size, 60.f), vector2df(140.f + size, 92.f), vector2df(140.f, 92.f));
		
		// Update the text
		m_Health->SetText(std::to_string(health) + "/" + std::to_string(max_health) + " HP");
	}
}

void GUICharacterPane::BuildElement()
{
	// Add a pane
	GLColour col1(m_Active ? 255 : 0, 60);
	GLColour col2(m_Active ? 255 : 0, 0);
	
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), col1),
		GLVertex2D(vector2df(size.x * 0.6f, 0.f), col1),
		GLVertex2D(vector2df(size.x * 0.6f, size.y), col1),
		GLVertex2D(vector2df(0.f, size.y), col1));
	AddQuad(GLVertex2D(vector2df(size.x * 0.6f, 0.f), col1),
		GLVertex2D(vector2df(size.x, 0.f), col2),
		GLVertex2D(vector2df(size.x, size.y), col2),
		GLVertex2D(vector2df(size.x * 0.6f, size.y), col1));
	AddQuad(GLVertex2D(vector2df(0.f, size.y), col1),
		GLVertex2D(vector2df(128.f, size.y), col1),
		GLVertex2D(vector2df(128.f, size.y + 88.f), col1),
		GLVertex2D(vector2df(0.f, size.y + 88.f), col1));
		
	// Health bar
	float bar_size = (static_cast<float>(m_HealthCount) / static_cast<float>(m_MaxHealthCount)) * 212.f;
	AddQuad(GLVertex2D(vector2df(136.f, 56.f), GLColour(0, 60)),
		GLVertex2D(vector2df(356.f, 56.f), GLColour(0, 60)),
		GLVertex2D(vector2df(356.f, 96.f), GLColour(0, 60)),
		GLVertex2D(vector2df(136.f, 96.f), GLColour(0, 60)));
	AddQuad(GLVertex2D(vector2df(140.f, 60.f), GLColour(255, 80, 60, 90)),
		GLVertex2D(vector2df(140.f + bar_size, 60.f), GLColour(255, 80, 60, 90)),
		GLVertex2D(vector2df(140.f + bar_size, 92.f), GLColour(255, 80, 60, 120)),
		GLVertex2D(vector2df(140.f, 92.f), GLColour(255, 80, 60, 120)));
	
	// Action bar
	bar_size = (static_cast<float>(m_ActionCount) / static_cast<float>(m_MaxActionCount)) * 212.f;
	AddQuad(GLVertex2D(vector2df(136.f, 100.f), GLColour(0, 60)),
		GLVertex2D(vector2df(356.f, 100.f), GLColour(0, 60)),
		GLVertex2D(vector2df(356.f, 132.f), GLColour(0, 60)),
		GLVertex2D(vector2df(136.f, 132.f), GLColour(0, 60)));
	AddQuad(GLVertex2D(vector2df(140.f, 104.f), GLColour(240, 160, 120, 90)),
		GLVertex2D(vector2df(140.f + bar_size, 104.f), GLColour(240, 160, 120, 90)),
		GLVertex2D(vector2df(140.f + bar_size, 128.f), GLColour(240, 160, 120, 120)),
		GLVertex2D(vector2df(140.f, 128.f), GLColour(240, 160, 120, 120)));
}
