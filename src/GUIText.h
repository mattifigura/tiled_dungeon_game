#ifndef GUI_TEXT_H_
#define GUI_TEXT_H_

#include "GUIElement.h"
#include "Mesh.h"
#include "Matrix.h"
#include <memory>

class Font;

enum TextAlignment {
	left,
	centre,
	right
};

// GUIText class - this is the class for rendering fonts

class GUIText : public GUIElement {
public:
	GUIText(GUIManager* guiMan, const std::string& text, GUIElement* parent = NULL);

	virtual void Update();

	void SetFont(const Font* font);

	void SetText(const std::string& text);
	void AppendText(const std::string& text, int pos = -1);
	const std::string& GetText() const;
	const std::string GetReadableText() const;

	size_t GetTextLength() const;
	float GetTextWidth() const;
	float GetTextHeight() const;

	void SetLineSpacing(float spacing);
	float GetLineSpacing() const;

	void SetAlignment(TextAlignment align);
	void SplitByWord(bool split = true);
	void SetShadowed(bool shadow = true);

	const Mesh<GLVertex2D>* GetMesh() const;
	const matrix4f& GetTransformationMatrix() const;

protected:
	virtual void BuildElement();

private:
	const Font*							m_Font;
	std::string							m_strText;
	bool								m_bSplitByWord;
	TextAlignment						m_TextAlignment;
	float								m_fLineSpacing;
	size_t								m_LineHeight;
	std::unique_ptr<Mesh<GLVertex2D>>	m_Mesh;
	matrix4f							m_Transformation;
	bool								m_bShadowed;

	vector2df							m_ShadowOffset;
	unsigned char						m_ucShadowAlpha;

	// Helper functions for text construction
	bool GetColour(GLColour& col, char c) const;
	void GetLineLengths(std::vector<int>& out) const;
};

#endif // GUI_TEXT_H_
