#include "CameraController.h"
#include "Unit.h"
#include "Projectile.h"
#include "Functions.h"
#include "Terrain.h"

CameraController::CameraController()
	: m_Camera(nullptr)
	, m_TargetUnit(nullptr)
	, m_TargetProjectile(nullptr)
	, m_TargetPosition(1.f, 16.f)
	, m_CurrentPosition(1.f, 16.f)
	, m_Animation(0.f)
	, m_CameraRotation(PI_OVER_4)
	, m_CurrentController(nsControllerNames::player)
	, m_ProjectileDied(false)
{
}

CameraController::~CameraController()
{
}

void CameraController::Update()
{
	// Do nothing if we don't have a camera to control
	if (!m_Camera)
		return;

	// Choose our destination based on whether we are following a projectile or a unit
	vector2df target(m_TargetPosition);
	bool can_animate(true);
	if (m_TargetProjectile)
	{
		target.x = m_TargetProjectile->GetPosition().x;
		target.y = m_TargetProjectile->GetPosition().z;
		m_Animation = 2.f;
		
		if (!m_TargetProjectile->IsAlive())
		{
			m_TargetProjectile = nullptr;
			m_ProjectileDied = true;
			m_TargetPosition = target;
		}
	}
	else if (m_TargetUnit && !m_ProjectileDied)
	{
		target.x = m_TargetUnit->GetPosition().x;
		target.y = m_TargetUnit->GetPosition().z;
		
		if (m_TargetUnit->GetHealth() <= 0 || m_Animation <= 0.f)
			m_TargetUnit = nullptr;
	}
	
	// Move us towards our target
	///TODO: Allow the controller smoothing settings to be controlled in settings?
	const float smooth = m_TargetProjectile ? 0.2f : 0.1f;
	const float diff = 0.02f;
	const float max_change = 0.5f;
	util::Interpolate(m_CurrentPosition.x, target.x, smooth, diff, max_change);
	util::Interpolate(m_CurrentPosition.y, target.y, smooth, diff, max_change);
	
	can_animate = m_CurrentPosition.Distance(target) <= 0.1f;
	
	// Return the animation to 0. This variable stops us from snapping away from our target too quickly
	double delta = Singleton<DeltaTime>::Instance().GetDelta();
	if (can_animate)
	{
		if (m_Animation > 0.f)
		{
			m_Animation -= 2.5f * delta;
		}
		else
		{
			m_ProjectileDied = false;
		}
	}
	
	// Allow us to move the camera around (if the player controller is in control)
	if (m_Animation <= 0.f && m_CurrentController == nsControllerNames::player)
	{
		const vector2di& mouse = GET_CURSOR_POSITION;
		const vector2di& screen_size = GET_SCREEN_SIZE;
		
		float rotation = 0.f;
		int size = 0;
		int edge_size = 80;
		
		if (mouse.y < edge_size)
		{
			rotation += 0.f;
			++size;
		}
		if (mouse.y > screen_size.y - edge_size)
		{
			rotation += 180.f;
			
			if (mouse.x > screen_size.x - edge_size)
				rotation += 360.f;
			
			++size;
		}
		if (mouse.x < edge_size)
		{
			rotation += 90.f;
			++size;
		}
		if (mouse.x > screen_size.x - edge_size)
		{
			rotation -= 90.f;
			++size;
		}

		// Update our position
		float move_x = 0.f;
		float move_y = 0.f;

		if (size > 0)
		{
			rotation = (rotation / size) * DEG_TO_RAD;
			float angle = -m_Camera->GetDirection() + rotation;

			move_x = 4.f * (float)cos(-angle) * delta;
			move_y = 4.f * (float)sin(-angle) * delta;
			
			// Keep us within bounds
			if (m_CurrentPosition.x + move_x < 1.f || m_CurrentPosition.x + move_x > AREA_SIZE - 1.f)
				move_x = 0.f;
			if (m_CurrentPosition.y + move_y < 1.f || m_CurrentPosition.y + move_y > AREA_SIZE - 1.f)
				move_y = 0.f;
		}
		
		m_CurrentPosition.x += move_x;
		m_CurrentPosition.y += move_y;
		m_TargetPosition = m_CurrentPosition;
	}
	
	// Finally update the camera
	vector3df camera_position(m_CurrentPosition.x, 0.5f, m_CurrentPosition.y);
	vector3df offset(-1.8f * sin(m_CameraRotation + 0.001f), 3.2f, -1.8f * cos(m_CameraRotation + 0.001f));
	vector3df billboard_offset(-2.8f * sin(m_CameraRotation + 0.001f), 3.2f, -2.8f * cos(m_CameraRotation + 0.001f));
	
	m_Camera->SetPosition(camera_position + offset);
	m_Camera->SetBillboardPosition(camera_position + billboard_offset);
	m_Camera->LookAt(camera_position);
}

void CameraController::SetTargetUnit(const Unit* target, float animation_delay)
{
	m_TargetUnit = target;
	m_Animation = animation_delay;
	
	if (!m_TargetProjectile && target)
	{
		m_TargetPosition.x = target->GetPosition().x;
		m_TargetPosition.y = target->GetPosition().z;
	}
}

void CameraController::SetTargetProjectile(const Projectile* projectile)
{
	m_TargetProjectile = projectile;
	m_Animation = 1.75f;
	
	if (projectile)
	{
		m_TargetPosition.x = projectile->GetPosition().x;
		m_TargetPosition.y = projectile->GetPosition().z;
	}
}

void CameraController::LookAt(const vector3df& pos)
{
	m_TargetPosition.x = pos.x;
	m_TargetPosition.y = pos.z;
	
	if (!IsAnimating())
		m_CurrentPosition = m_TargetPosition;
}

bool CameraController::ScrollEvent(double x, double y)
{
	// If we scrolled a sideways scroll, do not continue
	if (abs(y) < 0.01)
		return false;
	
	// Rotate the camera
	m_CameraRotation -= (y / abs(y)) * (PI / 16.f);
	
	// Keep the rotation within limits
	if (m_CameraRotation < 0.f)
		m_CameraRotation += TWO_PI;
	
	if (m_CameraRotation > TWO_PI)
		m_CameraRotation -= TWO_PI;
	
	return true;
}