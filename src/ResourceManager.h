#ifndef RESOURCE_MANAGER_H_
#define RESOURCE_MANAGER_H_

#include <map>
#include <memory>
#include <string>

// ResourceManager class - a template class that can be used to create objects to manager textures, meshes, sounds etc

template<typename T>
class ResourceManager {
public:
	ResourceManager(T*(*func)(const std::string& key));
	~ResourceManager();

	void Clear();

	const T& GetResource(const std::string& key);

protected:

private:
	// StringCompare struct - used to quickly compare strings for quicker access in the map
	struct StringCompare {
		bool operator()(const std::string& a, const std::string& b) const
		{
			const size_t al = a.length();
			const size_t bl = b.length();
			if (al != bl)
				return al < bl;

			return a < b;
		}
	};

	std::map<std::string, std::unique_ptr<T>, StringCompare> m_Resources;
	T*(*m_CreateFunction)(const std::string& key);
};

#endif
