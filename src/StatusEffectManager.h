#ifndef STATUS_EFFECT_MANAGER_H
#define STATUS_EFFECT_MANAGER_H

#include "StatusEffect.h"
#include "ControllerNames.h"
#include "Unit.h"
#include "Singleton.h"
#include <map>
#include <vector>
#include <functional>
#include <memory>

struct EffectDetails {
	StatusEffect* effect = {nullptr};
	bool owner = {false};
	bool self_owned = {false};
};

typedef std::vector<EffectDetails> StatusEffectVector;
typedef std::map<size_t, StatusEffectVector> StatusEffectMap;

// StatusEffectManager class - handles status effects on units and ensuring they are created/destroyed correctly

class StatusEffectManager
{
public:
	StatusEffectManager();
	~StatusEffectManager();

	void Kill();

	void AddStatusEffect(Unit* target, Unit* owner, StatusEffect* effect) { AddStatusEffect(target->GetID(), owner->GetID(), effect); }
	void AddStatusEffect(size_t target, size_t owner, StatusEffect* effect);
	
	void UnitKilled(Unit* unit) { UnitKilled(unit->GetID()); }
	void UnitKilled(size_t id);

	bool StatusEffectAlreadyApplied(Unit* id, nsStatusEffect::EffectType type, bool check_for_owned = false) { return StatusEffectAlreadyApplied(id->GetID(), type, check_for_owned); }
	bool StatusEffectAlreadyApplied(size_t id, nsStatusEffect::EffectType type, bool check_for_owned = false);

	void UpdateEffects();
	void TurnStart(nsControllerNames::Controller controller);

	bool OnDamage(Unit* unit, int& value, Unit* other = 0) { return OnDamage(unit->GetID(), value, other); }
	bool OnHeal(Unit* unit, int& value, Unit* other = 0) { return OnHeal(unit->GetID(), value, other); }
	bool OnAbilityUse(Unit* unit, int& value, Unit* other = 0) { return OnAbilityUse(unit->GetID(), value, other); }
	bool OnMovement(Unit* unit, int& value) { return OnMovement(unit->GetID(), value); }

	bool OnDamage(size_t id, int& value, Unit* other = 0);
	bool OnHeal(size_t id, int& value, Unit* other = 0);
	bool OnAbilityUse(size_t id, int& value, Unit* other = 0);
	bool OnMovement(size_t id, int& value);

protected:

private:
	StatusEffectMap		m_StatusEffects;
	
	inline StatusEffectVector& GetEffectVector(size_t id);
	void RemoveStatusEffect(size_t id, const StatusEffect* effect);
	bool OnApply(size_t id, int& value, std::function<bool(StatusEffect* effect)> event);
};

#endif // STATUS_EFFECT_MANAGER_H
