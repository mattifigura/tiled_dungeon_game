#ifndef GL_SHADER_PROGRAM_H_
#define GL_SHADER_PROGRAM_H_

#include "GLBase.h"
#include "Matrix.h"
#include <string>

class GLShaderProgram {
public:
	GLShaderProgram() : m_VertexShader(0), m_FragmentShader(0), m_ShaderProgram(0) {}
	GLShaderProgram(const std::string& vertex, const std::string& fragment, const std::string& geometry = std::string(""));
	~GLShaderProgram();

	void Clear();
	void CompileShaders();
	void RecompileShaders();

	void SetShaders(const std::string& vertex, const std::string& fragment, const std::string& geometry = std::string(""));

	void Bind();
	void Unbind();

	GLint GetAttribLocation(const GLchar* attrib);
	GLint GetUniformLocation(const GLchar* attrib);

	void SetOrthoProjection(int width, int height);
	void SetPerspectiveProjection(float angle, float ratio, float near, float far, bool bDefaults = true);

	const matrix4f& GetProjectionMatrix() const;

protected:

private:
	GLuint		m_VertexShader;
	GLuint		m_FragmentShader;
	GLuint		m_GeometryShader;
	GLuint		m_ShaderProgram;
	matrix4f	m_ProjMatrix;

	GLfloat		m_fAngle;
	GLfloat		m_fRatio;
	GLfloat		m_fNear;
	GLfloat		m_fFar;
	GLfloat		m_fCurrentAngle;

	std::string m_Vertex;
	std::string m_Fragment;
	std::string m_Geometry;
};

#endif
