#ifndef GAME_MANAGER_H_
#define GAME_MANAGER_H_

#include "GameScene.h"

// GameManager class - handles the creation of the window, loading settings / resources, input handling, scene managing etc

class GameManager {
public:
	GameManager();
	~GameManager();

	bool Initialise();
	void Run();

protected:

private:
	GLFWwindow*	m_pWindow;
	GameScene	m_GameScene; // TEMPORARY: will be a scenes manager object
};

#endif // GAME_MANAGER_H_
