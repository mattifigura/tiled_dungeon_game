#include "DataFile.h"

DataFile::DataFile(const std::string& filename)
	: m_File(filename)
	, m_LinePos(std::string::npos)
	, m_Depth(-1)
{
	if (m_File.is_open())
	{
		// First line is always a comment so we can skip this now
		std::string line;
		std::getline(m_File, line);
	}
}

nsDataFileRead::DataFileRead DataFile::Read()
{
	// Check file is open
	if (!IsOpen())
		return nsDataFileRead::end_of_file;
	
	// New format reading
	m_Depth = -1;
	m_Value = "";
	m_AttributeName = "";
	std::string line;
	if (std::getline(m_File, line))
	{
		// Check if the line is empty
		if (line.empty())
			return nsDataFileRead::empty;
		
		// Get the current depth
		while (line[++m_Depth] == ' ')
			;
		
		// Another check for emptiness
		if (static_cast<size_t>(m_Depth) == line.length())
			return nsDataFileRead::empty;
		
		// Find the position of the attribute operator (:)
		size_t attr = line.find(':', 0);
		m_AttributeName = line.substr(m_Depth, attr - m_Depth);
		
		// Check for a new section
		if (attr == std::string::npos)
			return nsDataFileRead::section;
		
		// Read the attribute value
		m_Value = line.substr(attr + 2);
		return nsDataFileRead::attribute;
	}
	else
	{
		m_File.close();
		return nsDataFileRead::end_of_file;
	}
}

bool DataFile::ReadBool() const
{
	return (m_Value == "true");
}

int DataFile::ReadInt() const
{
	return atoi(m_Value.c_str());
}

float DataFile::ReadFloat() const
{
	return atof(m_Value.c_str());
}

template<class T> Vector2<T> DataFile::ReadVector2() const
{
	Vector2<T> vec;
	size_t pos = 0;
	std::string value;
	int component = 0;
	
	while (ReadToSeparator(',', pos, value) && value != "")
	{
		switch (component++)
		{
			case 0: // X
				vec.x = static_cast<T>(atof(value.c_str()));
				break;
			case 1: // Y
				vec.y = static_cast<T>(atof(value.c_str()));
				break;
			default:
				break;
		}
	}
	
	return vec;
}

template<class T> Vector3<T> DataFile::ReadVector3() const
{
	Vector3<T> vec;
	size_t pos = 0;
	std::string value;
	int component = 0;
	
	while (ReadToSeparator(',', pos, value) && value != "")
	{
		switch (component++)
		{
			case 0: // X
				vec.x = static_cast<T>(atof(value.c_str()));
				break;
			case 1: // Y
				vec.y = static_cast<T>(atof(value.c_str()));
				break;
			case 2: // Z
				vec.z = static_cast<T>(atof(value.c_str()));
				break;
			default:
				break;
		}
	}
	
	return vec;
}

GLColour DataFile::ReadGLColour() const
{
	GLColour col;
	size_t pos = 0;
	std::string value;
	int component = 0;
	
	while (ReadToSeparator(',', pos, value) && value != "")
	{
		switch (component++)
		{
			case 0: // R
				col.r = atoi(value.c_str());
				break;
			case 1: // G
				col.g = atoi(value.c_str());
				break;
			case 2: // B
				col.b = atoi(value.c_str());
				break;
			case 3: // A
				col.a = atoi(value.c_str());
				break;
			default:
				break;
		}
	}
	
	return col;
}

bool DataFile::ReadToSeparator(char separator, size_t& pos, std::string& out) const
{
	if (pos == std::string::npos)
	{
		out = "";
		return false;
	}

	size_t next = m_Value.find(separator, pos + 1);
	
	if (next == std::string::npos)
	{
		size_t start = pos;
		pos = std::string::npos;
		out = m_Value.substr(start);
		return true;
	}
	
	size_t old = pos;
	pos = next + 1;
	
	out = m_Value.substr(old, next - old);
	return true;
}

template Vector2<int> DataFile::ReadVector2<int>() const;
template Vector2<short> DataFile::ReadVector2<short>() const;
template Vector2<float> DataFile::ReadVector2<float>() const;
template Vector2<double> DataFile::ReadVector2<double>() const;
template Vector2<char> DataFile::ReadVector2<char>() const;

template Vector3<int> DataFile::ReadVector3<int>() const;
template Vector3<short> DataFile::ReadVector3<short>() const;
template Vector3<float> DataFile::ReadVector3<float>() const;
template Vector3<double> DataFile::ReadVector3<double>() const;
template Vector3<char> DataFile::ReadVector3<char>() const;