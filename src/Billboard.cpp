#include "SceneManager.h"

Billboard::Billboard(SceneManager* sceneMan)
	: SceneNode(sceneMan)
	, m_HideOnAnimationFinish(false)
{
	// Give us a dynamic mesh
	AssignNewDynamicMesh();
}

Board* Billboard::AddBoard()
{
	Board* board = new Board;
	board->id = m_Boards.size();
	board->animation = -1;
	m_Boards.push_back(std::shared_ptr<Board>(board));
	
	GetDynamicMesh()->AddQuad(GLVertex(vector3df(), GLColour(), vector2df(1.f, 0.f)),
		GLVertex(vector3df(), GLColour(), vector2df(0.f, 0.f)),
		GLVertex(vector3df(), GLColour(), vector2df(0.f, 1.f)),
		GLVertex(vector3df(), GLColour(), vector2df(1.f, 1.f)));
	
	return (m_Boards.at(m_Boards.size() - 1).get());
}

int Billboard::AddAnimation(const vector2di& start, const vector2di& end, const vector2df& size)
{
	// Create a new animation which our billboards can use
	const GLTexture* tex = GetTexture();
	if (tex)
	{
		BoardAnimation animation;
		
		const vector2di& image_size = tex->Dimensions();
		animation.frame_size.Set(size.x / static_cast<float>(image_size.x), size.y / static_cast<float>(image_size.y));
		const int max_frames_x = image_size.x / size.x;
		
		int x = start.x;
		int y = start.y;
		int length = (end.y * max_frames_x) - (start.y * max_frames_x) + (end.x + 1) - start.x;
		
		for (int i = 0; i < length; ++i) {
			animation.frames.push_back(vector2df(animation.frame_size.x * x, animation.frame_size.y * y));
			++x;

			if (x >= max_frames_x)
			{
				x = 0;
				++y;
			}
		}
		
		m_Animations.push_back(animation);
		
		return static_cast<int>(m_Animations.size() - 1);
	}
	
	// No texture, we have failed
	return -1;
}

void Billboard::Update()
{
	// Update our vertex coordinates
	const Camera& camera = GetSceneManager()->GetActiveCamera();
	Mesh<GLVertex>* mesh = GetDynamicMesh();
	vector3df cam(camera.GetBillboardPosition());
	std::vector<GLVertex>& vertices = mesh->GetVertexData();
	float delta = Singleton<DeltaTime>::Instance().GetDelta();
	
	// Go through each board and update it
	for (auto& board : m_Boards) {
		float direction = atan2(board->position.z - cam.z, board->position.x - cam.x);
		float direction2 = atan2(board->position.y - cam.y, vector2df(board->position.x, board->position.z).Distance(vector2df(cam.x, cam.z)));
		float sizeX = board->size.x * 0.5f * sin(-direction);
		float sizeZ = board->size.x * 0.5f * cos(-direction);
		float sizeY = board->size.y * 0.5f * cos(-direction2);
		float tiltX = board->size.y * 0.5f * sin(-direction + PI_OVER_2) * sin(-direction2);
		float tiltZ = board->size.y * 0.5f * cos(-direction + PI_OVER_2) * sin(-direction2);
		
		size_t start = board->id * 4;
		vertices[start].pos = board->position + vector3df(-sizeX - tiltX, -sizeY, -sizeZ - tiltZ);
		vertices[start + 1].pos = board->position + vector3df(sizeX - tiltX, -sizeY, sizeZ - tiltZ);
		vertices[start + 2].pos = board->position + vector3df(sizeX + tiltX, sizeY, sizeZ + tiltZ);
		vertices[start + 3].pos = board->position + vector3df(-sizeX + tiltX, sizeY, -sizeZ + tiltZ);
		
		// Now do animations
		if (board->animation > -1)
		{
			// update the billboard
			board->frame += board->animation_speed * delta;

			if (board->frame >= m_Animations[board->animation].frames.size())
			{
				if (m_HideOnAnimationFinish)
				{
					board->animation_speed = 0.f;
					board->position.y = 9001.f;
					board->frame = m_Animations[board->animation].frames.size() - 1;
				}
				else board->frame = 0;
			}
			else if (board->frame < 0)
			{
				board->frame = m_Animations[board->animation].frames.size() - 0.01f;
			}

			const vector2df& frame = m_Animations[board->animation].frames.at(static_cast<int>(board->frame));
			
			vertices[start].uv.Set(frame.x, 1.f - (frame.y + m_Animations[board->animation].frame_size.y));
			vertices[start + 1].uv.Set(frame.x + m_Animations[board->animation].frame_size.x, 1.f - (frame.y + m_Animations[board->animation].frame_size.y));
			vertices[start + 2].uv.Set(frame.x + m_Animations[board->animation].frame_size.x, 1.f - frame.y);
			vertices[start + 3].uv.Set(frame.x, 1.f - frame.y);
		}
	}
	
	// Buffer our mesh data
	if (mesh->IsDirty())
		mesh->Buffer();
	else
		mesh->Update();
	
	// Call base class Update
	SceneNode::Update();
}
