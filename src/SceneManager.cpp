#include "SceneManager.h"
#include "MeshHelper.h"
#include "Functions.h"
#include "StatusEffectManager.h"
#include <fstream>
#include <string>

static void PrintGLError(const std::string& pre)
{
	// For debugging
	int error = glGetError();
	if (error != 0)
		std::cout << pre << ": " << error << std::endl;
}

SceneManager::SceneManager()
	: m_MeshMan(MeshHelper::CreateMesh)
	, m_TexMan([](const std::string& filename) { return new GLTexture(filename); })
	, m_ActiveCamera(0)
	, m_CurrentActiveTexture(0u)
	, m_pLightMan(nullptr)
	, m_Running(true)
	, m_ParticleThread(&SceneManager::ParticleThreadUpdate, this)
	, m_pTerrain(new Terrain(this))
{
}

SceneManager::~SceneManager()
{
	m_Running = false;
	m_ParticleThread.join();
	
	Singleton<StatusEffectManager>::Instance().Kill();
}

void SceneManager::Initialise()
{
	// Create our GLSL shader programs
	std::string strVertPlain, strFragPlain;
	util::file_to_string("media/shaders/vert_main", strVertPlain);
	util::file_to_string("media/shaders/frag_main", strFragPlain);

	// Add this shader program to our vector
	GLShaderProgram* program = new GLShaderProgram(strVertPlain, strFragPlain);
	program->SetPerspectiveProjection(75, static_cast<float>(GET_SCREEN_SIZE.x) / static_cast<float>(GET_SCREEN_SIZE.y), 1.f, 1000.f);

	// Get our attribute and uniform positions
	program->Bind();
	m_iPositionAttrib = program->GetAttribLocation("position");
	m_iColourAttrib = program->GetAttribLocation("colour");
	m_iTextureAttrib = program->GetAttribLocation("texcoord");
	m_iModelUniform = program->GetUniformLocation("model");
	m_iViewUniform = program->GetUniformLocation("view");
	m_iProjectionUniform = program->GetUniformLocation("projection");
	m_iTexImageUniform = program->GetUniformLocation("texImage");
	m_iLightMapUniform = program->GetUniformLocation("lightMap");
	m_iOutlineUniform = program->GetUniformLocation("outline");
	
	// Send default value in for the teximage and sun uniforms
	glUniform1i(m_iTexImageUniform, 0);
	glUniform1i(m_iLightMapUniform, 1);
	program->Unbind();

	m_Shaders.push_back(std::unique_ptr<GLShaderProgram>(program));

	// Now create the particle shader
	std::string strVertParticle, strGeomParticle, strFragParticle;
	util::file_to_string("media/shaders/vert_particle", strVertParticle);
	util::file_to_string("media/shaders/geom_particle", strGeomParticle);
	util::file_to_string("media/shaders/frag_particle", strFragParticle);
	
	program = new GLShaderProgram(strVertParticle, strFragParticle, strGeomParticle);
	program->SetPerspectiveProjection(75, static_cast<float>(GET_SCREEN_SIZE.x) / static_cast<float>(GET_SCREEN_SIZE.y), 1.f, 1000.f);
	
	// Get our attribute and uniform positions
	program->Bind();
	m_iParticlePositionAttrib = program->GetAttribLocation("position");
	m_iParticleColourAttrib = program->GetAttribLocation("colour");
	m_iParticleSizeAttrib = program->GetAttribLocation("size");
	m_iParticleViewUniform = program->GetUniformLocation("view");
	m_iParticleProjectionUniform = program->GetUniformLocation("projection");
	m_iParticleLightMapUniform = program->GetUniformLocation("lightMap");
	m_iParticleLightingUniform = program->GetUniformLocation("lighting");
	
	glUniform1i(m_iLightMapUniform, 0);
	program->Unbind();
	
	m_Shaders.push_back(std::unique_ptr<GLShaderProgram>(program));

	// Load the texture for the terrain
	m_pTerrain->SetTexture(&m_TexMan.GetResource("media/terrain_tiles.png"));

	// Create our light manager
	m_pLightMan.reset(new LightManager(21000, 1024));
	
	// Just create a bunch of random lights
	for (int i = 0; i < 50; ++i) {
		vector3df pos(2 + rand() % (AREA_SIZE - 4) + 0.5f, 0.1f, 2 + rand() % (AREA_SIZE - 4) + 0.5f);
		GLColour col(rand() % 255, rand() % 255, rand() % 255, 255);
		m_pLightMan->CreateLight(vector2df(pos.x, pos.z), col, vector2df(2.4f, 2.4f));
		
		ParticleEmitter* emitter = CreateParticleEmitter(pos, 100);
		EmitterBehaviour& behaviour = emitter->GetBehaviour();
		
		behaviour.duration = 1000.f;
		behaviour.duration_randomiser = 1000.f;
		behaviour.gravity.Set(0.f, 0.f, 0.f);
		behaviour.start_colour = col;
		behaviour.end_colour.Set(col.r, col.g, col.b, 0);
		behaviour.position_randomiser_lower.Set(-0.25f, 0.f, -0.25f);
		behaviour.position_randomiser_upper.Set(0.25f, 0.1f, 0.25f);
		behaviour.force.Set(0.f, 0.6f, 0.f);
		behaviour.force_randomiser_lower.Set(-0.25f, -0.1f, -0.25f);
		behaviour.force_randomiser_upper.Set(0.25f, 0.1f, 0.25f);
		behaviour.friction.Set(0.f, 0.f, 0.f);
		behaviour.start_size.Set(0.08f, 0.08f);
		behaviour.end_size.Set(0.01f, 0.01f);
		behaviour.collisions = false;
		behaviour.bounce.Set(1.f, 1.f, 1.f);
		behaviour.only_live_once = false;
		behaviour.force_from_start = false;
		
		emitter->AddLighting();
	}
}

void SceneManager::Update()
{
	// Update the scene, starting with the terrain and player
	m_pTerrain->Update(GetActiveCamera().GetPosition(), GetActiveCamera().GetViewFrustum());
	m_pLightMan->Update();

	// Now update our cameras
	for (auto& it : m_Cameras) {
		it->Update();
	}

	// Now update each scene node individually
	NodeVector::iterator it = m_SceneNodes.begin();
	while (it != m_SceneNodes.end())
	{
		// Check if this scene node is dead, and if it is, delete it
		if ((*it)->IsDead())
			it = m_SceneNodes.erase(it);
		else
		{
			(*it)->Update();
			++it;
		}
	}
	
	// Buffer particle emitters
	if (m_ParticleMutex.try_lock())
	{
		EmitterVector::iterator pit = m_Emitters.begin();
		while (pit != m_Emitters.end())
		{
			// Check if it is dead, and if it is, delete it
			if ((*pit)->IsDead())
			{
				pit = m_Emitters.erase(pit);
			}
			else
			{
				(*pit)->Buffer();
				++pit;
			}
		}
		m_ParticleMutex.unlock();
	}
}

void SceneManager::Render()
{
	// Firstly render our lights
	m_pLightMan->Render();

	// Begin the rendering
	StartRender();

	// Render the terrain first as everything else should be rendered over top
	static matrix4f identity;
	glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, identity.GetDataPointer());
	UseTexture(m_pTerrain->GetTexture()->ID());
	RenderMeshes(m_pTerrain->GetRenderList());

	// Render our SceneNodes
	for (auto& it : m_SceneNodes) {
		const MeshBase* mesh = it->GetMesh();
		if (it->IsVisible() && mesh && VALID_BUFFER(mesh->GetVBO()))
		{
			glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, it->GetTransformationMatrix().GetDataPointer());
			glUniform1i(m_iOutlineUniform, it->IsOutline());

			if (it->GetTexture())
				UseTexture(it->GetTexture()->ID());

			if (it->ClearsDepthBuffer())
				glClear(GL_DEPTH_BUFFER_BIT);

			if (it->CullFrontFace())
				glCullFace(GL_FRONT);

			RenderMesh(mesh, it->GetRenderMode());

			if (it->CullFrontFace())
				glCullFace(GL_BACK);
		}
	}
	
	glUniform1i(m_iOutlineUniform, false);

	// Finish the rendering
	FinishRender();
	
	// Now render any particle effects
	m_Shaders.at(1)->Bind();
	glUniformMatrix4fv(m_iParticleViewUniform, 1, GL_TRUE, GetActiveCamera().GetCameraMatrix());
	GLsizei size = sizeof(Particle);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_pLightMan->GetLightMap());
	
	const ViewFrustum& frustum = GetActiveCamera().GetViewFrustum();
	for (auto& it : m_Emitters) {
		if (!frustum.PointInFrustum(it->GetPosition()))
			continue;
		
		size_t count(0);
		unsigned int vbo = it->GetBufferObject(count);
		
		if (VALID_BUFFER(vbo))
		{
			glUniform1i(m_iParticleLightingUniform, it->IsAffectedByLighting());
			
			glBindBuffer(GL_ARRAY_BUFFER, vbo);

			glEnableVertexAttribArray(m_iParticlePositionAttrib);
			glEnableVertexAttribArray(m_iParticleColourAttrib);
			glEnableVertexAttribArray(m_iParticleSizeAttrib);

			glVertexAttribPointer(m_iParticlePositionAttrib, 3, GL_FLOAT, GL_FALSE, size, 0);
			glVertexAttribPointer(m_iParticleColourAttrib, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(12));
			glVertexAttribPointer(m_iParticleSizeAttrib, 2, GL_FLOAT, GL_TRUE, size, (GLvoid *)(16));

			//glDrawElements(GL_POINTS, count, GL_UNSIGNED_INT, 0);
			glDrawArrays(GL_POINTS, 0, count);

			glDisableVertexAttribArray(m_iParticlePositionAttrib);
			glDisableVertexAttribArray(m_iParticleColourAttrib);
			glDisableVertexAttribArray(m_iParticleSizeAttrib);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
	}
	
	PrintGLError("Render 3");
}

void SceneManager::RenderMeshes(const std::vector<MeshBase*>& meshes)
{
	for (auto& it : meshes) {
		if (it && VALID_BUFFER(it->GetVBO()))
			RenderMesh(it);
	}
}

void SceneManager::UseTexture(GLuint texture)
{
	// Set the texture currently needed for rendering
	if (m_CurrentActiveTexture != texture)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		m_CurrentActiveTexture = texture;
	}
}

void SceneManager::AddCamera()
{
	// Add a new camera to our vector
	m_Cameras.push_back(std::unique_ptr<Camera>(new Camera(m_Shaders.at(0)->GetProjectionMatrix())));
}

void SceneManager::SetActiveCamera(int camera)
{
	m_ActiveCamera = camera;
}

const Camera& SceneManager::GetActiveCamera() const
{
	return *m_Cameras.at(m_ActiveCamera);
}

Camera& SceneManager::GetActiveCamera()
{
	return *m_Cameras.at(m_ActiveCamera);
}

const GLTexture& SceneManager::GetTexture(const std::string& filename)
{
	return m_TexMan.GetResource(filename);
}

const MeshBase& SceneManager::GetMesh(const std::string& key)
{
	return m_MeshMan.GetResource(key);
}

void SceneManager::GetRayFromScreenCoords(line3df& out)
{
	Camera& Camera = GetActiveCamera();

	const ViewFrustum& vf = Camera.GetViewFrustum();
	vector3df farLeftUp = vf.FarLeftUp();
	vector3df leftToRight = vf.FarRightUp() - farLeftUp;
	vector3df upToDown = vf.FarLeftDown() - farLeftUp;

	const vector2di& ScreenSize = GET_SCREEN_SIZE;
	const vector2di& CursorPos = GET_CURSOR_POSITION;

	GLfloat dx = CursorPos.x / (GLfloat)ScreenSize.x;
	GLfloat dy = CursorPos.y / (GLfloat)ScreenSize.y;

	out.start = Camera.GetPosition();
	out.end = farLeftUp + (leftToRight * dx) + (upToDown * dy);
}

SceneNode& SceneManager::CreateSceneNode()
{
	// Create a new SceneNode and return a reference to it
	SceneNode* node = new SceneNode(this);
	node->SetTexture("media/all_white.png");
	m_SceneNodes.push_back(std::unique_ptr<SceneNode>(node));
	return *m_SceneNodes.at(m_SceneNodes.size() - 1);
}

Billboard& SceneManager::CreateBillboard()
{
	// Create a new Billboard and return a reference to it
	Billboard* board = new Billboard(this);
	board->SetTexture("media/all_white.png");
	m_SceneNodes.push_back(std::unique_ptr<SceneNode>(board));
	return dynamic_cast<Billboard&>(*m_SceneNodes.at(m_SceneNodes.size() - 1));
}

Light* SceneManager::CreateLight(const vector2df& pos, const GLColour& col, const vector2df& size, bool reserved)
{
	return m_pLightMan->CreateLight(pos, col, size, reserved);
}

ParticleEmitter* SceneManager::CreateParticleEmitter(const vector3df& pos, size_t size)
{
	// Create a new Particle Emitter and return a reference to it
	{
		std::lock_guard<FastMutex> lock(m_ParticleMutex);
		m_Emitters.push_back(std::unique_ptr<ParticleEmitter>(new ParticleEmitter(this, size)));
	}
	
	ParticleEmitter* emitter = dynamic_cast<ParticleEmitter*>(m_Emitters.at(m_Emitters.size() - 1).get());
	emitter->SetPosition(pos);
	
	return emitter;
}

ParticleEmitter* SceneManager::CreateParticleEmitter(const vector3df& pos, const ParticleDefinition& definition)
{
	// Create a new Particle Emitter and return a reference to it
	{
		std::lock_guard<FastMutex> lock(m_ParticleMutex);
		m_Emitters.push_back(std::unique_ptr<ParticleEmitter>(new ParticleEmitter(this, definition.count)));
	}
	
	ParticleEmitter* emitter = dynamic_cast<ParticleEmitter*>(m_Emitters.at(m_Emitters.size() - 1).get());
	emitter->SetPosition(pos);
	emitter->SetAffectedByLighting(definition.affected_by_lights);
	
	if (definition.lights)
		emitter->AddLighting();
	
	if (definition.burst)
		emitter->Burst();
	
	emitter->GetBehaviour() = definition.behaviour;
	emitter->SpawnAll();
	
	return emitter;
}

const Terrain& SceneManager::GetTerrain() const
{
	return *m_pTerrain;
}

Terrain& SceneManager::GetTerrain()
{
	return *m_pTerrain;
}

void SceneManager::EnableVertexAttributes()
{
	// Our shaders have been set up with fixed attribute locations, meaning we know exactly where everything lies
	// This is set up in the defines in the SceneManager header file
	glEnableVertexAttribArray(m_iPositionAttrib);
	glEnableVertexAttribArray(m_iColourAttrib);
	glEnableVertexAttribArray(m_iTextureAttrib);

	GLsizei size = sizeof(GLVertex);
	glVertexAttribPointer(m_iPositionAttrib, 3, GL_FLOAT, GL_FALSE, size, 0);
	glVertexAttribPointer(m_iColourAttrib, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(12));
	glVertexAttribPointer(m_iTextureAttrib, 2, GL_SHORT, GL_FALSE, size, (GLvoid *)(16));
}

void SceneManager::DisableVertexAttributes()
{
	// Disable the vertex attributes
	glDisableVertexAttribArray(m_iPositionAttrib);
	glDisableVertexAttribArray(m_iColourAttrib);
	glDisableVertexAttribArray(m_iTextureAttrib);
}

void SceneManager::StartRender()
{
	// Clear the screen
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set the viewport
	glViewport(0, 0, GET_SCREEN_SIZE.x, GET_SCREEN_SIZE.y);
	
	// Reset the current active texture as this could be wrong due to outside events
	m_CurrentActiveTexture = 0u;

	// Bind our shader program
	m_Shaders.at(0)->Bind();

	// Uniform camera matrix
	glUniformMatrix4fv(m_iViewUniform, 1, GL_TRUE, GetActiveCamera().GetCameraMatrix());
	
	glDisable(GL_CULL_FACE);
}

void SceneManager::FinishRender()
{
	// Not entirely sure if this needs to do anything at the moment
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void SceneManager::RenderMesh(const MeshBase* mesh, int mode)
{
	// Render this mesh
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_pLightMan->GetLightMap());

	glBindBuffer(GL_ARRAY_BUFFER, mesh->GetVBO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetEBO());

	EnableVertexAttributes();

	glDrawElements(mode, static_cast<GLsizei>(mesh->GetIndexBufferSize()), GL_UNSIGNED_INT, 0);

	DisableVertexAttributes();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SceneManager::ParticleThreadUpdate()
{
	DeltaTime time;
	std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
	int sleep_time = 16;
	
	while (m_Running)
	{
		// Update the delta time for this thread
		time.CalculateDelta();
		float delta = time.GetDelta();
		start = std::chrono::system_clock::now();
		
		// Update particle emitters
		if (!m_Cameras.empty())
		{
			const ViewFrustum& frustum = GetActiveCamera().GetViewFrustum();
			
			std::lock_guard<FastMutex> lock(m_ParticleMutex);
			EmitterVector::iterator pit = m_Emitters.begin();
			while (pit != m_Emitters.end())
			{
				// Update only if we're on screen
				if (frustum.PointInFrustum((*pit)->GetPosition()))
					(*pit)->Update(delta);
				++pit;
			}
		}
		
		sleep_time = std::max(16 - static_cast<int>((std::chrono::system_clock::now() - start).count() / 1000.f), 4);
		std::this_thread::sleep_for(std::chrono::milliseconds(sleep_time));
	}
}
