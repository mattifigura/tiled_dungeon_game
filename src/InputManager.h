#ifndef INPUT_MANAGER_H_
#define INPUT_MANAGER_H_

#include "GLBase.h"
#include "Singleton.h"
#include <vector>

// InputObject class - this pure virtual class can be used for objects which need to attach to mouse / keyboard events
class InputObject {
public:
	// Virtual destructor
	InputObject() : m_bOverride(false) {}
	virtual ~InputObject();

	// All these return false by default to show they're not implemented / aren't triggered
	virtual bool MouseEvent(int button, int action, int mods) { return false; }
	virtual bool KeyboardEvent(int key, int scancode, int action, int mods) { return false; }
	virtual bool CharEvent(unsigned int codepoint) { return false; }
	virtual bool ScrollEvent(double x, double y) { return false; }

	// Used by the InputManager to see if this is an override
	bool IsOverride() const { return m_bOverride; }

protected:
	void SetOverride(bool override = true) { m_bOverride = override; }

private:
	bool	m_bOverride;
};

typedef std::vector<InputObject*> InputVector;

// InputManager class - handles the inputs for the window it is attached to
// Note this will only be valid for a single window application, such as this game, as this is a singleton class

class InputManager {
public:
	InputManager();
	
	void AttachWindow(GLFWwindow* window);
	void Update();

	const vector2di& GetScreenSize() const;

	const vector2di& GetCursorPosition() const;
	bool SetCursorPosition(InputObject* who, const vector2di& pos);

	void SetCursorVisible(bool visible = true);
	bool IsCursorVisible() const;

	bool IsKeyPressed(InputObject* who, int key) const;
	bool IsButtonPressed(InputObject* who, int button) const;

	bool Lock(InputObject* who);
	void Unlock(InputObject* who);

	void AddInputObject(InputObject* object);
	void RemoveInputObject(InputObject* object);

	void MouseEvent(int button, int action, int mods);
	void KeyboardEvent(int key, int scancode, int action, int mods);
	void CharEvent(unsigned int codepoint);
	void ScrollEvent(double x, double y);

protected:

private:
	GLFWwindow*		m_pWindow;
	vector2di		m_CursorPosition;
	vector2di		m_ScreenSize;
	InputObject*	m_Lock;
	InputVector		m_InputObjects;

	inline bool Locked(InputObject* who) const;
};

// Definitions for easier-to-read code
#define IS_KEY_PRESSED(key) Singleton<InputManager>::Instance().IsKeyPressed(this, key)
#define IS_BUTTON_PRESSED(button) Singleton<InputManager>::Instance().IsButtonPressed(this, button)
#define SET_CURSOR_POSITION(pos) Singleton<InputManager>::Instance().SetCursorPosition(this, pos)
#define GET_CURSOR_POSITION Singleton<InputManager>::Instance().GetCursorPosition()
#define GET_SCREEN_SIZE Singleton<InputManager>::Instance().GetScreenSize()
#define LOCK_INPUT Singleton<InputManager>::Instance().Lock(this)
#define UNLOCK_INPUT Singleton<InputManager>::Instance().Unlock(this)
#define REGISTER_INPUT_OBJECT Singleton<InputManager>::Instance().AddInputObject(this)
#define UNREGISTER_INPUT_OBJECT Singleton<InputManager>::Instance().RemoveInputObject(this)
#define SET_MOUSE_CURSOR_VISIBLE(visible) Singleton<InputManager>::Instance().SetCursorVisible(visible)

#endif
