#ifndef FONT_H_
#define FONT_H_

#include "GLTexture.h"
#include <memory>

// FontGlyph struct - this holds details required about each character in order to create the text correctly in openGL
struct FontGlyph {
	GLubyte width;
	vector2ds pos;
	GLubyte advance;
};

typedef std::unordered_map<char, FontGlyph> FontMap;

// Font class - this loads a freetype font, creates a texture and provides information on each glyph
// Fonts should be loaded with following format (passed in as a string):
// [filename]:[size] - ie "media/sans.ttf:12" or "media/sans.ttf:16"

class Font {
public:
	Font(const std::string& key);

	const FontGlyph& GetCharacterInfo(char c) const;
	const GLTexture* GetTexture() const;

	int GetHeight() const;

	int GetStringWidth(const std::string& in) const;

protected:

private:
	std::unique_ptr<GLTexture>	m_Texture;
	FontMap						m_Font;
	int							m_iHeight;
	int							m_iFontSize;

	void ExtractKey(const std::string& key, std::string& filename, int& size);
};

#endif // FONT_H_
