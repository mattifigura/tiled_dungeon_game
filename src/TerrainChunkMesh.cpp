#include "TerrainChunkMesh.h"
#include "TerrainData.h"
#include "Terrain.h"

TerrainChunkMesh::TerrainChunkMesh()
	: Mesh<GLVertex>()
	, m_bNeedsCreation(false)
{
	// We need not do anything else in the constructor
}

void TerrainChunkMesh::BufferTerrain()
{
	// This must be called in the main thread (where OpenGL is current)
	// This will buffer the data into the OpenGL buffers and then clear our internal storage
	if (m_bNeedsCreation && IsDirty())
	{
		// Lock the mutex and buffer the data
		Buffer(false);

		// Set our needs creation flag to false and unlock the mutex
		m_bNeedsCreation = false;
	}
}

static void GetUV(const nsTile::Tile tile, vector4df& uv)
{
	// As tiles are hard programmed we need a function to get the UV coordinates of each tile type
	switch (tile)
	{
	case nsTile::water:
		uv.Set(4.f, 16.f, 5.f, 17.f);
		break;
	case nsTile::dirt:
		uv.Set(0.f, 16.f, 1.f, 17.f);
		break;
	case nsTile::wood:
		uv.Set(3.f, 16.f, 4.f, 17.f);
		break;
	case nsTile::stone_1:
		uv.Set(2.f, 16.f, 3.f, 17.f);
		break;
	case nsTile::cave_1:
		uv.Set(1.f, 16.f, 2.f, 17.f);
		break;
	default:
		uv.Set(0.f, 0.f, 1.f, 1.f);
		break;
	}

	// Now scale it appropriately
	static const float fScale = 1.f / 32.f;
	uv *= fScale;
}

static void GetWallUV(const nsTile::Tile tile, vector4df& uv)
{
	// As tiles are hard programmed we need a function to get the UV coordinates of each tile wall type
	int iRandomTexture(4);
	switch (tile)
	{
	case nsTile::stone_wall_1:
		uv.Set(0.f, 0.f, 1.f, 1.f);
		break;
	case nsTile::stone_wall_2:
		uv.Set(4.f, 0.f, 5.f, 1.f);
		break;
	case nsTile::wooden_wall_1:
		uv.Set(12.f, 0.f, 13.f, 1.f);
		break;
	default:
		uv.Set(4.f, 0.f, 5.f, 1.f);
		break;
	}

	// Now add on the randomness!
	if (iRandomTexture > 0)
	{
		float fRandomTexture = static_cast<float>(std::max(-8 + rand() % (8 + iRandomTexture), 0));
		uv += vector4df(fRandomTexture, 0.f, fRandomTexture, 0.f);
	}

	// Now scale it appropriately
	static const float fScale = 1.f / 32.f;
	uv *= fScale;
}

void TerrainChunkMesh::CreateMesh(const Terrain* terrain)
{
	// Set needs creation to false whilst we construct
	m_bNeedsCreation = false;

	// Clear our data, just in case
	Clear();

	// Function to get the tile from the tile array
	const Tile empty_tile;
	auto get_tile = [&](int x, int y) -> const Tile& {
		if (x < 0 || x >= AREA_SIZE || y < 0 || y >= AREA_SIZE)
			return empty_tile;

		return terrain->GetTile(x, y);
	};

	// Function to add a quad
	auto add_quad = [&](const GLVertex& ver1, const GLVertex& ver2, const GLVertex& ver3, const GLVertex& ver4, bool flip = false) {
		// Get non-const GLVertex
		GLVertex _ver1(ver1), _ver2(ver2), _ver3(ver3), _ver4(ver4);

		// Sort out the UV coordinates
		GLushort temp;
		_ver1.uv.v = 8192 - ver1.uv.v;
		_ver2.uv.v = 8192 - ver2.uv.v;
		_ver3.uv.v = 8192 - ver3.uv.v;
		_ver4.uv.v = 8192 - ver4.uv.v;

		temp = _ver1.uv.v;
		_ver1.uv.v = _ver4.uv.v;
		_ver4.uv.v = temp;

		temp = _ver2.uv.v;
		_ver2.uv.v = _ver3.uv.v;
		_ver3.uv.v = temp;

		// Call the Mesh::AddQuad function
		AddQuad(_ver1, _ver2, _ver3, _ver4, flip);
	};

	// Loop through each block and add faces as needed
	vector4df uv;
	vector2df half_uv;
	GLColour col(255);
	float fPixel = (1.f / 512.f);

	for (int j = 0; j < AREA_SIZE; ++j) {
		for (int i = 0; i < AREA_SIZE; ++i) {
			// Get the tile at this position
			const Tile& tile = get_tile(i, j);

			// If there is nothing here move on
			if (tile.tile == nsTile::empty)
				continue;

			// Get the adjacent tiles
			nsTile::Tile left = get_tile(i - 1, j).tile;
			nsTile::Tile right = get_tile(i + 1, j).tile;
			nsTile::Tile down = get_tile(i, j - 1).tile;
			nsTile::Tile up = get_tile(i, j + 1).tile;

			// So we don't have to keep calculating positions and casting to float, these are used to help create the mesh
			float x1 = static_cast<float>(i);
			float x2 = x1 + 1.f;
			float y = terrain->GetTileHeight(tile.tile);
			float z1 = static_cast<float>(j);
			float z2 = z1 + 1.f;
			float x3 = x1 + 0.5f;
			float z3 = z1 + 0.5f;
			static const unsigned char shade(180);

			if (tile.tile > nsTile::last_wall_type)
			{
				// Get the UV
				GetUV(tile.tile, uv);
				half_uv.x = (uv.x + uv.z) * 0.5f;
				half_uv.y = (uv.y + uv.w) * 0.5f;

				// Sort out shading
				unsigned char left_shade = (left < nsTile::last_wall_type ? shade : 255);
				unsigned char right_shade = (right < nsTile::last_wall_type ? shade : 255);
				unsigned char down_shade = (down < nsTile::last_wall_type ? shade : 255);
				unsigned char up_shade = (up < nsTile::last_wall_type ? shade : 255);
				unsigned char left_up_shade = std::min(left_shade, up_shade);
				unsigned char left_down_shade = std::min(left_shade, down_shade);
				unsigned char right_up_shade = std::min(right_shade, up_shade);
				unsigned char right_down_shade = std::min(right_shade, down_shade);
				
				if (left_up_shade == 255) left_up_shade = (get_tile(i - 1, j + 1).tile < nsTile::last_wall_type ? shade : 255);
				if (left_down_shade == 255) left_down_shade = (get_tile(i - 1, j - 1).tile < nsTile::last_wall_type ? shade : 255);
				if (right_up_shade == 255) right_up_shade = (get_tile(i + 1, j + 1).tile < nsTile::last_wall_type ? shade : 255);
				if (right_down_shade == 255) right_down_shade = (get_tile(i + 1, j - 1).tile < nsTile::last_wall_type ? shade : 255);

				// If all shades are 255, just do one quad
				if (left_shade == 255 && right_shade == 255 && down_shade == 255 && up_shade == 255 &&
					left_up_shade == 255 && left_down_shade == 255 && right_up_shade == 255 && right_down_shade == 255)
				{
					add_quad(GLVertex(vector3df(x2, y, z1), GLColour(), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x1, y, z1), GLColour(), vector2df(uv.z, uv.y)),
						GLVertex(vector3df(x1, y, z2), GLColour(), vector2df(uv.z, uv.w)),
						GLVertex(vector3df(x2, y, z2), GLColour(), vector2df(uv.x, uv.w)));
				}
				else
				{
					// Create 4 quads so that we can add shading
					add_quad(GLVertex(vector3df(x3, y, z1), GLColour(down_shade), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x1, y, z1), GLColour(left_down_shade), vector2df(half_uv.x, uv.y)),
						GLVertex(vector3df(x1, y, z3), GLColour(left_shade), vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x3, y, z3), GLColour(255), vector2df(uv.x, half_uv.y)), true);
					
					add_quad(GLVertex(vector3df(x2, y, z1), GLColour(right_down_shade), vector2df(half_uv.x, uv.y)),
						GLVertex(vector3df(x3, y, z1), GLColour(down_shade), vector2df(uv.z, uv.y)),
						GLVertex(vector3df(x3, y, z3), GLColour(255), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x2, y, z3), GLColour(right_shade), vector2df(half_uv.x, half_uv.y)));
					
					add_quad(GLVertex(vector3df(x3, y, z3), GLColour(255), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y, z3), left_shade, vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y, z2), GLColour(left_up_shade), vector2df(half_uv.x, uv.w)),
						GLVertex(vector3df(x3, y, z2), up_shade, vector2df(uv.x, uv.w)));
					
					add_quad(GLVertex(vector3df(x2, y, z3), right_shade, vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x3, y, z3), GLColour(255), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x3, y, z2), up_shade, vector2df(uv.z, uv.w)),
						GLVertex(vector3df(x2, y, z2), GLColour(right_up_shade), vector2df(half_uv.x, uv.w)), true);
				}
				
				/*add_quad(GLVertex(vector3df(x2, 1.f, z2), GLColour(), vector2df(uv.x, uv.w), vector3df(0.f, -1.f, 0.f)),
					GLVertex(vector3df(x1, 1.f, z2), GLColour(), vector2df(uv.z, uv.w), vector3df(0.f, -1.f, 0.f)),
					GLVertex(vector3df(x1, 1.f, z1), GLColour(), vector2df(uv.z, uv.y), vector3df(0.f, -1.f, 0.f)),
					GLVertex(vector3df(x2, 1.f, z1), GLColour(), vector2df(uv.x, uv.y), vector3df(0.f, -1.f, 0.f)));*/
			}
			else
			{
				// Now check if we need to create any walls
				y = 0.f;
				GetWallUV(tile.tile, uv);
				if (up > nsTile::last_wall_type)
				{
					half_uv.y = (uv.y + uv.w) * 0.5f;
					add_quad(GLVertex(vector3df(x2, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x1, y + 0.5f, z2), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y, z2), GLColour(shade), vector2df(uv.x, uv.w)),
						GLVertex(vector3df(x2, y, z2), GLColour(shade), vector2df(uv.z, uv.w)));
						
					add_quad(GLVertex(vector3df(x2, y + 1.f, z2), GLColour(), vector2df(uv.z, uv.y)),
						GLVertex(vector3df(x1, y + 1.f, z2), GLColour(), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x1, y + 0.5f, z2), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x2, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)));
				}

				GetWallUV(tile.tile, uv);
				if (down > nsTile::last_wall_type)
				{
					half_uv.y = (uv.y + uv.w) * 0.5f;
					add_quad(GLVertex(vector3df(x2, y, z1), GLColour(shade), vector2df(uv.z, uv.w)),
						GLVertex(vector3df(x1, y, z1), GLColour(shade), vector2df(uv.x, uv.w)),
						GLVertex(vector3df(x1, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x2, y + 0.5f, z1), GLColour(), vector2df(uv.z, half_uv.y)));
					
					add_quad(GLVertex(vector3df(x2, y + 0.5f, z1), GLColour(), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x1, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y + 1.f, z1), GLColour(), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x2, y + 1.f, z1), GLColour(), vector2df(uv.z, uv.y)));
				}

				GetWallUV(tile.tile, uv);
				if (left > nsTile::last_wall_type)
				{
					half_uv.y = (uv.y + uv.w) * 0.5f;
					add_quad(GLVertex(vector3df(x1, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y, z1), GLColour(shade), vector2df(uv.x, uv.w)),
						GLVertex(vector3df(x1, y, z2), GLColour(shade), vector2df(uv.z, uv.w)),
						GLVertex(vector3df(x1, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)));
					
					add_quad(GLVertex(vector3df(x1, y + 1.f, z1), GLColour(), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x1, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x1, y + 1.f, z2), GLColour(), vector2df(uv.z, uv.y)));
				}

				GetWallUV(tile.tile, uv);
				if (right > nsTile::last_wall_type)
				{
					half_uv.y = (uv.y + uv.w) * 0.5f;
					add_quad(GLVertex(vector3df(x2, y, z1), GLColour(shade), vector2df(uv.x, uv.w)),
						GLVertex(vector3df(x2, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x2, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x2, y, z2), GLColour(shade), vector2df(uv.z, uv.w)));
					
					add_quad(GLVertex(vector3df(x2, y + 0.5f, z1), GLColour(), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x2, y + 1.f, z1), GLColour(), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x2, y + 1.f, z2), GLColour(), vector2df(uv.z, uv.y)),
						GLVertex(vector3df(x2, y + 0.5f, z2), GLColour(), vector2df(uv.z, half_uv.y)));
				}
				
				// The top of the wall is going to have shading
				GetUV(tile.tile, uv);
				half_uv.x = (uv.x + uv.z) * 0.5f;
				half_uv.y = (uv.y + uv.w) * 0.5f;
				
				static const unsigned char top_shade(255);
				unsigned char left_shade = (left > nsTile::last_wall_type ? top_shade : 0);
				unsigned char right_shade = (right > nsTile::last_wall_type ? top_shade : 0);
				unsigned char down_shade = (down > nsTile::last_wall_type ? top_shade : 0);
				unsigned char up_shade = (up > nsTile::last_wall_type ? top_shade : 0);
				unsigned char left_up_shade = std::max(left_shade, up_shade);
				unsigned char left_down_shade = std::max(left_shade, down_shade);
				unsigned char right_up_shade = std::max(right_shade, up_shade);
				unsigned char right_down_shade = std::max(right_shade, down_shade);
				
				if (left_up_shade == 0) left_up_shade = (get_tile(i - 1, j + 1).tile > nsTile::last_wall_type ? top_shade : 0);
				if (left_down_shade == 0) left_down_shade = (get_tile(i - 1, j - 1).tile > nsTile::last_wall_type ? top_shade : 0);
				if (right_up_shade == 0) right_up_shade = (get_tile(i + 1, j + 1).tile > nsTile::last_wall_type ? top_shade : 0);
				if (right_down_shade == 0) right_down_shade = (get_tile(i + 1, j - 1).tile > nsTile::last_wall_type ? top_shade : 0);
				
				add_quad(GLVertex(vector3df(x3, 1.f, z1), GLColour(down_shade), vector2df(uv.x, uv.y)),
						GLVertex(vector3df(x1, 1.f, z1), GLColour(left_down_shade), vector2df(half_uv.x, uv.y)),
						GLVertex(vector3df(x1, 1.f, z3), GLColour(left_shade), vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x3, 1.f, z3), GLColour(0), vector2df(uv.x, half_uv.y)), true);
					
					add_quad(GLVertex(vector3df(x2, 1.f, z1), GLColour(right_down_shade), vector2df(half_uv.x, uv.y)),
						GLVertex(vector3df(x3, 1.f, z1), GLColour(down_shade), vector2df(uv.z, uv.y)),
						GLVertex(vector3df(x3, 1.f, z3), GLColour(0), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x2, 1.f, z3), GLColour(right_shade), vector2df(half_uv.x, half_uv.y)));
					
					add_quad(GLVertex(vector3df(x3, 1.f, z3), GLColour(0), vector2df(uv.x, half_uv.y)),
						GLVertex(vector3df(x1, 1.f, z3), left_shade, vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x1, 1.f, z2), GLColour(left_up_shade), vector2df(half_uv.x, uv.w)),
						GLVertex(vector3df(x3, 1.f, z2), up_shade, vector2df(uv.x, uv.w)));
					
					add_quad(GLVertex(vector3df(x2, 1.f, z3), right_shade, vector2df(half_uv.x, half_uv.y)),
						GLVertex(vector3df(x3, 1.f, z3), GLColour(0), vector2df(uv.z, half_uv.y)),
						GLVertex(vector3df(x3, 1.f, z2), up_shade, vector2df(uv.z, uv.w)),
						GLVertex(vector3df(x2, 1.f, z2), GLColour(right_up_shade), vector2df(half_uv.x, uv.w)), true);
				
				/*add_quad(GLVertex(vector3df(x2, 1.f, z1), GLColour(0), vector2df(uv.x, uv.y)),
					GLVertex(vector3df(x1, 1.f, z1), GLColour(0), vector2df(uv.z, uv.y)),
					GLVertex(vector3df(x1, 1.f, z2), GLColour(0), vector2df(uv.z, uv.w)),
					GLVertex(vector3df(x2, 1.f, z2), GLColour(0), vector2df(uv.x, uv.w)));*/
			}

			// Certain tile types that are higher than others will overflow into adjacent tiles that are different
			// Left
			float yDest = terrain->GetTileHeight(left);
			if (y > yDest)
			{
				float yUV = (y - yDest) * fPixel * 16.f;
				add_quad(GLVertex(vector3df(x1, y, z1), GLColour(), vector2df(uv.x, uv.y)),
					GLVertex(vector3df(x1, yDest, z1), GLColour(), vector2df(uv.x, uv.y + yUV)),
					GLVertex(vector3df(x1, yDest, z2), GLColour(), vector2df(uv.z, uv.y + yUV)),
					GLVertex(vector3df(x1, y, z2), GLColour(), vector2df(uv.z, uv.y)));
			}

			// Right
			yDest = terrain->GetTileHeight(right);
			if (y > yDest)
			{
				float yUV = (y - yDest) * fPixel * 16.f;
				add_quad(GLVertex(vector3df(x2, yDest, z1), GLColour(), vector2df(uv.x, uv.y + yUV)),
					GLVertex(vector3df(x2, y, z1), GLColour(), vector2df(uv.x, uv.y)),
					GLVertex(vector3df(x2, y, z2), GLColour(), vector2df(uv.z, uv.y)),
					GLVertex(vector3df(x2, yDest, z2), GLColour(), vector2df(uv.z, uv.y + yUV)));
			}

			// Down
			yDest = terrain->GetTileHeight(down);
			if (y > yDest)
			{
				float yUV = (y - yDest) * fPixel * 16.f;
				add_quad(GLVertex(vector3df(x2, yDest, z1), GLColour(), vector2df(uv.z, uv.y + yUV)),
					GLVertex(vector3df(x1, yDest, z1), GLColour(), vector2df(uv.x, uv.y + yUV)),
					GLVertex(vector3df(x1, y, z1), GLColour(), vector2df(uv.x, uv.y)),
					GLVertex(vector3df(x2, y, z1), GLColour(), vector2df(uv.z, uv.y)));
			}

			// Up
			yDest = terrain->GetTileHeight(up);
			if (y > yDest)
			{
				float yUV = (y - yDest) * fPixel * 16.f;
				add_quad(GLVertex(vector3df(x2, y, z2), GLColour(), vector2df(uv.z, uv.y)),
					GLVertex(vector3df(x1, y, z2), GLColour(), vector2df(uv.x, uv.y)),
					GLVertex(vector3df(x1, yDest, z2), GLColour(), vector2df(uv.x, uv.y + yUV)),
					GLVertex(vector3df(x2, yDest, z2), GLColour(), vector2df(uv.z, uv.y + yUV)));
			}
		}
	}

	// Set the creation flag to true so that we build the mesh in the next update
	m_bNeedsCreation = true;

	// Build us
	BufferTerrain();
}
