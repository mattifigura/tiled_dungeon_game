#include "ResourceManager.h"

template<typename T>
ResourceManager<T>::ResourceManager(T*(*func)(const std::string& key))
	: m_CreateFunction(func)
{

}

template<typename T>
ResourceManager<T>::~ResourceManager()
{
	Clear();
}

template<typename T>
void ResourceManager<T>::Clear()
{
	m_Resources.clear();
}

template<typename T>
const T& ResourceManager<T>::GetResource(const std::string& key)
{
	// Check to see if this resource already exists
	auto resource = m_Resources.find(key);
	if (resource != m_Resources.end())
		return *resource->second;

	// We need to load this resource
	m_Resources.insert(std::make_pair(key, std::unique_ptr<T>(m_CreateFunction(key))));

	// Now return a reference to this
	return *(m_Resources.find(key)->second);
}